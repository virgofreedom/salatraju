<div class="small-12 columns big-menu">
    <?php
    if(isset($_POST['save'])){
        $wrong_word = $_POST['Word'];
        $correct_word = $_POST['UpdateWord'];
        $correct_des = array();
        $res = db_get('dictionnary',"WHERE Description LIKE '%$wrong_word%'",'',"Order BY Id");
        if (count($res)==0){
            echo 'There is no data found!';
            return;
        }
        for ($i=0;$i<count($res);$i++){
            array_push($correct_des,
                array(
                    'Id'=>$res[$i]['Id'],
                    'Description'=>str_ireplace($wrong_word,$correct_word,$res[$i]['Description'])
                    )
            );
        }
        
        for($i=0;$i<count($correct_des);$i++){
            $cond = array("Id"=>$correct_des[$i]['Id']);
            $val = array("Description"=>$correct_des[$i]['Description']);
            db_update('dictionnary',$val,$cond);
        }
    echo 'Database has been updated!!';
    }else{
    ?>
    <form action="<?=THIS_PAGE?>" method="POST"/>
        <div class="row">
            <div class="small-12">
                <label for="title">Wrong word:*
                    <input type="text" name="Word" value="" placeholder="" required/>
                </label>
            </div>
        </div>
        <div class="row">
        <label for="title">Correct word:*
                    <input type="text" name="UpdateWord" value="" placeholder="" required/>
        </label>
        </div>
        
        <div class="row">
            <div class="small-12">
                <input type="submit" class="button right" name="save" value="Save"/>
            </div>  
        </div>
    </form>
    <?php
    }
    
    ?>
</div>