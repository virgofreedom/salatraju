<?php
$side_set = db_get('sidebar_set');
$SideOrders = array();
$SideType = array();
$SideLabel = array();
$SidePositoin = array();
$SideOpotion = array();
$SideUrl = array();
$SideText = array();
$side_items = array();
if(count($side_set)==0){
    echo "You do not set any sidebar yet! please login into the admin panel to set the sidebar.";
    die;
}else{
    function sidebar_left($class="",$class_col_grid=""){
        #var class is use to add the class to design the block
        #var class_col_grid is to use to define the width of the side bar. in mobile mode is alway 100%. set for medium and large resolution only.
        echo '
        <div class="small-12 '.$class_col_grid.' columns">
        ';
        
        $res_side = db_get('sidebar_set','Where Position<>"right"','','Order by Orders');
        if (count($res_side) == 0){
            echo '<p></p>';
        }
        for($i=0;$i<count($res_side);$i++){
            echo '
            <div class="small-12 medium-12 large-12 columns '.$class.'">
	        <p><b>'.$res_side[$i]['Label'].'</b></p>
            ';
            if($res_side[$i]['SidebarType'] == 'cat_items'){
                echo '<ul>';
                $res_items = db_get('fp_posts',"WHERE Catergories LIKE '%".$res_side[$i]['SourceId']."%'",'','Order by PostDate DESC','LIMIT 0,'.$res_side[$i]['Options']);
                for($z = 0;$z<count($res_items);$z++){
                    echo '
                        <li><a href="'.DOMAIN.'post/'.$res_items[$z]['PostId'].'">'.$res_items[$z]['PostTitle'].'</a></li>
                    ';
                }
                $res_cat = db_get('catergories',"WHERE Id='".$res_side[$i]['SourceId']."'");
                echo '
                </ul>
                <p class="right"><a href="'.DOMAIN.'categories/'.$res_cat[0]['Name'].'">ព័ត៌មានផ្សេងៗទៀត</a></p>
                ';
            }else{
                
                $res_items = db_get('sidebar_items','Where SidebarId="'.$res_side[$i]['SourceId'].'"');
                if($res_items[0]['SidebarType']=='input'){
                    echo '
                    <form action="'.DOMAIN.$res_items[0]['Url'].'" method="post">
                    '.htmlspecialchars_decode($res_items[0]['Text'],ENT_QUOTES).'
                    </form>
                    ';
                }else if($res_items[0]['SidebarType']=='link'){
                    echo '
                <ul class="cat-item">
                    <li><a href="'.$res_items[0]['Url'].'">'.htmlspecialchars_decode($res_items[0]['Text'],ENT_QUOTES).'</a></li>
                </ul>';
                }else if($res_items[0]['SidebarType']=='api'){
                    echo htmlspecialchars_decode($res_side[$i]['Options'],ENT_QUOTES);
                }else if($res_items[0]['SidebarType']=='lastest'){
                        $res = db_get('fp_posts',"WHERE PostType='post' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT 0,'.$res_side[$i]['Options']);
                        echo '<ul>';
                        for($y=0;$y<count($res);$y++){
                            echo '
                                <li><a href="'.DOMAIN.'post/'.$res[$y]['PostId'].'">'.$res[$y]['PostTitle'].'</a></li>
                                    ';
                        }
                        echo '</ul>';
                }else if($res_items[0]['SidebarType']=='slideshow'){
                    
                        slideshow(PHYSICAL_PATH.$res_items[0]['Url'],$res_items[0]["Text"],$res_items[0]['Url']);
                }
                
            }
            echo '
            </div>
            ';
        }
        
        echo '
        </div>
        ';
    }
    function sidebar_right($class="",$class_col_grid=""){
        #var class is use to add the class to design the block
        #var class_col_grid is to use to define the width of the side bar. in mobile mode is alway 100%. set for medium and large resolution only.
        echo '
        <div class="small-12 '.$class_col_grid.' columns">
        ';
        $res_side = db_get('sidebar_set','Where Position<>"left"','','Order by Orders');
        
        if (count($res_side) == 0){
            echo '<p></p>';
        }

        
        for($i=0;$i<count($res_side);$i++){
            echo '
            <div class="small-12 medium-12 large-12 columns '.$class.'">
	        <p><b>'.$res_side[$i]['Label'].'</b></p>
            ';
            if($res_side[$i]['SidebarType'] == 'cat_items'){
                echo '<ul>';
                $res_items = db_get('fp_posts',"WHERE Catergories LIKE '%".$res_side[$i]['SourceId']."%'",'','Order by PostDate DESC','LIMIT 0,'.$res_side[$i]['Options']);
                for($z = 0;$z<count($res_items);$z++){
                    echo '
                        <li><a href="'.DOMAIN.'post/'.$res_items[$z]['PostId'].'">'.$res_items[$z]['PostTitle'].'</a></li>
                    ';
                }
                $res_cat = db_get('catergories',"WHERE Id='".$res_side[$i]['SourceId']."'");
                echo '
                </ul>
                <p class="right"><a href="'.DOMAIN.'categories/'.$res_cat[0]['Name'].'">ព័ត៌មានផ្សេងៗទៀត</a></p>
                ';
            }else{
                
                $res_items = db_get('sidebar_items','Where SidebarId="'.$res_side[$i]['SourceId'].'"');
                if($res_items[0]['SidebarType']=='input'){
                    echo '
                    <form action="'.DOMAIN.$res_items[0]['Url'].'" method="post">
                    '.htmlspecialchars_decode($res_items[0]['Text'],ENT_QUOTES).'
                    </form>
                    ';
                }else if($res_items[0]['SidebarType']=='link'){
                    echo '
                <ul class="cat-item">
                    <li><a href="'.$res_items[0]['Url'].'">'.htmlspecialchars_decode($res_items[0]['Text'],ENT_QUOTES).'</a></li>
                </ul>';
                }else if($res_items[0]['SidebarType']=='api'){
                    echo htmlspecialchars_decode($res_side[$i]['Options'],ENT_QUOTES);
                }else if($res_items[0]['SidebarType']=='lastest'){
                        $res = db_get('fp_posts',"WHERE PostType='post' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT 0,'.$res_side[$i]['Options']);
                        echo '<ul>';
                        for($y=0;$y<count($res);$y++){
                            echo '
                                <li><a href="'.DOMAIN.'post/'.$res[$y]['PostId'].'">'.$res[$y]['PostTitle'].'</a></li>
                                    ';
                        }
                        echo '</ul>';
                }else if($res_items[0]['SidebarType']=='slideshow'){
                    
                        slideshow(PHYSICAL_PATH.$res_items[0]['Url'],$res_items[0]["Text"],$res_items[0]['Url']);
                }
                
            }
            echo '
            </div>
            ';
        }
        
        echo '
        </div>
        ';
    }
    
}