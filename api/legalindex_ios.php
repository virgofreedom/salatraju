<?php
include '../fp-admin/config/config.php';


$myObj = new \stdClass();
$krom = array();
// Get Krom Data
if (isset($_GET['keyword']) && $_GET['keyword']=="krom"){
    $krom_data = db_get("krom","WHERE Status=1","","ORDER BY kromId ASC","",DB_NAME2);
    for ($i=0;$i<count($krom_data);$i++){
        array_push(
            $krom,array(
                "kromId"=>$krom_data[$i]['kromId'],
                "kromDateuse"=>$krom_data[$i]['kromDateuse'],
                "kromTitle"=>$krom_data[$i]['kromTitle'],
                "kromDescrtiption"=>html_entity_decode(trim($krom_data[$i]['kromDescrtiption']),ENT_QUOTES),
            )
        );
    }
    http_response_code(200);
    // Here convert from array into json object
    $myJSON = json_encode(array("legalindexs"=>$krom), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="kinthi"){
    // Get Kinthi data
    // $_GET['id'] is krom id 
    $kinthin = array();
    $kinthi_data = db_get("kinthi","WHERE kromId='".$_GET['id']."' AND Status=1","","ORDER BY kinthiId ASC","","salatraju_annotation");
    $krom_data =  db_get("krom","WHERE kromId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($kinthi_data);$i++){
        array_push(
            $kinthin,array(
                "kinthiId"=>$kinthi_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[$i]['kinthiTitle'],
                "kromId"=>$kinthi_data[$i]['kromId'],
                "kromTitle"=>$krom_data[0]['kromTitle'],
                "Status"=>$kinthi_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("kinithis"=>$kinthin), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="metika"){
// Get metika data
// $_GET['id'] is kinthi id
$metika = array();
    $metika_data = db_get("metika","WHERE kinthiId='".$_GET['id']."' AND Status=1","","ORDER BY metikaId ASC","","salatraju_annotation");
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($metika_data);$i++){
        array_push(
            $metika,array(
                "kinthiId"=>$metika_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[0]['kinthiTitle'],
                "metikaId"=>$metika_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[$i]['metikaTitle'],
                "Status"=>$metika_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("metikas"=>$metika), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="chapter"){
    // Get chapter data
    // $_GET['id'] is metika id
    $chapter = array();
    $chapter_data = db_get("chapter","WHERE metikaId='".$_GET['id']."' AND Status=1","","ORDER BY chapterId ASC","","salatraju_annotation");
    $metika_data = db_get("metika","WHERE metikaId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($chapter_data);$i++){
        array_push(
            $chapter,array(
                "chapterId"=>$chapter_data[$i]['chapterId'],
                "metikaId"=>$chapter_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[0]['metikaTitle'],
                "chapterTitle"=>$chapter_data[$i]['chapterTitle'],
                "Status"=>$chapter_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("chapters"=>$chapter), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="section"){
    // Get section data
    // $_GET['id'] is chapter id
    $section = array();
    $section_data = db_get("section","WHERE chapterId='".$_GET['id']."' AND Status=1","","ORDER BY sectionId ASC","","salatraju_annotation");
    $chapter_data = db_get("chapter","WHERE chapterId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($section_data);$i++){
        array_push(
            $section,array(
                "chapterId"=>$section_data[$i]['chapterId'],
                "chapterTitle"=>$chapter_data[0]['chapterTitle'],
                "sectionId"=>$section_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[$i]['sectionTitle'],
                "Status"=>$section_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("sections"=>$section), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="kathapheak"){
    // Get kathapheak data
    // $_GET['id'] is section id
    $kathapheak = array();
    $kathapheak_data = db_get("kathapheak","WHERE sectionId='".$_GET['id']."' AND Status=1","","ORDER BY kathapheakId ASC","","salatraju_annotation");
    $section_data = db_get("section","WHERE sectionId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($kathapheak_data);$i++){
        array_push(
            $kathapheak,array(
                "kathapheakId"=>$kathapheak_data[$i]['kathapheakId'],
                "sectionId"=>$kathapheak_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[0]['sectionTitle'],
                "kathapheakTitle"=>$kathapheak_data[$i]['kathapheakTitle'],
                "Status"=>$kathapheak_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("kathapheaks"=>$kathapheak), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="metra"){
    // Get metra data
    // $_GET['id'] is kathapheak id
    $metra = array();
    $metra_data = db_get("metra","WHERE kathapheakId='".$_GET['id']."'","","ORDER BY metraOrder ASC","","salatraju_annotation");
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($metra_data);$i++){
        array_push(
            $metra,array(
                "kathapheakId"=>$metra_data[$i]['kathapheakId'],
                "kathapheakTitle"=>$kathapheak_data[0]['kathapheakTitle'],
                "metraId"=>$metra_data[$i]['metraId'],
                "metraTitle"=>$metra_data[$i]['metraTitle'],
                "metraDescription"=>$metra_data[$i]['metraDescription'],
                "metraOrder"=>$metra_data[$i]['metraOrder']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("metras"=>$metra), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="detail"){
    // Get metra detail
    // $_GET['id'] is metra id
    $detail = array();
    if (isset($_GET["kapheakid"]) && $_GET["kapheakid"]!=""){
        $metra_data = db_get("metra","WHERE metraOrder='".$_GET['id']."' ","","ORDER BY metraOrder ASC","",DB_NAME2);
    }else{
        $metra_data = db_get("metra","WHERE metraId='".$_GET['id']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    }
    $kathapheak_data = db_get("metra","WHERE kathapheakId='".$metra_data[0]['kathapheakId']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    $count_kathapeak = count($kathapheak_data);
    for ($i=0;$i<count($metra_data);$i++){
        
        array_push($detail,array(
            "metraTitle"=> $metra_data[$i]['metraTitle'],
            "metraDescription"=>$metra_data[$i]['metraDescription'],
            "kathapheakId"=>$metra_data[$i]['kathapheakId'],
            "metraOrder"=>$metra_data[$i]['metraOrder'],
            "NextMetra"=> $metra_data[$i]['metraOrder'] + 1,
            "PrevMetra"=> $metra_data[$i]['metraOrder'] - 1,
            "CountKathapheak"=> $count_kathapeak
        ));
    }
 // Here convert from array into json object
    $myJSON = json_encode(array("detail"=>$detail), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="metras"){
    $detail = array();
    $metra_data = db_get("metra","WHERE metraOrder='".$_GET['id']."' AND kathapheakId='".$_GET['kapheakid']."'","","ORDER BY metraOrder ASC","",DB_NAME2);

}elseif (isset($_GET['keyword']) && $_GET['keyword']=="searchs"){
    // Get data from search
    $result = array();
    $keyword = $_GET['id'];
    #get metra from keyword
    $res = db_get("metra","Where metraDescription LIKE'%".$keyword."%' OR metraTitle LIKE'%".$keyword."%'","","ORDER BY kathapheakId ASC","",DB_NAME2);
    if (count($res)>0){
        for ($i=0;$i<count($res);$i++){
            array_push(
                $result,array(
                    'metraId'=>$res[$i]["metraId"],
                    'metraTitle'=>$res[$i]["metraTitle"],
                ));
        }
    }else{
        array_push(
            $result,array(
                'metraId'=>"0",
                'metraTitle'=>"មិនមានពាក្យដែលស្វែងរកនេះទេ!",
            ));
    }
    
    $myJSON = json_encode(array("results"=>$result), JSON_PRETTY_PRINT);
    echo $myJSON;
}elseif (isset($_GET['keyword']) && $_GET['keyword']=="getallmetra"){
    // Get all metra data
    $res = db_get("metra","","","ORDER BY kathapheakId ASC","","salatraju_annotation");
    $result = array();
    if (count($res)>0){
        for ($i=0;$i<count($res);$i++){
            array_push(
                $result,array(
                    'metraId'=>$res[$i]["metraId"],
                    'metraTitle'=>$res[$i]["metraTitle"],
                    'metraDescription'=>$res[$i]['metraDescription']
                ));
        }
    }else{
        array_push(
            $result,array(
                'metraId'=>"0",
                'metraTitle'=>"មិនមានពាក្យដែលស្វែងរកនេះទេ!",
                'metraDescription'=>"មិនមានពាក្យដែលស្វែងរកនេះទេ"
            ));
    }
    
    $myJSON = json_encode(array("results"=>$result), JSON_PRETTY_PRINT);
    echo $myJSON;
}else{
    http_response_code(400);
    $legalindex=array(
        "error"=> 2,
        "reason"=> "No data found!"
    );
    // Here convert from array into json object
    $myJSON = json_encode($legalindex, JSON_PRETTY_PRINT);
    echo $myJSON;
   
}