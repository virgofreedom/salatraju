<div class="small-12 columns big-menu">
<?php
include PHYSICAL_PATH.'library/admin.php';
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
$res = db_get('sidebar_items');
echo '<ul>';
for ($i=0;$i<count($res);$i++){
    echo '<li><a href="'.ADMIN_NAV_PATH.'sidebar_list.php?edit='.$res[$i]['SidebarId'].'">'.$res[$i]['SidebarName'].'</a></li>';
}
echo '<li><a href="'.ADMIN_NAV_PATH.'sidebar_new.php">Add new</a></li>';
echo '</ul>';
?>
</div>
<?php
if (isset($_GET['edit'])){
    $cond = array("SidebarId"=>$_GET['edit']);
    $res = db_get_where('sidebar_items',$cond);
    echo '
    <div class="small-12 columns big-menu">
        <form action="'.THIS_PAGE.'" method="POST">
            
            <input type="hidden" name="SidebarId" value="'.$_GET['edit'].'">
            <label for="">Sidebar Name:</label>
            <input type="text" name="SidebarName" value="'.$res[0]['SidebarName'].'">
            <label for="">Sidebar Type:</label>
            <input type="text" name="SidebarType" value="'.$res[0]['SidebarType'].'">
            <label for="">Sidebar Url:</label>
            <input type="text" name="Url" value="'.$res[0]['Url'].'">
            <label for="">Sidebar Lable/Caption:</label>
            <input type="text" name="Text" value="'.$res[0]['Text'].'">
            <input type="submit" name="Save" value="Save" class="button">
        </form>
    </div>
    ';
}
if (isset($_POST['Save'])){
    $val = array(
        "SidebarName"=>$_POST['SidebarName'],
        "SidebarType"=>$_POST['SidebarType'],
        "Url"=>$_POST['Url'],
        "Text"=>$_POST['Text']
    );
    $cond = array(
        "SidebarId"=>$_POST['SidebarId']
    );
    db_update('sidebar_items',$val,$cond);
}
}#end permission
?>
