
<div class="small-12 medium-12 large-12 columns post-block index-post" id="annotaion" style="margin-top:10px;">
<h4><a href="<?=DOMAIN?>annotation">Traju Legal Index</a></h4>
    
    <div class="input-group">
            <label>ស្វែងតាមរកពាក្យគន្លឹះ</label>
            <input class="input-group-field" type="text" id="word">
        <div class="">
          <button class="button small" onclick='ann_search("word","listkrom")' id="btn_search">ស្វែករក</button>
        </div>
    </div>
</div>

<div id="id01"></div>
<div class="w3-col l12 m12 s12 index-post" id="stepback">
<div id="krom"></div><div id="kinthi"></div><div id="metika"></div>
<div id="chapter"></div><div id="section"></div><div id="katapheak"></div>
</div>
<div class="small-12 medium-12 large-12 columns post-block index-post" id="listkrom"></div>


<?php
if($_GET['legal_index'] == "krom"){
  echo '<script>load_krom("listkrom","'.$_GET['id'].'");';
}elseif($_GET['legal_index'] == "kinthi"){
  echo '<script>load_kinthi("listkrom","'.$_GET['id'].'");';
}elseif ($_GET['legal_index'] == "metika"){
  echo '<script>load_metika("listkrom","'.$_GET['id'].'");';
}elseif ($_GET['legal_index'] == "chapter"){
  echo '<script>load_chapter("listkrom","'.$_GET['id'].'");';
}elseif ($_GET['legal_index'] == "section"){
  echo '<script>load_section("listkrom","'.$_GET['id'].'");';
}elseif ($_GET['legal_index'] == "katapheak"){
  echo '<script>load_katapheak("listkrom","'.$_GET['id'].'");';
  
}else{
echo "<script>load_legal_index('listkrom');";
}
?>
//click "Enter" to search
var input = document.getElementById("word");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("btn_search").click();
  }
});
//load legal index
function load_legal_index(receiver){
  
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/legalindex";

  var showout="";
  
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.krom.length;i++){
      showout += '<a href="<?=DOMAIN?>legal_index/krom/'+arr.krom[i].kromId+'" class="w3-button w3-block w3-left-align">'+arr.krom[i].kromTitle+'</a>';
      showout += '<div id="krom'+i+'" class="w3-container w3-hide"><p>Some Text....</p></div>';
      
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  
}
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}
//load Krom
function load_krom(receiver,id){
  
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/krom/"+id;

  var showout="";
  
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.kinthi.length;i++){
      showout += '<a href="<?=DOMAIN?>legal_index/kinthi/'+arr.kinthi[i].kinthiId+'" class="w3-button w3-block w3-left-align">'+arr.krom[i].kinthiTitle+'</a>';
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  document.getElementById("krom").innerHTML = "<a href='<?=DOMAIN?>legal_index/"+arr.kinthi[0].kromId+"'>"+arr.kinthi[0].kromTitle+"</a>";
}
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

//load kinthi
function load_kinthi(receiver,id){

  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/kinthi/"+id;

  var showout="";
  
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.kinthi.length;i++){
      
      if (arr.kinthi[i].kinthiTitle == "0" || arr.kinthi[i].kinthiTitle == ""){
        
        showout += '<div id="kinthi'+id+i+'" class="w3-container"><p>Some text..</p></div>';
        load_metika("kinthi"+id+i,arr.kinthi[i].kinthiId);
        
      }else{
        showout += '<a href="<?=DOMAIN?>legal_index/kinthi/'+arr.kinthi[i].kinthiId+'" class="w3-button w3-block w3-left-align">'+arr.kinthi[i].kinthiTitle+'</a>';
      showout += '<div id="kinthi'+id+i+'" class="w3-container w3-hide"><p>Some text..</p></div>';
      }
      
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  document.getElementById("krom").innerHTML = "<a href='<?=DOMAIN?>legal_index/"+arr.kinthi[0].kromId+"'>"+arr.kinthi[0].kromTitle+"</a>";
}
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

//load metika
function load_metika(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/metika/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.metika.length;i++){
    if (arr.metika[i].metikaTitle == "0" || arr.metika[i].metikaTitle == ""){
      showout += '<div id="metika'+id+i+'" class="w3-container"><p>Some text..</p></div>';
      load_chapter("metika"+id+i,arr.metika[i].metikaId);
      
    }else{
      showout += '<button onclick="load_chapter('+ "'metika" +id+ i + "'"+','+"'"+arr.metika[i].metikaId
      +"'"+');myFunction('+ "'metika"+id + i + "'"+')" class="w3-button w3-block w3-left-align">'+arr.metika[i].metikaTitle+'</button>';
    showout += '<div id="metika'+id+i+'" class="w3-container w3-hide"><p>Some text..</p></div>';
    }
    
  }
  
}

document.getElementById(receiver).innerHTML = showout;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load chapter
function load_chapter(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/chapter/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.chapter.length;i++){
    if (arr.chapter[i].chapterTitle == "0" || arr.chapter[i].chapterTitle==""){
      showout += '<div id="chapter'+id+i+'" class="w3-container"><p>Some text..</p></div>';
      load_section("chapter"+id+i,arr.chapter[i].chapterId);
    }else{
      showout += '<button onclick="load_section('+ "'chapter" +id+ i + "'"+','+"'"+arr.chapter[i].chapterId
      +"'"+');myFunction('+ "'chapter"+id + i + "'"+')" class="w3-button w3-block w3-left-align">'+arr.chapter[i].chapterTitle+'</button>';
    showout += '<div id="chapter'+id+i+'" class="w3-container w3-hide"><p>Some text..</p></div>';
    }
    
  }
  
}

document.getElementById(receiver).innerHTML = showout;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load section
function load_section(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/section/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.section.length;i++){
    if(arr.section[i].sectionTitle =="0" || arr.section[i].sectionTitle == ""){
      showout += '<div id="section'+id+i+'" class="w3-container"><p>Some text..</p></div>';
      load_katapheak("section"+id+i,arr.section[i].sectionId);
    }else{
      showout += '<button onclick="load_katapheak('+ "'section" +id+ i + "'"+','+"'"+arr.section[i].sectionId
      +"'"+');myFunction('+ "'section"+id + i + "'"+')" class="w3-button w3-block w3-left-align">'+arr.section[i].sectionTitle+'</button>';
    showout += '<div id="section'+id+i+'" class="w3-container w3-hide"><p>Some text..</p></div>';
    }
    
  }
  
}

document.getElementById(receiver).innerHTML = showout;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load katapheak data
function load_katapheak(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/kathapheak/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.kathapheak.length;i++){
    if (arr.kathapheak[i].kathapheakTitle == "0" || arr.kathapheak[i].kathapheakTitle == ""){
    showout += '<div id="kathapheak'+id+i+'" class="w3-container"><p>Some text..</p></div>';
    load_metra("kathapheak"+id+i,arr.kathapheak[i].kathapheakId);
    }else{
      showout += '<button onclick="load_metra('+ "'kathapheak" +id+ i + "'"+','+"'"+arr.kathapheak[i].kathapheakId
      +"'"+');myFunction('+ "'kathapheak"+id + i + "'"+')" class="w3-button w3-block w3-left-align">'+arr.kathapheak[i].kathapheakTitle+'</button>';
    showout += '<div id="kathapheak'+id+i+'" class="w3-container w3-hide"><p>Some text..</p></div>';
    }
    
  }
  
}

  document.getElementById(receiver).innerHTML = showout;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}


//load metra data
function load_metra(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/metra/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.metra.length;i++){
    showout += '<button style="overflow-wrap: break-word;" onclick="load_detail('+"'listkrom'"+','+"'"+ arr.metra[i].metraId +"'"+')" class="w3-button w3-block w3-left-align">'+arr.metra[i].metraTitle+'</button>';

  }
  
}

document.getElementById(receiver).innerHTML = showout;
}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}
//function for accordions
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
//Load all data
function load_detail(receiver,id){
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/detail/"+id;

  var showout="";

  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.detail.length;i++){
      var kinthi_t = "";var metika_t = "";var chapter_t = "";var section_t = "";var kathapheak_t ="";
      if (arr.detail[i].kinthiTitle == '0'){ kinthi_t = "";}else{kinthi_t = "<a href='<?=DOMAIN?>legal_index/kinthi/"+arr.detail[i].kinthiId+"'>"+arr.detail[i].kinthiTitle +"</a>";}
      if (arr.detail[i].metikaTitle == '0'){ metika_t = "";}else{metika_t = "<a href='<?=DOMAIN?>legal_index/metika/"+arr.detail[i].metikaId+"'>" + ">>" +arr.detail[i].metikaTitle +"</a>";}
      if (arr.detail[i].chapterTitle == '0'){ chapter_t = "";}else{chapter_t = "<a href='<?=DOMAIN?>legal_index/chapter/"+arr.detail[i].chapterId+"'>" + ">>" +arr.detail[i].chapterTitle +"</a>";}
      if (arr.detail[i].sectionTitle == '0'){ section_t = "";}else{section_t = "<a href='<?=DOMAIN?>legal_index/section/"+arr.detail[i].sectionId+"'>" + ">>" +arr.detail[i].sectionTitle +"</a>";}
      if (arr.detail[i].kathapheakTitle == '0'){ kathapheak_t ="";}else{kathapheak_t = "<a href='<?=DOMAIN?>legal_index/section/"+arr.detail[i].kathapheakId+"'>" + ">>" +arr.detail[i].kathapheakTitle +"</a>";}
      var ref_list = "<ul>";
      var date_ref = "";
      for (var y = 0;y < arr.detail[i].referenceDate.length;y++){
          if(arr.detail[i].referenceDate[y] =="0000-00-00"){
              date_ref = "";
          }else{
              date_ref = " ចុះថ្ងៃទី "+arr.detail[i].referenceDate[y];
          }
          ref_list += "<li>បានទ្រង់យល់ "+arr.detail[i].referenceTitle[y]+" "+ arr.detail[i].referenceIdkh[y] 
          + date_ref +" "+arr.detail[i].referenceDescription[y]+" </li>";
      }
      ref_list += "</ul>";
      var showout = '<h4>'+kinthi_t+metika_t+chapter_t+section_t+kathapheak_t+kathapheak_t+
      '</h4><center><h5>'+arr.detail[i].metraTitle+'</h5></center>'+arr.detail[i].metraDescription;
    }
    
  }
  showout += "<div class='row'><a href='<?=DOMAIN?>legal_index'>" + "<< បង្ហាញក្រមទាំងអស់</a></div>";
  document.getElementById(receiver).innerHTML = showout;
  }
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}
//Annotation search
function ann_search(word,receiver){
var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/searchs/" + document.getElementById(word).value;

  xmlhttp.onreadystatechange=function() {
    var out = "<h5>លទ្ធផលការស្វែករក :</h5>";    
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
          var arr = JSON.parse(xmlhttp.responseText);
          //Do loop over array and add style output
          for (var i = 0; i < arr.metras.length;i++){
              var ref_title = "[";
              var ref_id = "[";
              var ref_date = "[";
              var ref_desc = "[";
              for (var y = 0; y < arr.metras[i].referenceTitle.length;y++){
                  if(y ==0){
                    ref_title += "'" + arr.metras[i].referenceTitle[y] + "'";
                    ref_id += "'" + arr.metras[i].referenceIdkh[y] + "'";
                    ref_date += "'" + arr.metras[i].referenceDate[y] + "'";
                    ref_desc += "'" + arr.metras[i].referenceDescription[y] + "'";
                  }else{
                    ref_title += ",'" + arr.metras[i].referenceTitle[y] + "'";
                    ref_id += ",'" + arr.metras[i].referenceIdkh[y] + "'";
                    ref_date += ",'" + arr.metras[i].referenceDate[y] + "'";
                    ref_desc += ",'" + arr.metras[i].referenceDescription[y] + "'";
                  }
                  
              }
              ref_title += "]";
              ref_id += "]";
              ref_date += "]";
              ref_desc += "]";
            out += 
            '<div class="small-12 medium-12 large-12 columns post-block index-post"><a href="#" onclick="'+
            "ann_detail('listkrom','"+ arr.metras[i].kromTitle +"'," + ref_title + ","+ref_id + ","+ ref_date + ","+ ref_desc + ",'"+
            arr.metras[i].kromDescrtiption + "','" +arr.metras[i].kinthiTitle + "','"+arr.metras[i].metikaTitle+ "','"+arr.metras[i].chapterTitle+
            "','"+arr.metras[i].sectionTitle+ "','"+arr.metras[i].kathapheakTitle+ "','"+arr.metras[i].metraTitle+ "','"+ arr.metras[i].Description
            +"');"
            +'"><p><b>' + 
            arr.metras[i].metraTitle + '</b> (មាត្រា '+ arr.metras[i].metraOrder +' នៃ '+ arr.metras[i].kromTitle +') </p></a></div>';
          }
      }
      
      document.getElementById(receiver).innerHTML = out;
  }
  
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
  
}
function ann_detail(receiver,kromtitle,ref_title,ref_id,ref_date,ref_desc,kromdesc,kinthi,metika,chapter,section,kathapheak,metra,metradesc){
    
    var kinthi_t = "";var metika_t = "";var chapter_t = "";var section_t = "";var kathapheak_t ="";
    if (kinthi == '0'){ kinthi_t = "";}else{kinthi_t = kinthi;}
    if (metika == '0'){ metika_t = "";}else{metika_t = ">>" +metika;}
    if (chapter == '0'){ chapter_t = "";}else{chapter_t = ">>" +chapter;}
    if (section == '0'){ section_t = "";}else{section_t = ">>" +section;}
    if (kathapheak == '0'){ kathapheak_t ="";}else{kathapheak_t = ">>" +kathapheak;}
    var ref_list = "<ul>";
    var date_ref = "";
    for (var i = 0;i < ref_title.length;i++){
        if(ref_date[i] =="0000-00-00"){
            date_ref = "";
        }else{
             date_ref = " ចុះថ្ងៃទី "+ref_date[i];
        }
        ref_list += "<li>បានទ្រង់យល់ "+ref_title[i]+" "+ ref_id[i] + date_ref +" "+ref_desc[i]+" </li>";
    }
    ref_list += "</ul>";
    var out = metradesc;
    out += "<div class='row'><a href='<?=DOMAIN?>legal_index'>" + "<< បង្ហាញក្រមទាំងអស់</a></div>";
    document.getElementById(receiver).innerHTML = out;
}
</script>