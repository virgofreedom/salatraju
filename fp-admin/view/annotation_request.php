<div class="small-12 columns big-menu">
<?php
$permission = array("Admin","Superuser","Annotation");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    //Do the chage the Status to -1
    
    $cond = array($_GET['step']."Id"=>$_GET['id']);
    $get_data = db_get_where($_GET['step'],$cond,'',DB_NAME2);
    $data = array(
        "Status"=> -1
    );
    db_update($_GET['step'],$data,$cond,DB_NAME2);
    $data_log = array(
        "UserId"=> $_SESSION['infos']['id'],
        "Action"=> "deleted",
        "ItemName"=>$_GET['step'],
        "ItemTitle"=>$get_data[0][$_GET['step'].'Title'],
        "Date"=>date('Y-m-d h:i:s')
    );
    db_insert('annotation_deleted_list',$data_log);
?>
    <h4>ពាក្យនេះបានលុបចេញរួចរាល់ហើយ! សូមអរគុណ!</h4>
    <!-- Cancel button -->
    <form action="annotation_<?=$_GET['back']?>" method="POST">
                <input type="text" value="<?=$_GET['backid']?>" name="<?=$_GET['bname']?>" readonly hidden>
                <input type="text" value="<?=$_GET['previousval']?>" name="<?=$_GET['previous']?>" readonly hidden>
                <div class="small-6 large-1 columns">
                    <input type="submit" class="button" name="<?=$_GET['step']?>" value="Done">
                </div>
    </form>
<?php
}
?>
</div>