<?php
$res = db_get('index_layout','','','Order by Orders');
/*Start loop */
for ($i=0;$i<count($res);$i++){
    echo '
    <h4>'.$res[$i]['Label'].'</h4>
    ';
    /* Get Layout to make grid */
        if ($res[$i]['TypeLayout'] == 1){
            $grid = "medium-10 large-10 columns medium-offset-1 large-offset-1";
        }elseif ($res[$i]['TypeLayout'] == 2){
            $grid = "medium-6 large-6 columns";
        }elseif ($res[$i]['TypeLayout'] == 3){
            $grid = "medium-4 large-4 columns";
        }elseif ($res[$i]['TypeLayout'] == 4){
            $grid = "medium-3 large-3 columns";
        }elseif ($res[$i]['TypeLayout'] == 5){
            $grid = "medium-6 large-6 columns";
        }
    /* End Get Layout to make grid */
    echo '<div class="w3-row-padding w3-padding-16 w3-border-bottom border-traju" onclick="w3_close(\'sidebar\')">';
    /* Get content from slide show */
        if ($res[$i]['Type'] == "slideshow"){#Slideshow
            if ($res[$i]['Path'] == ''){
                echo '
                <div class="small-12 medium-10 large-10 columns medium-offset-1 large-offset-1">';
                slideshow('fp-admin/ckeditor/plugins/imageuploader/uploads/slideshow/',$res[$i]['Caption'],$i);
                echo '</div>';
            }else{#will update to use with customize path
                echo '
                <div class="small-12 medium-10 large-10 columns medium-offset-1 large-offset-1">';
                slideshow($res[$i]['Path'],$res[$i]['Caption'],$i);
                echo '</div>';
            }
            

        }
        elseif($res[$i]['Type'] == "apps"){#apps
        echo '
        <div class="small-12 medium-10 large-10 columns medium-offset-1 large-offset-1"><p>';
        echo $res[$i]['Caption'];
        echo'</p>';
            echo '<div class="w3-container w3-center l12">';
            #Andoird
                echo'<div class="w3-col s6">';
                echo '<a href="'.$res[$i]['Android'].'"><img alt="ទាញយកវានៅលើ App Store" src="'.IMG_PATH_SITE.'kh_playstore.png"/></a>
                </div>
                ';
            #iOS
                echo'<div class="w3-col s6">';
                echo '<a href="'.$res[$i]['Ios'].'"><img alt="ទាញយកវានៅលើ App Store" src="'.IMG_PATH_SITE.'appstore.png"/></a>
                </div>
                ';
            echo'</div>';
        echo'</div>';
    /* End Get content from slide show */
    /* Get content from post, page, and categories */
        }
        elseif($res[$i]['Type'] == "post" || $res[$i]['Type'] == "page" || $res[$i]['Type'] == "catergories"){#for post
            $seemore = "mores/".$res[$i]['Type'];
                if ($res[$i]['Type'] == "page"){
                    if ($res[$i]['Start'] == 0){#recent added
                        $res_content = db_get('fp_posts',"WHERE PostType='page' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT 0,'.$res[$i]['Length']);
                    }else{
                        $res_content = db_get('fp_posts',"WHERE PostType='page' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT '.$res[$i]['Start'].','.$res[$i]['Length']);
                    }
                }elseif($res[$i]['Type'] == "catergories"){
                    $res_content=[];
                    /*Set original category */
                    $or_cat = db_get("catergories","Where Id=".$res[$i]['ItemsId']);
                    /* Get all the post that has the same category id*/
                    $res_cat = db_get('fp_posts',"WHERE Catergories LIKE '%".$or_cat[0]['Id']."%'",'','ORDER BY PostDate DESC','LIMIT 0,'.$res[$i]['Length']);
                    for ($y=0; $y<count($res_cat);$y++){#check all the post that has look like Catogry Id
                        if (stristr($res_cat[$y]['Catergories'],';')!=""){
                            #find in Category if there is more than 1, if there comma<;> that mean more than 1
                            $cat_arr = explode(';',$res_cat[$y]['Catergories']);#explode into array
                            $resul = array_intersect($or_cat[0],$cat_arr);#get only the content that has the same Id
                            
                            if(count($resul) > 0){
                                $show = true;
                            }else{
                                $show = false;
                            }
                        }else{
                            if ($or_cat[0]['Id'] == $res_cat[$y]['Catergories']){
                                $show = true;
                            }else{
                                $show = false;
                            }
                        }
                        if ($show == true){#correct the good data and store it in an array
                            array_push($res_content,array(
                                "PostId"=>$res_cat[$y]['PostId'],
                                "PostTitle"=>$res_cat[$y]['PostTitle'],
                                "PostContent"=>$res_cat[$y]['PostContent'],
                                "Categories"=>$or_cat[0]['Name'],
                                
                            ));
                        
                        }
                    }
                    $seemore = "categories/".$or_cat[0]['Name'];    
                }else{
                    if ($res[$i]['Start'] == 0){#recent added
                        $res_content = db_get('fp_posts',"WHERE PostType='post' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT 0,'.$res[$i]['Length']);
                    }else{
                        $res_content = db_get('fp_posts',"WHERE PostType='post' AND PostStatus=2 ",'','ORDER BY PostDate DESC','LIMIT '.$res[$i]['Start'].','.$res[$i]['Length']);
                    }
                    
                }
            if ($res[$i]['TypeLayout']!=5){#if type of layout is not 5
                
                for ($y=0;$y<count($res_content);$y++){
                
                $str_cont = htmlspecialchars_decode($res_content[$y]['PostContent'],ENT_QUOTES);
                #convert from whole content to a paragraph.
                $p1 = stripos($str_cont,'<p');
                $p2 = stripos($str_cont,'<p',stripos($str_cont,'</p>'));

                $end_p2 = stripos($str_cont,'/p>',$p2);
                $len_p2 = $end_p2 - $p2 - 1;
                $sub_content = substr($str_cont,stripos($str_cont,'<p'),stripos($str_cont,'</p>'));
                $content = substr($str_cont,$p2,$len_p2);
                #find if there is image inside the paragraph or not.
                if(stripos($sub_content,"<img")!=""){
                    $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                    $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                    $len_code = strripos($img,".") - strripos($img,"/");
                    $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                }else{#make the default image
                    $code_img = '62a6153d.jpg';
                }
                    echo '
                    <div class="small-12 '.$grid.' hide-for-small" style="height: 290px;overflow-y:hidden">';
                        echo '<a href="'.NAV_PATH.'post/'.$res_content[$y]['PostId'].'">
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/thumbnail/thumb_'.$code_img.'" alt="" class="w3-border w3-padding">
                        ';
                    echo '<h5 class="w3-left-align"><strong class="traju-color">'.$res_content[$y]['PostTitle'].'</strong></h5>
                        </a>
                    </div>'; 
                    echo '
                    <div class="small-12 '.$grid.' columns show-for-small"><a href="'.NAV_PATH.'post/'.$res_content[$y]['PostId'].'">';
                    if(stripos($sub_content,"<img")!=""){
                        $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                        $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                        $len_code = strripos($img,".") - strripos($img,"/");
                        $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                        echo '
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/'.$code_img.'" alt="" class="w3-border w3-padding">
                        ';
                    }else{
                        echo '
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/62a6153d.jpg" alt="" class="w3-border w3-padding ">
                        ';
                    }
                    
                    echo '<h5 class="w3-left-align"><strong class="traju-color">'.$res_content[$y]['PostTitle'].'</strong></h5>
                        </a>
                    </div>';
                    
                }
            }else{#if the type of layout is 5
                $str_cont = htmlspecialchars_decode($res_content[0]['PostContent'],ENT_QUOTES);
                #convert from whole content to a paragraph.
                $p1 = stripos($str_cont,'<p');
                $p2 = stripos($str_cont,'<p',stripos($str_cont,'</p>'));

                $end_p2 = stripos($str_cont,'/p>',$p2);
                $len_p2 = $end_p2 - $p2 - 1;
                $sub_content = substr($str_cont,stripos($str_cont,'<p'),stripos($str_cont,'</p>'));
                $content = substr($str_cont,$p2,$len_p2);
                #find if there is image inside the paragraph on not.
                if(stripos($sub_content,"<img")!=""){
                    $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                    $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                    $len_code = strripos($img,".") - strripos($img,"/");
                    $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                }else{
                    $code_img = '62a6153d.jpg';
                }
                echo '
                <div class="small-12 '.$grid.' hide-for-small" style="height: 345px;overflow-y:hidden">
                    <a href="'.NAV_PATH.'post/'.$res_content[0]['PostId'].'">
                    <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/'.$code_img.'" alt="" class="w3-border w3-padding">
                    <h5 class="w3-left-align"><strong class="traju-color">'.$res_content[0]['PostTitle'].'</strong></h5>
                    </a>
                </div>
                <div class="small-12 '.$grid.' hide-for-small" style="height: 345px;overflow-y:scroll">';
                for ($y=1;$y<count($res_content);$y++){
                    $str_cont = htmlspecialchars_decode($res_content[$y]['PostContent'],ENT_QUOTES);
                    #convert from whole content to a paragraph.
                    $p1 = stripos($str_cont,'<p');
                    $p2 = stripos($str_cont,'<p',stripos($str_cont,'</p>'));

                    $end_p2 = stripos($str_cont,'/p>',$p2);
                    $len_p2 = $end_p2 - $p2 - 1;
                    $sub_content = substr($str_cont,stripos($str_cont,'<p'),stripos($str_cont,'</p>'));
                    $content = substr($str_cont,$p2,$len_p2);
                    #find if there is image inside the paragraph on not.
                    if(stripos($sub_content,"<img")!=""){
                        $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                        $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                        $len_code = strripos($img,".") - strripos($img,"/");
                        $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                    }else{
                        $code_img = '62a6153d.jpg';
                    }
                    echo '<div class="row" style="height:100px;overflow-y:hidden;">
                    <a href="'.NAV_PATH.'post/'.$res_content[$y]['PostId'].'">
                    <div class="small-3 columns">
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/thumbnail/thumb_'.$code_img.'" alt="" class="w3-border w3-padding">
                    </div>';
                    echo '<div class="small-9 columns">
                        <h5 class="w3-left-align"><strong class="traju-color">'.$res_content[$y]['PostTitle'].'</strong></h5>
                    </div></a></div>';
                }
                echo '
                </div>
                ';
                for ($y=1;$y<count($res_content);$y++){
                    $str_cont = htmlspecialchars_decode($res_content[$y]['PostContent'],ENT_QUOTES);
                    #convert from whole content to a paragraph.
                    $p1 = stripos($str_cont,'<p');
                    $p2 = stripos($str_cont,'<p',stripos($str_cont,'</p>'));

                    $end_p2 = stripos($str_cont,'/p>',$p2);
                    $len_p2 = $end_p2 - $p2 - 1;
                    $sub_content = substr($str_cont,stripos($str_cont,'<p'),stripos($str_cont,'</p>'));
                    $content = substr($str_cont,$p2,$len_p2);
                    #find if there is image inside the paragraph on not.
                    if(stripos($sub_content,"<img")!=""){
                        $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                        $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                        $len_code = strripos($img,".") - strripos($img,"/");
                        $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                    }else{
                        $code_img = '62a6153d.jpg';
                    }
                    echo '
                    <div class="small-12 '.$grid.' columns show-for-small"><a href="'.NAV_PATH.'post/'.$res_content[$y]['PostId'].'">';
                    if(stripos($sub_content,"<img")!=""){
                        $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                        $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                        $len_code = strripos($img,".") - strripos($img,"/");
                        $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
                        echo '
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/'.$code_img.'" alt="" class="w3-border w3-padding">
                        ';
                    }else{
                        echo '
                        <img src="https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/62a6153d.jpg" alt="" class="w3-border w3-padding ">
                        ';
                    }
                    
                    echo '<h5 class="w3-left-align"><strong class="traju-color">'.$res_content[$y]['PostTitle'].'</strong></h5>
                    </a></div>';
                }
            }
            echo '<div class="small-12 medium-12 large-12 columns w3-right-align"><h5><i><a href="'.DOMAIN.$seemore.'"><strong>អត្ថបទផ្សេងៗទៀត</strong></a></i></h5></div>';

        }
    /* End Get content from post, page, and categories */
    /* Get content from video */
        elseif($res[$i]['Type'] == "videos"){
            $res_content = db_get('videos','','','Order by Id DESC');
            if ($res[$i]['TypeLayout']!=5){#if type of layout is not 5
                for ($y=0;$y<count($res_content);$y++){
                    $n = strripos($res_content[$y]['Url'],"/");
                    $youid = substr($res_content[$y]['Url'],$n+1);
                    $url = "https://www.youtube.com/embed/".$youid;
                    echo '
                    <div class="small-12 '.$grid.' left">
                        <iframe width="560" height="315" src="'.$url.'" frameborder="0" allowfullscreen></iframe>
                        <h5 class="w3-left-align"><strong class="traju-color">
                        '.$res_content[$y]['Title'].'
                        </strong></h5>
                    </div>
                    ';
                }
            }else{#if type of layout is 5
                $n = strripos($res_content[0]['Url'],"/");
                $youid = substr($res_content[0]['Url'],$n+1);
                $url = "https://www.youtube.com/embed/".$youid;
                echo '
                <div class="small-12 '.$grid.'">
                <iframe width="560" height="315" src="'.$url.'" frameborder="0" allowfullscreen></iframe>
                <h5 class="w3-left-align"><strong class="traju-color">
                '.$res_content[0]['Title'].'
                </strong></h5>
                </div>
                ';
                echo '
                <div class="small-12 '.$grid.'" style="height: 345px;overflow-y:scroll">';
                for ($y=1;$y<count($res_content);$y++){
                    $n = strripos($res_content[$y]['Url'],"/");
                    $youid = substr($res_content[$y]['Url'],$n+1);
                    $url = "https://www.youtube.com/embed/".$youid;
                    $thumb = "https://i1.ytimg.com/vi/$youid/default.jpg";
                    
                    echo '
                    <div class="row">
                    <a href="'.DOMAIN.'videos/'.$res_content[$y]['Id'].'">
                        <div class="small-3 columns">
                            <img src="'.$thumb.'" alt="'.$res_content[$y]['Title'].'">
                        </div>
                        <div class="small-9 columns">
                        <h5 class="w3-left-align"><strong class="traju-color">
                        '.$res_content[$y]['Title'].'
                        </strong></h5>
                        </div>
                    </a>
                    </div>
                    ';
                }
                echo '</div>';
            }
        
    /*End Get content from video */
    }
/*End loop*/

echo '</div>';
}
?>

