<?php
include '../fp-admin/config/config.php';




$myObj = new \stdClass();
$krom = array();
// Get Krom Data
if (isset($_GET['keyword']) && $_GET['keyword']=="legalindex"){
    $krom_data = db_get("krom","WHERE Status=1","","ORDER BY Orders ASC","",DB_NAME2);
    for ($i=0;$i<count($krom_data);$i++){
        array_push(
            $krom,array(
                "kromId"=>$krom_data[$i]['kromId'],
                "kromDateuse"=>$krom_data[$i]['kromDateuse'],
                "kromTitle"=>$krom_data[$i]['kromTitle'],
                "kromDescrtiption"=>html_entity_decode(trim($krom_data[$i]['kromDescrtiption']),ENT_QUOTES),
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("krom"=>$krom), JSON_PRETTY_PRINT);
    echo $myJSON;
}
//Get Kinthi data
if (isset($_GET['keyword']) && $_GET['keyword']=="krom"){
    $kinthin = array();
    $kinthi_data = db_get("kinthi","WHERE kromId='".$_GET['id']."' AND Status=1","","ORDER BY kinthiTitle ASC","",DB_NAME2);
    $krom_data = db_get("krom","WHERE kromId='".$_GET['id']."' AND Status=1","","ORDER BY Orders ASC","",DB_NAME2);
    for ($i=0;$i<count($kinthi_data);$i++){
        array_push(
            $kinthin,array(
                "kinthiId"=>$kinthi_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[$i]['kinthiTitle'],
                "kromId"=>$kinthi_data[$i]['kromId'],
                "kromTitle"=>$krom_data[0]['kromTitle'],
                "Status"=>$kinthi_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("kinthi"=>$kinthin), JSON_PRETTY_PRINT);
    echo $myJSON;

}
//Get Metika data
if (isset($_GET['keyword']) && $_GET['keyword']=="kinthi"){
    $metika = array();
    $metika_data = db_get("metika","WHERE kinthiId='".$_GET['id']."' AND Status=1","","ORDER BY metikaTitle ASC","",DB_NAME2);
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$_GET['id']."' AND Status=1","","ORDER BY kinthiTitle ASC","",DB_NAME2);
    for ($i=0;$i<count($metika_data);$i++){
        array_push(
            $metika,array(
                "kinthiId"=>$metika_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[0]['kinthiTitle'],
                "metikaId"=>$metika_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[$i]['metikaTitle'],
                "Status"=>$metika_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("metika"=>$metika), JSON_PRETTY_PRINT);
    echo $myJSON;
}


//Get Chapter data
if (isset($_GET['keyword']) && $_GET['keyword']=="metika"){ 
    $chapter = array();
    $chapter_data = db_get("chapter","WHERE metikaId='".$_GET['id']."' AND Status=1","","ORDER BY chapterTitle ASC","",DB_NAME2);
    $metika_data = db_get("metika","WHERE metikaId='".$_GET['id']."' AND Status=1","","ORDER BY metikaTitle ASC","",DB_NAME2);
    for ($i=0;$i<count($chapter_data);$i++){
        array_push(
            $chapter,array(
                "chapterId"=>$chapter_data[$i]['chapterId'],
                "metikaId"=>$chapter_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[0]['metikaTitle'],
                "chapterTitle"=>$chapter_data[$i]['chapterTitle'],
                "Status"=>$chapter_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("chapter"=>$chapter), JSON_PRETTY_PRINT);
    echo $myJSON;
}

//Get Section data
if (isset($_GET['keyword']) && $_GET['keyword']=="chapter"){
    $section = array();
    $section_data = db_get("section","WHERE chapterId='".$_GET['id']."' AND Status=1","","ORDER BY sectionTitle ASC","",DB_NAME2);
    $chapter_data = db_get("chapter","WHERE chapterId='".$_GET['id']."' AND Status=1","","ORDER BY chapterTitle ASC","",DB_NAME2);
    for ($i=0;$i<count($section_data);$i++){
        array_push(
            $section,array(
                "chapterId"=>$section_data[$i]['chapterId'],
                "chapterTitle"=>$chapter_data[0]['chapterTitle'],
                "sectionId"=>$section_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[$i]['sectionTitle'],
                "Status"=>$section_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("section"=>$section), JSON_PRETTY_PRINT);
    echo $myJSON;
}

//Get Katapheak data
if (isset($_GET['keyword']) && $_GET['keyword']=="section"){
    $kathapheak = array();
    $kathapheak_data = db_get("kathapheak","WHERE sectionId='".$_GET['id']."' AND Status=1","","ORDER BY kathapheakTitle ASC","",DB_NAME2);
    $section_data = db_get("section","WHERE sectionId='".$_GET['id']."' AND Status=1","","ORDER BY sectionTitle ASC","",DB_NAME2);
    for ($i=0;$i<count($kathapheak_data);$i++){
        array_push(
            $kathapheak,array(
                "kathapheakId"=>$kathapheak_data[$i]['kathapheakId'],
                "sectionId"=>$kathapheak_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[0]['sectionTitle'],
                "kathapheakTitle"=>$kathapheak_data[$i]['kathapheakTitle'],
                
                "Status"=>$kathapheak_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("kathapheak"=>$kathapheak), JSON_PRETTY_PRINT);
    echo $myJSON;
}

//GET metra data
if (isset($_GET['keyword']) && $_GET['keyword']=="kathapheak"){
    $metra = array();
    $metra_data = db_get("metra","WHERE kathapheakId='".$_GET['id']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$_GET['id']."' AND Status=1","","ORDER BY kathapheakTitle ASC","",DB_NAME2);
    for ($i=0;$i<count($metra_data);$i++){
        array_push(
            $metra,array(
                "kathapheakId"=>$metra_data[$i]['kathapheakId'],
                "kathapheakTitle"=>$kathapheak_data[0]['kathapheakTitle'],
                "metraId"=>$metra_data[$i]['metraId'],
                "metraTitle"=>$metra_data[$i]['metraTitle'],
                
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode(array("metra"=>$metra), JSON_PRETTY_PRINT);
    echo $myJSON;

}
//Get detail information of a metra
if (isset($_GET['keyword']) && $_GET['keyword']=="metra"){
    $detail = array();
    $metra_data = db_get("metra","WHERE metraId='".$_GET['id']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$metra_data[0]['kathapheakId']."' AND Status=1","","ORDER BY kathapheakId ASC","",DB_NAME2);
    $section_data = db_get("section","WHERE sectionId='".$kathapheak_data[0]['sectionId']."' AND Status=1","","ORDER BY sectionId ASC","",DB_NAME2);
    $chapter_data = db_get("chapter","WHERE chapterId='".$section_data[0]['chapterId']."' AND Status=1","","ORDER BY chapterId ASC","",DB_NAME2);
    $metika_data = db_get("metika","WHERE metikaId='".$chapter_data[0]['metikaId']."' AND Status=1","","ORDER BY metikaId ASC","",DB_NAME2);
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$metika_data[0]['kinthiId']."' AND Status=1","","ORDER BY kinthiId ASC","",DB_NAME2);
    $krom_data = db_get("krom","WHERE kromId='".$kinthi_data[0]['kromId']."'","","ORDER BY kromId ASC","",DB_NAME2);
    $ref_data = db_get("reference","WHERE KromId='".$krom_data[0]['kromId']."'","","ORDER BY KromId ASC","",DB_NAME2);
     #ref array
     $referenceIdkh = array();
     $referenceTitle = array();
     $referenceDate = array();
     $referenceDescription = array();

    for ($i=0;$i<count($ref_data);$i++){
        array_push($referenceIdkh,$ref_data[$i]['referenceIdkh']);
        array_push($referenceTitle,$ref_data[$i]['referenceTitle']);
        array_push($referenceDate,$ref_data[$i]['referenceDate']);
        array_push($referenceDescription,$ref_data[$i]['referenceDescription']);
    }
    for ($i=0;$i<count($krom_data);$i++){
        $kromId = $krom_data[$i]['kromId'];
        $kromDateuse = $krom_data[$i]['kromDateuse'];
        $kromTitle = $krom_data[$i]['kromTitle'];
        $kromDescrtiption = $krom_data[$i]['kromDescrtiption'];
    }
    for ($i=0;$i<count($kinthi_data);$i++){
        $kinthiId = $kinthi_data[$i]['kinthiId'];
        $kinthiTitle = $kinthi_data[$i]['kinthiTitle'];

    }
    for ($i=0;$i<count($metika_data);$i++){
        $metikaId = $metika_data[$i]['metikaId'];
        $metikaTitle = $metika_data[$i]['metikaTitle'];
    }
    for ($i=0;$i<count($chapter_data);$i++){
        $chapterId = $chapter_data[$i]['chapterId'];
        $chapterTitle = $chapter_data[$i]['chapterTitle'];
    }
    for ($i=0;$i<count($section_data);$i++){
        $sectionId = $section_data[$i]['sectionId'];
        $sectionTitle = $section_data[$i]['sectionTitle'];
    }
    for ($i=0;$i<count($kathapheak_data);$i++){
        $kathapheakId = $kathapheak_data[$i]['kathapheakId'];
        $kathapheakTitle = $kathapheak_data[$i]['kathapheakTitle'];
    }
    for ($i=0;$i<count($metra_data);$i++){
        $metraOrder = $metra_data[$i]['metraOrder'];
        $metraTitle = $metra_data[$i]['metraTitle'];
        $metraDescription = $metra_data[$i]['metraDescription'];
    }
    array_push(
        $detail,array(
            "referenceIdkh"=>$referenceIdkh,
            "referenceTitle"=>$referenceTitle,
            "referenceDate"=>$referenceDate,
            "referenceDescription"=>$referenceDescription,
            "kromId"=>$kromId,
            "kromDateuse"=>$kromDateuse,
            "kromTitle"=>$kromTitle,
            "kromDescrtiption"=>$kromDescrtiption,
            "kinthiId"=>$kinthiId,
            "kinthiTitle"=>$kinthiTitle,
            "metikaId"=>$metikaId,
            "metikaTitle"=>$metikaTitle,
            "chapterId"=>$chapterId,
            "chapterTitle"=>$chapterTitle,
            "sectionId"=>$sectionId,
            "sectionTitle"=>$sectionTitle,
            "kathapheakId"=>$kathapheakId,
            "kathapheakTitle"=>$kathapheakTitle,
            "metraOrder"=>$metraOrder,
            "metraTitle"=>$metraTitle,
            "metraDescription"=>$metraDescription
        )
    );
    // Here convert from array into json object
    $myJSON = json_encode(array("detail"=>$detail), JSON_PRETTY_PRINT);
    echo $myJSON;
    viewer_log('view_logs',$_SERVER['REMOTE_ADDR'],0);
}

?>