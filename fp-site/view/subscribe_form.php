<div class="small-12 medium-12 large-12 columns post-block index-post">  
    <?php
    if(isset($_POST['submit'])){
        $cond = array(
            "Email"=>$_POST['Email']
        );
        $res = db_get('subscribers');
        $expired_year = date("Y")+1;
        $expired_date = $expired_year . "/" . date("m")."/".date("d");
        
        $userid = date("y").date("m").date("d").count($res)+1;
        $val = array(
            "UserId"=>$userid,
            "LastName"=>$_POST['LastName'],
            "FirstName"=>$_POST['FirstName'],
            "Sex"=>$_POST['Sex'],
            "PhoneNumber"=>$_POST['PhoneNumber'],
            "Email"=>$_POST['Email'],
            "Password"=> crypt($_POST['Password'],KEY_ENCRYPT),
            "PlainPassword"=>$_POST['Password'],
            "DateJoin"=>date("y/m/d"),
            
            "Status"=>0
        );
        $income = array(
            "UserId"=>$userid,
            "LastName"=>$_POST['LastName'],
            "FirstName"=>$_POST['FirstName'],
            "PhoneNumber"=>$_POST['PhoneNumber'],
            "Fee"=>$_POST['Fee'],
            "DateRegistration"=>date("y/m/d"),
            "DateExpired"=>$expired_date
        );

        if (count(db_get_where('subscribers',$cond))>0){
            echo '
            <div class="w3-center">
            <h4 class="kh-moullight">Email នេះបានប្រើរួចហើយ!<a href="'.DOMAIN.'subscribe_login">ចុចត្រង់នេះដើម្បីធ្វើការចូលអាន</a></h4>
            <h4 class="kh-moullight">This email has been used! Please<a href="'.DOMAIN.'subscribe_login"> click here to login</a></h4>
            </div>
            ';
        }else{
            db_insert('Income',$income);
            db_insert('subscribers',$val);
            
            echo '
            <div class="w3-center">
            <h4 class="kh-moullight">សូមអរគុណសម្រាប់ការចុះឈ្មោះ!</h4>
            <h4 class="kh-moullight">ការចូលមើលវេបសាយរបស់លោកអ្នកនឹងត្រូវផ្អាក</h4>
            <h4 class="kh-moullight">ប្រសិនបើលោកអ្នកមិនបង់វិភាគទាន ក្នុងរយៈពេល១៤ថ្ងៃខាងមុខ!</h4>
            <h4 "><a class="kh-moullight" href="'.DOMAIN.'subscribe_login">សូមចុចត្រង់នេះដើម្បីចូលទៅកាន់វេបសាយ!</a></h4>
            <h4 class="kh-moullight">សូមអរគុណ!</h4>
            </div>
            ';    
        }
        
        
    }else{
    ?>
    <div class="w3-center"><h4 class="kh-moullight">តារាងចុះឈ្មោះ</h4></div>
    <form action="<?=DOMAIN?>subscribe_form" method="post">
        <div class="row">
            <div class="small-3 columns">
                    <label for="lastname" class="text-right middle" >នាមត្រកូល/Last Name</label>
            </div>
            <div class="small-9 columns">
                    <input type="Text" name="LastName" id="lastname" placeholder="នាមត្រកូល/Last Name" required>
                </div>
        </div>
        <div class="row">
            <div class="small-3 columns">
                    <label for="firstname" class="text-right middle" >នាមខ្លួន/First Name</label>
            </div>
            <div class="small-9 columns">
                    <input type="Text" name="FirstName" id="firstname" placeholder="នាមខ្លួន/First Name" required>
                </div>
        </div>
        <div class="row">
            <div class="small-3 columns">
                    <label for="sexm" class="text-right middle" >ភេទ/Sex</label>
            </div>
            <div class="small-9 columns">
                    <input type="radio" name="Sex" id="sexm" value="M" required><label for="sexm">ប្រុស/Male</label>
                    <input type="radio" name="Sex" id="sexf" value="F"><label for="sexf">ស្រី/Female</label>
                </div>
        </div>
        <div class="row">
            <div class="small-3 columns">
                    <label for="sexm" class="text-right middle" >តម្លៃវិភាគទាន/Contribution Fee</label>
            </div>
            <div class="small-9 columns">
                    <input type="radio" name="Fee" id="student" value="10" required><label for="student">សិស្សនិសិ្សត ១០$/១ឆ្នាំ</label>
                    <input type="radio" name="Fee" id="professional" value="15"><label for="professional">មន្ត្រីរាជការ/អ្នកប្រកបវិជ្ជាជីវះ ១៥$/១ឆ្នាំ</label>
                </div>
        </div>
        <div class="row">
            <div class="small-3 columns">
                    <label for="phonenumber" class="text-right middle" >លេខទូរស័ព្ទ/Phone Number</label>
            </div>
            <div class="small-9 columns">
                    <input type="Text" name="PhoneNumber" id="phonenumber" placeholder="លេខទូរស័ព្ទ/Phone Number" required>
                </div>
        </div>
        <div class="row">
            <div class="small-3 columns">
                    <label for="email" class="text-right middle" >អ៊ីម៉ែល/Email</label>
            </div>
            <div class="small-9 columns">
                    <input type="Email" name="Email" id="email" placeholder="អ៊ីម៉ែល/Email" required>
                </div>
        </div>
        <div class="row">
           <div class="small-3 columns">
                <label for="password" class="text-right middle" >លេខសម្ងាត់/Password</label>
           </div>
           <div class="small-9 columns">
                <input type="password" name="Password" id="password" placeholder="លេខសម្ងាត់/Password" required>
            </div>
       </div>
       <div class="row">
           <div class="small-3 columns">
                <label for="ConfirmPassword" class="text-right middle" >បញ្ជាក់លេខសម្ងាត់/<br>Confirm Password</label>
           </div>
           <div class="small-9 columns">
                <input type="password" name="ConfirmPassword" id="ConfirmPassword" onkeyup="confirmpassword('password','ConfirmPassword','ConfirmRes','submitbutton')" placeholder="បញ្ជាក់លេខសម្ងាត់/Confirm Password" required>
            </div>
            <div class="small-3 columns">
                
           </div>
           <div class="small-9 columns">
                <b class="w3-text-red" id="ConfirmRes"></b>
            </div>
            
       </div>
       <div class="row">     
       <div class="w3-col s12 m12 l12 y-scroll w3-border" >
           <?php
           include 'term_and_condition.php';
           ?>
       </div>
       </div>
       <div class="row">
       <input type="checkbox" id="term" required>
       <label for="term" style="font-size:16px;">ខ្ញុំបានអាន និងយល់ព្រមគ្រប់លក្ខខណ្ឌដែលបានបញ្ជាក់ខាងលើនេះ។</label>
        </div>
       <div class="row text-right">
                <input type="submit" name="submit" id="submitbutton" value="ចុះឈ្មោះ/Subscribe" class="button success small">
        </div>

    </form>
    <?php
    }
    ?>
</div>