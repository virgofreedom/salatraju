<div class="small-12 columns big-menu w3-padding-large ">
<?php
$permission = array("Admin","Superuser","LegalUpdate","AdminLegalUpdate");
$db_name = "salatraju_legal_update";
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    $db_name = "salatraju_legal_update";
    $legal_data = db_get("publications","","","","",$db_name);
?>

    <form action="lg_update_meteka" method="GET">
            <div class="row">
                <div class="small-12">
                លេខបច្ចុប្បន្នភាគគតិយុត្តកម្ពុជាប្រចាំសប្តាហ៍:*
                    <label for="lgupdate_number" id="legal_div">
                    
                        <select id="lgupdate_number" name="lgupdate_number" class="w3-input" onchange="toinput('legal_div','lgupdate_number','new');" required>
                            <option value=""></option>
                            <option value="new">Add New</option>
                            <?php
                            for($i=0;$i<count($legal_data);$i++){
                                echo '<option value="'.$legal_data[$i]['Id'].'">'.$legal_data[$i]['Title'].'</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <input type="submit" class="w3-button w3-right w3-green" value="Next"/>
                    
                </div>  
            </div>
    </form>

<?php
}
?>
</div>
