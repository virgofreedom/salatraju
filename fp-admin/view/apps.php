
<?php
$permission = array("Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
?>
<div class="small-12 columns big-menu">
    <form class="w3-container" action="<?=THIS_PAGE?>" method="POST">
        <label class="w3-text-blue"><b>Apps Name</b></label>
        <input class="w3-input w3-border" type="text" name="AppName" required>
    
        <label class="w3-text-blue"><b> Apps Status</b></label>
        <Select name="AppStatus" required>
            <option value=""></option>
            <option value="0"> Developping</option>
            <option value="1"> Published</option>
        </Select>
        <Select name="AppLogin" required>
            <option value=""></option>
            <option value="0"> Without Login Page</option>
            <option value="1"> With Login Page</option>
        </Select>
        <button class="w3-btn w3-blue" name="AppAdd">Add New</button>
    </form>
</div>
<div class="small-12 columns big-menu">
    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
            <thead>
            <tr><th>Id</th><th>Apps Name</th><th>Apps Status</th><th>App Login</th></tr>
            </thead>
            <tbody>
<?php
    $data_app = db_get("apps");
    if (count($data_app)  == 0){
        echo '<tr><td colspan="4"><center>No Apps</center></td></tr>';
    }else{
        for($i=0;$i<count($data_app);$i++){
            if ($data_app[$i]["AppStatus"] == 0){
                $label_status = "Developping";
                $label_action = "Published?";
                $status = 1;
            }else{
                $label_status = "Published";
                $label_action = "Put to Developping?";
                $status = 0;
            }
            if ($data_app[$i]["AppLogin"] == 0){
                $label_login = "Without login";
                $label_action_login = "Put login?";
                $login = 1;
            }else{
                $label_login = "With login";
                $label_action_login = "Take off login?";
                $login = 0;
            }
            echo '
            <tr><td>'.$data_app[$i]["Id"].'</td><td>'.$data_app[$i]["AppName"].' 
                <a onclick="msgbox('."'Please enter the app name','".THIS_PAGE."','_self','prompt','id=".$data_app[$i]["Id"]."','appname'".')">Edit</a> |
                <a onclick="msgbox('."'Do you really want to delete this app?','".THIS_PAGE."?delid=".$data_app[$i]["Id"]."','_self','yesno'".')">Delete</a>
            </td>
            <td>'.$label_status.' want to <a href="'.THIS_PAGE.'?status='.$status.'&id='.$data_app[$i]["Id"].'">'.$label_action.'</a>
            </td><td>'.$label_login.' want to <a href="'.THIS_PAGE.'?login='.$login.'&id='.$data_app[$i]["Id"].'">'.$label_action_login.'</a></td></tr>
            ';
        }
    }
?>
            </tbody>
    </table>
</div>
<?php
    if (isset($_POST['AppAdd'])){
        $data = array(
            "AppName"=>$_POST["AppName"],
            "AppStatus"=>$_POST["AppStatus"],
            "AppLogin"=>$_POST["AppLogin"]
        );
        db_insert("apps",$data);
        header("Location: ".THIS_PAGE);
        
    }elseif(isset($_GET['status'])){
        
        $cond = array("Id"=>$_GET['id']);
        $data = array("AppStatus"=>$_GET['status']);
        db_update("apps",$data,$cond);
        header("Location: ".THIS_PAGE);

    }elseif(isset($_GET['login'])){

        $cond = array("Id"=>$_GET['id']);
        $data = array("AppLogin"=>$_GET['login']);
        db_update("apps",$data,$cond);
        header("Location: ".THIS_PAGE);

    }elseif(isset($_GET['appname'])){

        $cond = array("Id"=>$_GET['id']);
        $data = array("AppName"=>$_GET['appname']);
        db_update("apps",$data,$cond);
        header("Location: ".THIS_PAGE);

    }elseif(isset($_GET['delid'])){

        $cond = array("Id"=>$_GET['delid']);
        db_delete("apps",$cond);
        header("Location: ".THIS_PAGE);

    }
    
}
?>
