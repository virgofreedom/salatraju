<?php

function menu_admin_ul($ul_class="",$li_sub_class="",$ul_sub_class="",$back_btn="",$data=""){
    echo '<ul class="'.$ul_class.'" '.$data.'>';
  $res = db_get('menu_admin','','GROUP BY MenuTitle',"ORDER BY Id");
  for ($i=0;$i < count($res);$i++){
    if ($_SESSION['infos']['role'] == $res[$i]['Permission']){
      if ($res[$i]['Type'] == "Link"){
        echo '
          <li><a href="'.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a></li>
        ';
      }else{
      echo '
      <li class="'.$li_sub_class.'">
        <a href="#">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a>
        <ul class="'.$ul_sub_class.'">'.$back_btn;

        $res_sub = db_get('menu_admin','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'"');
        for ($y=0;$y < count($res_sub);$y++){
            echo '<li><a href="'.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
        }
        echo '</ul>
      </li>
      ';
      }
    }else if ($res[$i]['Permission'] == NULL || $res[$i]['Permission'] == ''){
      if ($res[$i]['Type'] == "Link"){
        echo '
          <li><a href="'.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a></li>
        ';
      }else{
      echo '
      <li class="'.$li_sub_class.'">
        <a href="#">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a>
        <ul class="'.$ul_sub_class.'">'.$back_btn;

        $res_sub = db_get('menu_admin','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'"');
        for ($y=0;$y < count($res_sub);$y++){
            echo '<li><a href="'.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
        }
        echo '</ul>
      </li>
      ';
      }
    }
  }
echo '</ul>';
}
function admin_menu_div($class="",$sub_class="",$btn_class="",$a_class="",$drop_content_class="",$data=""){
    echo '<div class="'.$class.'">';
    
$res = db_get('menu_admin','','GROUP BY MenuTitle',"ORDER BY Id");
  for ($i=0;$i < count($res);$i++){
      echo '<div class="'.$sub_class.'">';
      if ($res[$i]['Type'] == "Link"){
            echo '
                <a class="'.$a_class.'" href="'.DOMAIN.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                </a>
                ';
      }else{
              echo '
                <button class="'.$btn_class.'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</button>
                    <div class="'.$drop_content_class.'">
                ';
                for ($y=0;$y < count($res_sub);$y++){
                    echo '<a class="'.$a_class.'" href="'.DOMAIN.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
                }
                echo '
                    </div>
                ';
          
        
      }
    echo '</div>';
  }
echo '</div>';

}

function w3_accordion_menu(){
// TO USE THIS FUNCTION MUST HAVE W3.CSS AND TABLE MENU_ADMIN IN DATABASE
$res = db_get('menu_admin','Where Permission LIKE "%'.$_SESSION['infos']['role'].'%"','GROUP BY MenuTitle',"ORDER BY Id");
$a_class = "w3-bar-item w3-block w3-padding";
$btn_class = "w3-button w3-block w3-left-align";
$drop_content_class = "w3-container w3-hide";
  for ($i=0;$i < count($res);$i++){
    $res_sub = db_get('menu_admin','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'" AND Permission LIKE "%'.$_SESSION['infos']['role'].'%"');

      if ($res[$i]['Type'] == "Link"){
            
            echo '
                <a class="'.$a_class.'" href="'.VIRTUAL_PATH."index.php/".$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                </a>
                ';
      }else{
              echo '
                <button class="'.$btn_class.'" onclick="myFunction('."'virgo".$i."'".')">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</button>
                    <div class="'.$drop_content_class.'" id="virgo'.$i.'">
                ';
                for ($y=0;$y < count($res_sub);$y++){
                    echo '<a class="'.$a_class.'" href="'.VIRTUAL_PATH. "index.php/".$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
                }
                echo '
                    </div>
                ';
          
        
      }
  
  }
echo '<script>
function myFunction(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else { 
        x.className = x.className.replace(" w3-show", "");
    }
}
</script>';

}

function fp_menu_ul($ul_class="",$li_sub_class="",$ul_sub_class="",$back_btn="",$data=""){
    echo '<ul class="'.$ul_class.'" '.$data.'>';
  $res = db_get('menu','','GROUP BY MenuTitle',"ORDER BY Id");
  for ($i=0;$i < count($res);$i++){
    if ($res[$i]['Permission'] == NULL || $res[$i]['Permission'] == ''){
      if ($res[$i]['Type'] == "Link"){
        echo '
          <li><a href="'.DOMAIN.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a></li>
        ';
      }else{
      echo '
      <li class="'.$li_sub_class.'">
        <a href="#">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</a>
        <ul class="'.$ul_sub_class.'">'.$back_btn;

        $res_sub = db_get('menu_admin','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'"');
        for ($y=0;$y < count($res_sub);$y++){
            echo '<li><a href="'.DOMAIN.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
        }
        echo '</ul>
      </li>
      ';
      }
    }
  }
echo '</ul>';
}


function fp_menu_div($class="",$sub_class="",$btn_class="",$a_class="",$drop_content_class="",$lang="both",$home="yes",$data=""){
    echo '
        <div class="'.$class.'">';
    if ($home=="yes"){
        echo '<div class="'.$sub_class.'">';
                    if ($lang == "both"){
                        echo '
                        <a class="'.$a_class.'" href="'.DOMAIN.'home">ទំព័រដើម
                        <br><span class="small">Home</span></a>
                        ';
                    }else if ($lang == "kh"){
                        echo '
                        <a class="'.$a_class.'" href="'.DOMAIN.'home">ទំព័រដើម</a>
                        ';
                    }else{
                        echo '
                        <a class="'.$a_class.'" href="'.DOMAIN.'home">Home</a>
                        ';
                    }
        echo '</div>';
    }
  $res = db_get('menu','','GROUP BY MenuTitle',"ORDER BY Id");
  for ($i=0;$i < count($res);$i++){
      echo '<div class="'.$sub_class.'">';
      if ($res[$i]['Type'] == "Link"){
          
          if ($lang == "both"){
                echo '
                <a class="'.$a_class.'" href="'.DOMAIN.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                <br><span class="small">'.$res[$i]['EnTitle'].'</span>
                </a>
                ';
          }else if ($lang == "kh"){
            echo '
                <a class="'.$a_class.'" href="'.DOMAIN.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                </a>
                ';
          }else{
            echo '
                <a class="'.$a_class.'" href="'.DOMAIN.$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['EnTitle'].'
                </a>
                ';
          }
        
      }else{
          $res_sub = db_get('menu','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'"');
          if ($lang == "both"){
                echo '
                <button class="'.$btn_class.'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                <br><span class="small">'.$res[$i]['EnTitle'].'</span>
                </button>
                    <div class="'.$drop_content_class.'">
                ';
                
                for ($y=0;$y < count($res_sub);$y++){
                    echo '<a class="'.$a_class.'" href="'.DOMAIN.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'
                    <br><span class="small">'.$res[$i]['EnTitle'].'</span>
                    </a></li>';
                }
                echo '
                    </div>
                ';
          }else if($lang == "kh"){
                echo '
                <button class="'.$btn_class.'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'</button>
                    <div class="'.$drop_content_class.'">
                ';
                
                for ($y=0;$y < count($res_sub);$y++){
                    echo '<a class="'.$a_class.'" href="'.DOMAIN.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
                }
                echo '
                    </div>
                ';
          }else{
              echo '
                <button class="'.$btn_class.'">'.$res[$i]['Icon'].$res[$i]['EnTitle'].'</button>
                    <div class="'.$drop_content_class.'">
                ';
                
                for ($y=0;$y < count($res_sub);$y++){
                    echo '<a class="'.$a_class.'" href="'.DOMAIN.$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkEnTitle'].'</a></li>';
                }
                echo '
                    </div>
                ';
          }
        
      }
    echo '</div>';
  }
echo '</div>';

}


function w3_menu(){
  // TO USE THIS FUNCTION MUCH HAVE W3.CSS AND TABLE MENU_ADMIN IN DATABASE
  $res = db_get('menu_admin','Where Permission LIKE "%'.$_SESSION['infos']['role'].'%"','GROUP BY MenuTitle',"ORDER BY Id");
  $a_class = "w3-bar-item w3-button w3-padding";
  $btn_class = "left";
  $drop_content_class = "w3-container w3-hide";
    for ($i=0;$i < count($res);$i++){
      $res_sub = db_get('menu_admin','WHERE MenuTitle="'.$res[$i]['MenuTitle'].'" AND Permission LIKE "%'.$_SESSION['infos']['role'].'%"');
  
        if ($res[$i]['Type'] == "Link"){
              
              echo '
                  <a class="'.$a_class.'" href="'.VIRTUAL_PATH."index.php/".$res[$i]['Link'].'">'.$res[$i]['Icon'].$res[$i]['MenuTitle'].'
                  </a>
                  ';
        }else{
                echo '
                  
                  ';
                  for ($y=0;$y < count($res_sub);$y++){
                      echo '<a class="'.$a_class.'" href="'.VIRTUAL_PATH. "index.php/".$res_sub[$y]['Link'].'">'.$res_sub[$y]['LinkTitle'].'</a></li>';
                  }
        }
    
    }

  
}
?>