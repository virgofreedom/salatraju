<div class="small-12 columns big-menu">
<?php
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    $kinthi = db_get("kinthi","Where Status=-1","","","",DB_NAME2);
    $methika = db_get("metika","Where Status=-1","","","",DB_NAME2);
    $chapter = db_get("chapter","Where Status=-1","","","",DB_NAME2);
    $section = db_get("section","Where Status=-1","","","",DB_NAME2);
    $kathapheak = db_get("kathapheak","Where Status=-1","","","",DB_NAME2);
    if(isset($_GET['rest'])){
        //Do the chage the Status to 1
        $cond = array($_GET['step']."Id"=>$_GET['rest']);
        $data = array(
        "Status"=> 1
        );
        db_update($_GET['step'],$data,$cond,DB_NAME2); 
        msgbox('ពាក្យបានដាក់បញ្ចូលទៅក្នុងបញ្ជីវិញហើយ។',THIS_PAGE);
    }
    if(isset($_GET['temp'])){
        //Do the chage the Status to -2
        $cond = array($_GET['step']."Id"=>$_GET['temp']);
        $data = array(
        "Status"=> -2
        );
        db_update($_GET['step'],$data,$cond,DB_NAME2); 
        msgbox('ពាក្យនេះបានដាក់មិនឲ្យមើលឃើញឡើយ។',THIS_PAGE . "?step=".$_GET['step']);
    }
    if(isset($_GET['perm'])){
        $cond = array($_GET['step']."Id"=>$_GET['perm']);
        db_delete($_GET['step'],$cond,DB_NAME2);
        msgbox('ពាក្យនេះបានលុបចេញពី database រួចរាក់ហើយ។',THIS_PAGE. "?step=".$_GET['step']);
    }
    if(isset($_GET['step'])){

        if($_GET['step'] == "kinthi"){
            $title = "គន្ថី";
        }elseif($_GET['step'] == "metika"){
            $title = "មាតិកា";
        }elseif($_GET['step'] == "chapter"){
            $title = "ជំពូក";
        }elseif($_GET['step'] == "section"){
            $title = "ផ្នែក";
        }elseif($_GET['step'] == "kathapheak"){
            $title = "កថាភាគ";
        }
?>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
            <th>Id</th><th>ចំណងជើង<?=$title?></th><th></th>
        </tr>
    </thead>
    <tbody>
<?php
        $res = db_get($_GET['step'],"Where Status=-1","","","",DB_NAME2);
        for($i=0;$i<count($res);$i++){
            echo'
            <tr>
                <td>'.$res[$i][$_GET['step'].'Id'].'</td><td>'.$res[$i][$_GET['step'].'Title'].'</td><td>
                <a href="?step='.$_GET['step'].'&rest='.$res[$i][$_GET['step'].'Id'].'">Restore</a> | 
                <a href="?step='.$_GET['step'].'&temp='.$res[$i][$_GET['step'].'Id'].'">Temporary delete</a> | 
                <a onclick="msgbox('."'តើពិតជាចង់លុបវាមែន?បើលុបហើយមិនអាចយកមកវិញបានទេ!','".THIS_PAGE."?step=".$_GET['step']."&perm=".$res[$i][$_GET['step'].'Id']."','_self','yesno'".');" href="#">Permanantly delete</a></td>
            </tr>
            ';
        }
?>
    </tbody>
</table>
    <a href="<?=THIS_PAGE?>" class="button">Go back</a>
<?php
    }else{
?>

<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
            <th>ផ្នែគ</th><th>ចំនួន</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="?step=kinthi">គន្ថី</a></td><td><?php echo count($kinthi);?></td>
        </tr>
        <tr>
            <td><a href="?step=metika">មាតិកា</a></td><td><?php echo count($methika);?></td>
        </tr>
        <tr>
            <td><a href="?step=chapter">ជំពូក</a></td><td><?php echo count($chapter);?></td>
        </tr>
        <tr>
            <td><a href="?step=section">ផ្នែក</a></td><td><?php echo count($section);?></td>
        </tr>
        <tr>
            <td><a href="?step=kathapheak">កថាភាគ</a></td><td><?php echo count($kathapheak);?></td>
        </tr>
    </tbody>
</table>
<?php
    }
?>
<div class="small-12 columns big-menu">
<?php
$result = db_get("annotation_deleted_list");
echo '<div class="small-12 medium-12 large-12">';
Pager_Nav_Admin($result,5,'actions','annotation_req_list.php');
echo '</div>';
?>
</div>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr><th>លេខសម្គាល់ខ្លួន</th><th>ឈ្មោះ</th><th>សកម្មភាព</th><th>ផ្នែក</th><th>ចំណងជើង</th><th>កាលបរិច្ឆេទ</th></tr>
    </thead>
    <tbody>
<?php
    $data = Pager_db("annotation_deleted_list","","","ORDER BY Date DESC","5","actions");
    
    for ($i=0;$i<count($data);$i++){
        $username = db_get("fp_users","WHERE Id='".$data[$i]['UserId']."'");
        echo '<tr>';
        echo '<td>'.$data[$i]['UserId'] .'</td><td>'.$username[0]['LastName'].' '.$username[0]['FirstName'].'</td><td>'.$data[$i]['Action'].'</td><td>'.
        $data[$i]['ItemName'].'</td><td>'.$data[$i]['ItemTitle'].'</td><td>'.$data[$i]['Date'].'</td>';
        echo '</tr>';
    }
?>
    </tbody>
</table>
</div>
<?php
}

?>


