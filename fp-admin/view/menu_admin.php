<div class="small-12 columns big-menu">
<?php
$permission = array("Admin","Superuser","Annotation");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    if (isset($_GET['Id'])){
        $id = $_GET['Id'];
        $cond = array("Id"=>$_GET['Id']);
        $res = db_get_where('menu_admin',$cond);
        $icon = htmlspecialchars_decode($res[0]['Icon'],ENT_QUOTES);
        $title=$res[0]['MenuTitle'];
        $ltitle = $res[0]['LinkTitle'];$link=$res[0]['Link'];
        $type=$res[0]['Type'];$permission = $res[0]['Permission'];
        $old_id = 'Order Id<input type="text" name="NewId" value="'.$id.'"/>';
        $btn_name = "update";
    }else{
        $id ="";$old_id="";
        $icon = "";$title="";$ltitle="";$link="";$type="";$permission="";$btn_name="addnew";
    }
    ?>
    <div class="row">
        <div class="small-12 medium-4 large-4 medium-offset-1 large-offset-1 columns">
                <div class="login-panel panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Create new menu for admin panel</h3>
                        </div>
                        <div class="panel-body" id="top">
                                <form action="<?=THIS_PAGE?>" method="POST">
                                <input type="hidden" name="Id" value="<?=$id?>"/>
                                <?=$old_id?>
                                Menu Icon
                                <input type="text" name="Icon" value='<?=$icon?>'/>
                                Menu Title 
                                <input type="text" name="Title" value="<?=$title?>"/>
                                Link Title
                                <input type="text" name="Ltitle" value="<?=$ltitle?>"/>
                                Link
                                <input type="text" name="Link" value="<?=$link?>"/>
                                Menu Type
                                <select name="Type" id="Type">
                                    <option value="<?=$type?>"><?=$type?></option>
                                    <option value="Link">Link</option>
                                    <option value="Menu">Menu</option>
                                </select>
                                Permission
                                <input type="text" name="Permission" value="<?=$permission?>"/>
                                <input type="submit" name="<?=$btn_name?>" class="button success" value="យល់ព្រម"/>
                                </form>
                        </div>
                </div>
        </div>
    <?php
    if(isset($_POST['addnew'])){
        $icon = $_POST['Icon'];
        $title = htmlspecialchars($_POST['Title'],ENT_QUOTES) ;
        $ltitle = htmlspecialchars($_POST['Ltitle'],ENT_QUOTES);
        $link = htmlspecialchars($_POST['Link'],ENT_QUOTES);
        $type = htmlspecialchars($_POST['Type'],ENT_QUOTES);
        $permission = htmlspecialchars($_POST['Permission'],ENT_QUOTES);
        $val = array(
            "Icon"=>$icon,
            "MenuTitle"=>$title,
            "LinkTitle"=>$ltitle,
            "Link"=>$link,
            "Type"=>$type,
            "Permission"=>$permission
        );
        db_insert('menu_admin',$val);
        header("Location: ". THIS_PAGE);
    }else if(isset($_POST['update'])){
        $id = $_POST['Id'];$newid = $_POST['NewId'];
        $icon = $_POST['Icon'];
        $title = htmlspecialchars($_POST['Title'],ENT_QUOTES) ;
        $ltitle = htmlspecialchars($_POST['Ltitle'],ENT_QUOTES);
        $link = htmlspecialchars($_POST['Link'],ENT_QUOTES);
        $type = htmlspecialchars($_POST['Type'],ENT_QUOTES);
        $permission = htmlspecialchars($_POST['Permission'],ENT_QUOTES);
        $val = array(
            "Id"=>$newid,
            "Icon"=>$icon,
            "MenuTitle"=>$title,
            "LinkTitle"=>$ltitle,
            "Link"=>$link,
            "Type"=>$type,
            "Permission"=>$permission
        );
        $cond = array("Id"=>$id);
        db_update('menu_admin',$val,$cond);
        header("Location: ". THIS_PAGE);
    }else if (isset($_GET['delid'])){
        $cond = array("Id"=>$_GET['delid']);
        db_delete('menu_admin',$cond);
        header("Location: ". THIS_PAGE);
    }
    ?>
    <div class="w3-row">
        <div class="w3-col s12 m12 l12" style="overflow: scroll;">
        <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable " >
            <thead>
                <tr class="danger"><th colspan="8" class="center">Menu table</th></tr>
                <tr>
                <th>Id</th><th>Icon</th><th>Title</th><th>Link Title</th><th>Link</th><th>Type</th><th>Permission</th><th></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $res = db_get('menu_admin');
                for ($i=0;$i<count($res);$i++){
                    echo '
                    <tr>
                    <td>'.$res[$i]['Id'].'</td><td>'.$res[$i]['Icon'].'</td><td>'.$res[$i]['MenuTitle'].'</td>
                    <td>'.$res[$i]['LinkTitle'].'</td><td><a href="'.$res[$i]['Link'].'">'.$res[$i]['Link'].'</a></td><td>'.$res[$i]['Type'].'</td>
                    <td>'.$res[$i]['Permission'].'</td>
                    <td>
                    <a href="'.THIS_PAGE.'?Id='.$res[$i]['Id'].'">Edit</a> | 
                    <a href="#" onclick="msgbox('."'Do you want to delete this menu?','?delid=".$res[$i]['Id']."','_self','yesno'".')" >Delete</a>
                    </td>
                    </tr>
                    ';
                }
                ?>
            </tbody>
            
        </table>
        </div>
        </div>
    </div>
<?php
}
?>
</div>