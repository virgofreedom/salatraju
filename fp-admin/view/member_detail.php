<?php
include PHYSICAL_PATH.'library/admin.php';

if (isset($_GET['id'])){#get all the information form the member id

    
    $res = db_get('registration','Where Id="'.$_GET['id'].'"');
    for ($i=0;$i<count($res);$i++){
        $Name = $res[$i]['KhName'];$Name_Latin = $res[$i]['EnName'];$Sex = $res[$i]['Sex'];$Dob = $res[$i]['Dob'];
        $Pob = $res[$i]['Pob'];$Job = $res[$i]['Job'];$Company = $res[$i]['Company'];$Address = $res[$i]['Address'];
        $Phone = $res[$i]['Phone'];$Email = $res[$i]['Email'];$Dad_Name = $res[$i]['DadName'];
        $Dad_Job = $res[$i]['DadJob'];$Mom_Name = $res[$i]['MomName'];$Mom_Job = $res[$i]['MomJob'];$Spouse_Name = $res[$i]['SpouseName'];
        $Spouse_Job = $res[$i]['SpouseJob'];$Fam_Address = $res[$i]['FamAdd'];$Fam_Phone = $res[$i]['FamPhone'];
        $Fam_Email = $res[$i]['FamEmail'];$Contact_Name = $res[$i]['ContactName'];$Relation = $res[$i]['Relationship'];
        $Contact_Phone = $res[$i]['ContactPhone'];$Contact_Email = $res[$i]['ContactEmail'];$How = $res[$i]['How'];
        $Why = $res[$i]['Why'];$Use = $res[$i]['Used'];$Think = $res[$i]['Think'];$status = $res[$i]['Status'];
        $img_path = VIRTUAL_PATH."ckeditor/plugins/imageuploader/uploads/members/".$res[$i]['Img'];
    }
}
?>
<!-- preview form here -->

<div class="small-12 columns big-menu">
    
    <div class="small-6 columns">
        <div style="width:128px;">
            <img src="<?=VIRTUAL_PATH?>img/logo_salatraju.png" alt="logo salatraju">
            <p style="text-align: center;fon-size:small;line-height:0.5;">សមាគមសាលាត្រាជូ</p>
            <p style="text-align: center;font-size:10px;line-height:0.5;">SALATRAJU ASSOCIATION</p>
        </div>
        
    </div>
    <div class="small-6 columns">
        <p style="text-align: center;">ព្រះរាជាណាចក្រកម្ពុជា</p>
        <p style="text-align: center;">ជាតិ​ សាសនា ព្រះមហាក្សត្រ</p>
    </div>
    
    <div class="row">
        <div class="small-12">
            <p style="text-align: center;">ពាក្យសុំចូលជាសមាជិក</p>    
        </div>
    </div>
    <div class="row">
        <div class="right">
            <img src="<?=$img_path?>" alt="Your Pic is here" style="width:128px;">
        </div>
        
    </div>
<h5>១.ប្រវត្តិរូបផ្ទាល់ខ្លួន</h5>
    <p>ខ្ញុំបាទ/នាងខ្ញុំឈ្មោះ <b><?=$Name?></b> អក្សរឡាតាំង <b><?=$Name_Latin?></b> ភេទ <b><?=$Sex?></b></p>
    <p>ថ្ងៃខែឆ្នាំកំណើត <b><?=$Dob?></b> ទីកន្លែងកំណើត <b><?=$Pob?></b></p>
    <p>មុខរបរបច្ចុប្បន្ន <b><?=$Job?></b> អង្គភាព <b><?=$Company?></b></p>
    <p>អាសយដ្ឋានបច្ចុប្បន្ន <b><?=$Address?></b> </p>
    <p>លេខទូរសព្ទ <b><?=$Phone?></b> អ៊ីម៉ែល <b><?=$Email?></b></p>
    <h5>២.ស្ថានភាពគ្រួសារ</h5>
    <p>ឪពុកឈ្មោះ <b><?=$Dad_Name?></b> មុខរបរ <b><?=$Dad_Job?></b></p>
    <p>ម្តាយឈ្មោះ <b><?=$Mom_Name?></b> មុខរបរ <b><?=$Mom_Job?></b></p>
    <p>ប្រពន្ធ/ប្តីឈ្មោះ <b><?=$Spouse_Name?></b> មុខរបរ <b><?=$Spouse_Job?></b></p>
    <p>អាសយដ្ឋានបច្ចុប្បន្ន <b><?=$Fam_Address?></b></p>
    <p>លេខទូរសព្ទ <b><?=$Fam_Phone?></b> អ៊ីម៉ែល <b><?=$Fam_Email?></b></p>
    <p>អ្នកដែលអាចទាក់ទងបានករណីចាំបាច់ ឈ្មោះ <b><?=$Contact_Name?></b> ត្រូវជា <b><?=$Relation?></b></p>
    <p>លេខទូរសព្ទ <b><?=$Contact_Phone?></b> អ៊ីម៉ែល <b><?=$Contact_Email?></b></p>
    <h5>៣.ព័ត៌មានពាក់ព័ន្ធនឹងសមាគម</h5>
    <p>-តើអ្នកស្គាល់សមាគមសាលាត្រាជូតាមរយៈអ្វី?</p>
    <p><b><?=$How?></b></p>
    <p>-ហេតុអ្វីបានជាអ្នកចាប់អារម្មណ៍ចូលជាសមាជិកសមាគមសាលាត្រាជូ??</p>
    <p><b><?=$Why?></b></p>
    <p>-តើអ្នកធ្លាប់ទទួលបានព័ត៌មានអ្វីខ្លះពីសមាគមសាលាត្រាជូ??</p>
    <p><b><?=$Use?></b></p>
    <p>-តើអ្នករំពឹងថានឹងទទួលបានអ្វីខ្លះពីសមាគមសាលាត្រាជូ??</p>
    <p><b><?=$Think?></b></p>
    <p style="text-indent:10%">ខ្ញុំសូមធានាអះអាងថា ប្រវត្តិរូបរបស់ខ្ញុំ និងព័ត៌មានខាងលើ គឺពិតជាត្រឹមត្រូវប្រាកដមែន ក្នុងករណី​ខុស​​ពី​ការពិត ខ្ញុំសូមទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។</p>
    <p>សូម លោកប្រធានសមាគមសាលាត្រាជូ មេត្តាអនុញ្ញាតឱ្យរូបខ្ញុំបាទ/នាងខ្ញុំ ចូលជា៖ <b><?=$status?></b> របស់សមាគមសាលាត្រាជូ ដោយក្តីអនុគ្រោះបំផុត។</p>
    <p>ខ្ញុំបាទ/នាងខ្ញុំ សូមសន្យាថា គោរព និងអនុវត្តតាមលក្ខន្តិកៈ និងបទបញ្ជាផ្ទៃ​ក្នុងរបស់សមាគម​សាលា​ត្រាជូ។</p>
    <a href="member_edit?Id=<?=$_GET['Id']?>" class="button">Edit</a>
</div>

<!-- End Preview Form -->