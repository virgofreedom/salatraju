<script>
function myFunction(id) {
  var x = document.getElementById(id);
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}
</script>
<?php
if (isset($_POST['createmenu'])){
    $arr_items_id = explode(";",$_POST['menuitems']);
    
    if (is_numeric($_POST['menutitle'])){
        for ($i=0; $i<count($arr_items_id);$i++){
            $data = array(
                "PostId"=> $arr_items_id[$i],
                "DropId"=> $_POST['menutitle'],
                "Type"=>2
            );
            db_insert("menu",$data);
        }
        header("Location: ".THIS_PAGE);
    }
}elseif(isset($_POST['createnew'])){
    $res_menu_title = db_get('menu','',"GROUP BY DropId");
    $dropid = count($res_menu_title) + 1;
    //define data menu title
    $data_menu = array(
        "PostId"=> 0,
        "DropId"=> $dropid,
        "MenuTitle"=>$_POST['KhmerTitle'],
        "EnTitle"=>$_POST['EnglishTitle'],
        "Type"=>1
    );
    db_insert("menu",$data_menu);
    $arr_items_id = explode(";",$_POST['menuitems']);
    for ($i=0; $i<count($arr_items_id);$i++){
        $data = array(
            "PostId"=> $arr_items_id[$i],
            "DropId"=> $dropid,
            "Type"=>2
        );
        db_insert("menu",$data);
    }
    header("Location: ".THIS_PAGE);
}elseif(isset($_GET['delid'])){
    $cond = array("Id"=>$_GET['delid']);
    db_delete("menu",$cond);
    header("Location: ".THIS_PAGE);
}elseif(isset($_GET['delgroup'])){
    $cond = array("DropId"=>$_GET['delgroup']);
    db_delete("menu",$cond);
    header("Location: ".THIS_PAGE);
}
$menu_data = db_get("menu","","GROUP BY DropId");
?>
<!-- Form to add new item or new menu -->
<div class="big-menu w3-row">
    <form class="w3-container" action="<?=THIS_PAGE?>" method="POST">
    <?php
    if (isset($_GET['new'])){
        $btn_action = "createnew";
    ?>
    <label class="w3-text-blue" id=""><b> Khmer Title</b>
    <input class="w3-input w3-border" type="text" name="KhmerTitle" id="" required>
    <label class="w3-text-blue" id=""><b> English Title</b>
    <input class="w3-input w3-border" type="text" name="EnglishTitle" id="" required>
    <?php
    }else{
        $btn_action = "createmenu";
    ?>
    <label class="w3-text-blue" id="menulabel"><b> Menu Title</b>
        <Select class="w3-block w3-select" id="menutitle" name="menutitle" required>
            <option value=""></option>
            <?php
            // $res is the data from menu group by dropid
            for($i=0;$i<count($menu_data);$i++){    
                echo '
                <option value="'.$menu_data[$i]['DropId'].'">'.$menu_data[$i]['MenuTitle'].' | '.$menu_data[$i]['EnTitle'].'</option>
                ';
            }
            ?>
            
        </Select>
    <?php
    }
    ?>
        
        </label>
        <label class="w3-text-blue" id=""><b> Menu Items</b>
        <input class="w3-input w3-border" type="text" name="menuitems" id="menuitemsvalue" required>
        <ul id="menuitems" class="w3-ul w3-border w3-row"></ul>
        <button class="w3-btn w3-blue" name="<?=$btn_action?>">Submit Menu</button>
    </form>
        <a class="w3-btn w3-blue" href="<?=THIS_PAGE?>?new">Create Menu</a>
</div>
<!-- From Here Table list of all menu -->
<div class="big-menu w3-row">
    <div class="w3-col l4 m4 s12">
    <?php
    
    $res = db_get('fp_posts','GROUP BY PostType');
    for($i=0;$i<count($res);$i++){    
    ?>
        <!-- onclick = "check_value(this,'item4add<?=$i?>');check_innerHTML(this,'title<?=$i?>_<?=$y?>','title4add<?=$i?>')" -->
        <button class="w3-button w3-block w3-left-align w3-green" onclick="myFunction('acc<?=$i?>')"><?php echo strtoupper($res[$i]['PostType'])?></button>
        <div class="w3-hide w3-container" id="acc<?=$i?>">
            <ul class="w3-ul w3-border">
            <?php
            $sub_res = db_get('fp_posts',"WHERE PostType='".$res[$i]['PostType']."'");
            for($y=0;$y<count($sub_res);$y++)
            {
                if($sub_res[$y]['PostId'] != 0){
            ?>
            <li class="kh-content">
            <span id="title<?=$i?>_<?=$y?>"><?=$sub_res[$y]['PostTitle']?></span>
            <!-- On click add item to menu -->
            <a id="add<?=$i?>_<?=$y?>"><i class="fas fa-plus right"></i></a></li>
            <script>
            document.getElementById("add<?=$i?>_<?=$y?>").onclick = function(){
                if (addValueSubmit('menuitemsvalue','<?=$sub_res[$y]['PostId']?>') == true){
                    newItem('li','menuitems','menuitemli<?=$i?>_<?=$y?>');
                    setAtt('menuitemli<?=$i?>_<?=$y?>','class','w3-col l2');

                    newItem('input','menuitemli<?=$i?>_<?=$y?>','menuitem<?=$i?>_<?=$y?>');
                    setAtt('menuitem<?=$i?>_<?=$y?>','type','hidden');
                    setAtt('menuitem<?=$i?>_<?=$y?>','value','<?=$sub_res[$y]['PostId']?>');
                    
                    newItem('label','menuitemli<?=$i?>_<?=$y?>','menuitemlabel<?=$i?>_<?=$y?>',caption='<?=$sub_res[$y]['PostTitle']?>');
                    newItem('a','menuitemli<?=$i?>_<?=$y?>','removeaction<?=$i?>_<?=$y?>');
                    setAtt('removeaction<?=$i?>_<?=$y?>','onclick','removeItemById(\'menuitemli<?=$i?>_<?=$y?>\');removeValueSubmit(\'menuitemsvalue\',\'<?=$sub_res[$y]['PostId']?>\')');
                    newItem('i','removeaction<?=$i?>_<?=$y?>','remove<?=$i?>_<?=$y?>');
                    setAtt('remove<?=$i?>_<?=$y?>','class','fas fa-minus-circle w3-text-red right');
                }
            }
            </script>
            <?php
                }
            }
            ?>
            </ul>
        </div>
        
    <?php
        
    }
    ?>
        <button class="w3-button w3-block w3-left-align w3-green" onclick="myFunction('categories<?=$i?>')">CATEGORIES</button>
    </div>
    <div class="w3-col l7 m7 s12 right ">
        <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
            <thead >
                <tr class="w3-green"><th>Id</th><th>Title</th><th>Type</th></tr>
            </thead>
            <tbody>
<?php
        // menu data was define on the top
        for($i=1;$i<count($menu_data);$i++){
            $item_data = db_get("menu","WHERE DropId='".$menu_data[$i]['DropId']."'");
            for($y=0;$y<count($item_data);$y++){
                $item_title = db_get("fp_posts","WHERE PostId='".$item_data[$y]['PostId']."'");
                if ($item_data[$y]['Type'] == 1){
                    $menu_type = "Menu Title";
                    $menu_title = $item_data[$y]['MenuTitle'].' | '.$item_data[$y]['EnTitle'] . 
                    '<a onclick="msgbox('."'Do you really want to delete this Group of menu?','".THIS_PAGE."?delgroup=".$item_data[$y]['DropId']."','_self','yesno'".')"><i class="fas fa-minus-circle w3-text-red right"></i></a>';
                }else{
                    $menu_type = "Menu Item";
                    $menu_title = $item_title[0]['PostTitle']. ' | '. $item_title[0]['PostTitleEn'] .
                    '<a onclick="msgbox('."'Do you really want to delete this link?','".THIS_PAGE."?delid=".$item_data[$y]['Id']."','_self','yesno'".')"><i class="fas fa-minus-circle w3-text-red right"></i></a>';
                }
                
                echo'
                <tr><td>'.$item_data[$y]['Id'].'</td><td>'.$menu_title.'</td><td>'.$menu_type.'</td></tr>
                ';
            }
        }
?>
            </tbody>
        </table>
    </div>
</div>