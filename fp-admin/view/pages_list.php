<?php
include PHYSICAL_PATH.'library/admin.php';
$permission = array("Admin","Superuser","Editor");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if(isset($_GET['delid'])){
    $data = array(
        'PostId'=>$_GET['delid']
    );
    db_delete('fp_posts',$data);
    header("Location: ".THIS_PAGE);
}
if(isset($_GET['pubid'])){
    $data = array(
        'PostStatus'=>'2'
    );
    $cond = array(
                'PostId'=>$_GET['pubid']
    );
    db_update('fp_posts',$data,$cond);
    header("Location: ".THIS_PAGE);
}
if(!isset($_POST['submit'])){
    if ($_SESSION['infos']['role'] == 'Superuser'){
        $data = array(
            'PostType'=>'page',
        );
    }else{
        $data = array(
            'PostType'=>'page',
            'PostAuthor'=>$_SESSION['infos']['id'],
        );
    }
    $result = db_get_where('fp_posts',$data);
    $res_status = db_get_count('fp_posts','PostTitle','PostStatus',$data);
    $count_detail = count($res_status);
}else{
    if ($_SESSION['infos']['role'] == 'Superuser'){
        $result = db_get('fp_posts',"Where PostType='page' AND PostTitle LIKE '%".$_POST['title']."%'");
    }else{
        $result = db_get('fp_posts',"Where PostType='page' AND PostAuthor= '".$_SESSION['infos']['id']."' AND PostTitle LIKE '%".$_POST['title']."%'");
    }
}


$count = count($result);

?>
<div class="small-12 columns big-menu">
<h3>PAGE LIST</h3>
<div class="row">
    <div class="small-12 medium-8 large-8 columns left">
        <form action="<?=THIS_PAGE?>" method="Post">
            <input class="input-group-field" type="text" name="title">
            <input type="submit" class="button" value="Submit" name="submit">
        </form>
    </div>
</div>
<ul class="menu">
  <li class="menu-text">All(<?=$count?>)</li>
  <?php
  if (!isset($_POST['submit'])){
  $status = '';
  for($i=0;$i<$count_detail;$i++)
        {
   if ($res_status[$i]['Name'] != 0){
            if($res_status[$i]['Name']==1){
                $status = 'save';
            }else if($res_status[$i]['Name']==2){
                $status = 'publish';
            }else if($res_status[$i]['Name']==3){
                $status = 'editing';
            }
    echo '<li><a href="#">| '.$status.'(
        '.$res_status[$i]['Qty'].'
    )</a></li>';
        }
        }
  }else{
    echo '<li class="menu-text"><a href="'.THIS_PAGE.'">Show All</a></li>';
}
    ?>
  
  
</ul>
<div class="table-scroll">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr>
        <th class="small-5">Title</th><th>Author</th><th><i class="fa fa-comment" aria-hidden="true"></i></th><th>Post Status</th><th>Date</th><th>URL</th>
        </tr>
        <?php
        
        for($i=0;$i<$count;$i++)
        {
        if($result[$i]['PostId']!=0){
            if($result[$i]['PostStatus'] == 1){
                $status = 'save';
                $quick_pub = '
                | <a href="?pubid='.$result[$i]['PostId'].'" class="small">Publish </a>
                ';
            }else if($result[$i]['PostStatus']== 2){
                $status = 'publish';
                $quick_pub='';
            }else if($result[$i]['PostStatus']== 3){
                $status = 'editing';
                $quick_pub = '
                | <a href="?pubid='.$result[$i]['PostId'].'" class="small">Publish  </a>
                ';
            }
            //Compare date and get the date
            if ($result[$i]['PostModify']== NULL){
                $date = format_date($result[$i]['PostDate'],'d-m-Y H:i:s');
            }else{
                $date = format_date($result[$i]['PostModify'],'d-m-Y H:i:s');
            }
            //Get the author last and first name from fp_users
            $data = array(
                'Id'=>$result[$i]['PostAuthor']
            );
            $res_author = db_get_where('fp_users',$data);
            for($y=0;$y<count($res_author);$y++){
                $author = $res_author[$y]['LastName'].' '.$res_author[$y]['FirstName'];
                $role_author = $res_author[$y]['Role'];
            }
             
       echo'
        <tr>
        <td >'.$result[$i]['PostTitle'].'<br/>';
            if($role_author == 'Admin' && $result[$i]['PostAuthor'] == $_SESSION['infos']['id'] ){
                echo '<a href="pages_add.php?id='.$result[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$result[$i]['PostId'].'" target="_blank" class="small">View</a>
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$result[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            }else if($role_author != 'Admin'){
                echo '<a href="pages_add.php?id='.$result[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$result[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$result[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            }      
                
        echo '
        </td><td>'.$author.'</td><td></td>
                <td>'.$status.'</td><td>'.$date.'</td>
                <td>'.NAV_PATH.'index.php/post?mem='.$result[$i]['PostId'].'</td>
        </tr>';
        }
    }
        ?>
  </table>
</div>
</div>
<?php
}#end permission
?>