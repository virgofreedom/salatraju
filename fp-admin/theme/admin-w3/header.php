<?php
if(SESSION == true){
    session_start();//session start
}
?>
<!DOCTYPE html>
<html>
<title>Salatraju Admin Panel</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="<?=VIRTUAL_PATH?>img/logo.png">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>css/foundation.min.css">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>css/souris.css">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>css/virgo.css">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>css/table.css">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>khmerfonts/font.css">
<link rel="stylesheet" href="<?=VIRTUAL_PATH?>css/main.css">


<script src="<?=VIRTUAL_PATH?>js/virgo.js"></script>
<script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">
<?php
if(VIEW_PAGE != 'login'){
?>
<!-- Top container -->
<div class="w3-bar w3-top w3-large" style="z-index:4;background:#af833c;">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right"><img src="<?=VIRTUAL_PATH?>img/logo1.png" alt="Logo" style="width:10%;float:right;"></span>
</div>
<?php
$path = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpg";
$path1 = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".png";
$path2 = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpeg";

if (file_exists($path)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpg";
}elseif (file_exists($path1)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".png";
}elseif (file_exists($path2)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpeg";
}else{
    $img_src = VIRTUAL_PATH."img/picID.jpg";
}
?>
<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="<?=$img_src?>" class="w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span class="Kmuol">សូមស្វាគមន៍! <strong><?php echo $_SESSION['infos']['first_name']?></strong></span><br>
      <?php echo $_SESSION['infos']['role'];?>, ID : <?php echo $_SESSION['infos']['id'];?><br>
      <a href="logout.php" class="w3-bar-item w3-button"><i class="fas fa-sign-out-alt"></i></a>
<!--       <a href="#" class="w3-bar-item w3-button"><i class="fa fa-user"></i></a>-->
      <a href="<?=VIRTUAL_PATH?>index.php/setting.php" class="w3-bar-item w3-button"><i class="fa fa-cog"></i></a> 
    </div>
  </div>
  <hr>
  <div class="w3-container">
    <h5><a href="index.php">Dashboard</a> </h5>
  </div>
  <div class="w3-bar-block">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    <a href="<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php"><i class="fa fa-file-image-o" aria-hidden="true"></i>Images Manager</a>
    <?php
      w3_accordion_menu();
    ?>
    <br><br>
  </div>
</nav>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:75px;">
<?php
}
?>

