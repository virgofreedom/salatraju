<div class="small-12 columns big-menu">
    <?php
    $permission = array("Admin","Superuser","Annotation");
    if (!in_array($_SESSION['infos']['role'],$permission)){
        echo "You don't have the permission to use this page.";
    }else{
        if (isset($_POST['submit'])){
            $cond = array($_POST['step']."Id"=>$_POST['id']);
            $data = array(
                $_POST['step']."Title"=>$_POST['title'],
            );
            db_update($_POST['step'],$data,$cond,DB_NAME2);
?>
            <form action="annotation_<?=$_POST['back']?>" method="POST" id="return_back">
                <input type="text" value="<?=$_POST['backid']?>" name="<?=$_POST['bname']?>" readonly hidden>
                <input type="text" value="<?=$_POST['previousval']?>" name="<?=$_POST['previous']?>" readonly hidden>
                <input type="text" name="<?=$_POST['step']?>" value="Cancel" hidden>
                
            </form>
            <script>
                    form_submit('return_back');
            </script>
<?php
        }else{
            
        
        $res = db_get($_GET['step'],"Where ".$_GET['step']."Id = '".$_GET['id']."'","","","",DB_NAME2);
        $title = $res[0][$_GET['step'].'Title'];
?>
        <!-- Edit form -->
        <div class="row">
            <form action="<?=THIS_PAGE?>" method="Post">
                    <input type="text" name="backid" value="<?=$_GET['backid']?>" readonly hidden>
                    <input type="text" name="bname" value="<?=$_GET['bname']?>" readonly hidden>
                    <input type="text" name="back" value="<?=$_GET['back']?>" readonly hidden>
                    <input type="text" name="step" value="<?=$_GET['step']?>" readonly hidden>
                    <input type="text" name="id" value="<?=$_GET['id']?>" readonly hidden>
                    <input type="text" name="previousval" value="<?=$_GET['previousval']?>" readonly hidden>
                    <input type="text" name="previous" value="<?=$_GET['previous']?>" readonly hidden>
                    <input type="text" name="title" value="<?=$title?>">
                    <div class="small-6 large-1 columns">
                        <input type="submit" class="button" name="submit" value="Save">
                    </div>
                
            </form>
            <!-- Cancel button -->
            <form action="annotation_<?=$_GET['back']?>" method="POST">
                <input type="text" value="<?=$_GET['backid']?>" name="<?=$_GET['bname']?>" readonly hidden>
                <input type="text" value="<?=$_GET['previousval']?>" name="<?=$_GET['previous']?>" readonly hidden>
                <div class="small-6 large-1 columns">
                    <input type="submit" class="button" name="<?=$_GET['step']?>" value="Cancel">
                </div>
            </form>
        </div>
<?php
        }
    }
    ?>
</div>