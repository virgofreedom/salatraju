<?php
$permission = array("Admin","Superuser","LegalUpdate","AdminLegalUpdate");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    if (isset($_GET['DelId'])){
        $id = $_GET['DelId'];
        $cond = array(
            "Id" => $id
        );
        db_delete("legal_update",$cond);
    }
?>
<div class="small-12 columns big-menu w3-padding-large ">


<table class="w3-table-all w3-hoverable">
<thead>
    <tr class="w3-green"><th>ចំណងជើង</th><th>អ្នករៀបរៀង</th><th>Status</th><th>Date</th></tr>
</thead>

<?php
// Get data from here
$show_all = array("Admin","Superuser","AdminLegalUpdate");
if (!in_array($_SESSION['infos']['role'],$show_all)){
    $data = db_get('legal_update','WHERE UserId="'.$_SESSION['infos']['id'].'"');    
}else{
    $data = db_get('legal_update');
}


for ($i=0;$i < count($data);$i++){
    echo '<tr>
            <th>'.$data[$i]['Title'].'<br><a href="'.VIRTUAL_PATH.'index.php/legal_update?id='.$data[$i]['Id'].'">Edit</a> | 
            <a href="#" onclick="msgbox('."'Do you really want to delete this?','".THIS_PAGE."?DelId=".$data[$i]['Id']."','_self','yesno'".');">Delete</a> | 
            <a href="'.VIRTUAL_PATH.'index.php/legal_update_view?id='.$data[$i]['Id'].'">View</a>
            </th>
            <th>'.$data[$i]['Writer'].'</th>
            <th>'.$data[$i]['Status'].'</th>
            <th>'.$data[$i]['DateModify'].'</th>
        </tr>';
}
?>
</table>

</div>
<?php

}
?>