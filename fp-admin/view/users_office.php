<?php 
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
?>
    <div class="small-12 columns big-menu w3-padding-large">
        <div class="row">
            <div class="small-12">
                <label for="title">ឈ្មោះការិយាល័យ ៖
                    <input class="w3-input" type="text" name="title" value="" placeholder="Please enter title here." required/>
                </label>
            </div>
        </div>
    </div>
<?php
}
?>