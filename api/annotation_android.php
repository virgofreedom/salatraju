<?php
include '../fp-admin/config/config.php';


$myObj = new \stdClass();
$krom = array();
// Get Krom Data
if (isset($_GET['keyword']) && $_GET['keyword']=="krom"){
    $krom_data = db_get("krom","WHERE Status=1","","ORDER BY kromId ASC","",DB_NAME2);
    for ($i=0;$i<count($krom_data);$i++){
        array_push(
            $krom,array(
                "kromId"=>$krom_data[$i]['kromId'],
                "kromDateuse"=>$krom_data[$i]['kromDateuse'],
                "kromTitle"=>$krom_data[$i]['kromTitle'],
                "kromDescrtiption"=>html_entity_decode(trim($krom_data[$i]['kromDescrtiption']),ENT_QUOTES),
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($krom, JSON_PRETTY_PRINT);
    echo $myJSON;
}
//Get kinthi data
if (isset($_GET['keyword']) && $_GET['keyword']=="kinthi"){
    $kinthin = array();
    $kinthi_data = db_get("kinthi","WHERE kromId='".$_GET['id']."' AND Status=1","","ORDER BY kinthiId ASC","","salatraju_annotation");
    $krom_data =  db_get("krom","WHERE kromId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($kinthi_data);$i++){
        array_push(
            $kinthin,array(
                "kinthiId"=>$kinthi_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[$i]['kinthiTitle'],
                "kromId"=>$kinthi_data[$i]['kromId'],
                "kromTitle"=>$krom_data[0]['kromTitle'],
                "Status"=>$kinthi_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($kinthin, JSON_PRETTY_PRINT);
    echo $myJSON;

}
//Get methika data
if (isset($_GET['keyword']) && $_GET['keyword']=="metika"){
    $metika = array();
    $metika_data = db_get("metika","WHERE kinthiId='".$_GET['id']."' AND Status=1","","ORDER BY metikaId ASC","","salatraju_annotation");
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($metika_data);$i++){
        array_push(
            $metika,array(
                "kinthiId"=>$metika_data[$i]['kinthiId'],
                "kinthiTitle"=>$kinthi_data[0]['kinthiTitle'],
                "metikaId"=>$metika_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[$i]['metikaTitle'],
                "Status"=>$metika_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($metika, JSON_PRETTY_PRINT);
    echo $myJSON;
}

//Get Chapter data
if (isset($_GET['keyword']) && $_GET['keyword']=="chapter"){
    $chapter = array();
    $chapter_data = db_get("chapter","WHERE metikaId='".$_GET['id']."' AND Status=1","","ORDER BY chapterId ASC","","salatraju_annotation");
    $metika_data = db_get("metika","WHERE metikaId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($chapter_data);$i++){
        array_push(
            $chapter,array(
                "chapterId"=>$chapter_data[$i]['chapterId'],
                "metikaId"=>$chapter_data[$i]['metikaId'],
                "metikaTitle"=>$metika_data[0]['metikaTitle'],
                "chapterTitle"=>$chapter_data[$i]['chapterTitle'],
                "Status"=>$chapter_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($chapter, JSON_PRETTY_PRINT);
    echo $myJSON;
}

//Get Section data
if (isset($_GET['keyword']) && $_GET['keyword']=="section"){
    $section = array();
    $section_data = db_get("section","WHERE chapterId='".$_GET['id']."' AND Status=1","","ORDER BY sectionId ASC","","salatraju_annotation");
    $chapter_data = db_get("chapter","WHERE chapterId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($section_data);$i++){
        array_push(
            $section,array(
                "chapterId"=>$section_data[$i]['chapterId'],
                "chapterTitle"=>$chapter_data[0]['chapterTitle'],
                "sectionId"=>$section_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[$i]['sectionTitle'],
                "Status"=>$section_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($section, JSON_PRETTY_PRINT);
    echo $myJSON;
}

//Get Katapheak data
if (isset($_GET['keyword']) && $_GET['keyword']=="kathapheak"){
    $kathapheak = array();
    $kathapheak_data = db_get("kathapheak","WHERE sectionId='".$_GET['id']."' AND Status=1","","ORDER BY kathapheakId ASC","","salatraju_annotation");
    $section_data = db_get("section","WHERE sectionId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($kathapheak_data);$i++){
        array_push(
            $kathapheak,array(
                "kathapheakId"=>$kathapheak_data[$i]['kathapheakId'],
                "sectionId"=>$kathapheak_data[$i]['sectionId'],
                "sectionTitle"=>$section_data[0]['sectionTitle'],
                "kathapheakTitle"=>$kathapheak_data[$i]['kathapheakTitle'],
                "Status"=>$kathapheak_data[$i]['Status']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($kathapheak, JSON_PRETTY_PRINT);
    echo $myJSON;
}

//GET metra data
if (isset($_GET['keyword']) && $_GET['keyword']=="metra"){
    $metra = array();
    $metra_data = db_get("metra","WHERE kathapheakId='".$_GET['id']."'","","ORDER BY metraOrder ASC","","salatraju_annotation");
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($metra_data);$i++){
        array_push(
            $metra,array(
                "kathapheakId"=>$metra_data[$i]['kathapheakId'],
                "kathapheakTitle"=>$kathapheak_data[0]['kathapheakTitle'],
                "metraId"=>$metra_data[$i]['metraId'],
                "metraTitle"=>$metra_data[$i]['metraTitle'],
                "metraDescription"=>$metra_data[$i]['metraDescription'],
                "metraOrder"=>$metra_data[$i]['metraOrder']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($metra, JSON_PRETTY_PRINT);
    echo $myJSON;
}
//GET metra data
if (isset($_GET['keyword']) && $_GET['keyword']=="findmetra"){
    $metra = array();
    $metra_data = db_get("metra","WHERE metraOrder='".$_GET['id']."'","","ORDER BY metraId ASC","","salatraju_annotation");
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$_GET['id']."' AND Status=1","","","","salatraju_annotation");
    for ($i=0;$i<count($metra_data);$i++){
        array_push(
            $metra,array(
                "kathapheakId"=>$metra_data[$i]['kathapheakId'],
                "kathapheakTitle"=>$kathapheak_data[0]['kathapheakTitle'],
                "metraId"=>$metra_data[$i]['metraId'],
                "metraTitle"=>$metra_data[$i]['metraTitle'],
                "metraDescription"=>$metra_data[$i]['metraDescription'],
                "metraOrder"=>$metra_data[$i]['metraOrder']
            )
        );
    }
    // Here convert from array into json object
    $myJSON = json_encode($metra, JSON_PRETTY_PRINT);
    echo $myJSON;
}
//Get detail information of a metra
if (isset($_GET['keyword']) && $_GET['keyword']=="detail"){
    $detail = array();
    $metra_data = db_get("metra","WHERE metraId='".$_GET['id']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$metra_data[0]['kathapheakId']."' AND Status=1","","ORDER BY kathapheakId ASC","",DB_NAME2);
    $section_data = db_get("section","WHERE sectionId='".$kathapheak_data[0]['sectionId']."' AND Status=1","","ORDER BY sectionId ASC","",DB_NAME2);
    $chapter_data = db_get("chapter","WHERE chapterId='".$section_data[0]['chapterId']."' AND Status=1","","ORDER BY chapterId ASC","",DB_NAME2);
    $metika_data = db_get("metika","WHERE metikaId='".$chapter_data[0]['metikaId']."' AND Status=1","","ORDER BY metikaId ASC","",DB_NAME2);
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$metika_data[0]['kinthiId']."' AND Status=1","","ORDER BY kinthiId ASC","",DB_NAME2);
    $krom_data = db_get("krom","WHERE kromId='".$kinthi_data[0]['kromId']."'","","ORDER BY kromId ASC","",DB_NAME2);
    $ref_data = db_get("reference","WHERE KromId='".$krom_data[0]['kromId']."'","","ORDER BY KromId ASC","",DB_NAME2);
     #ref array
     $referenceIdkh = array();
     $referenceTitle = array();
     $referenceDate = array();
     $referenceDescription = array();

    for ($i=0;$i<count($ref_data);$i++){
        array_push($referenceIdkh,$ref_data[$i]['referenceIdkh']);
        array_push($referenceTitle,$ref_data[$i]['referenceTitle']);
        array_push($referenceDate,$ref_data[$i]['referenceDate']);
        array_push($referenceDescription,$ref_data[$i]['referenceDescription']);
    }
    for ($i=0;$i<count($krom_data);$i++){
        $kromId = $krom_data[$i]['kromId'];
        $kromDateuse = $krom_data[$i]['kromDateuse'];
        $kromTitle = $krom_data[$i]['kromTitle'];
        $kromDescrtiption = $krom_data[$i]['kromDescrtiption'];
    }
    for ($i=0;$i<count($kinthi_data);$i++){
        $kinthiId = $kinthi_data[$i]['kinthiId'];
        $kinthiTitle = $kinthi_data[$i]['kinthiTitle'];

    }
    for ($i=0;$i<count($metika_data);$i++){
        $metikaId = $metika_data[$i]['metikaId'];
        $metikaTitle = $metika_data[$i]['metikaTitle'];
    }
    for ($i=0;$i<count($chapter_data);$i++){
        $chapterId = $chapter_data[$i]['chapterId'];
        $chapterTitle = $chapter_data[$i]['chapterTitle'];
    }
    for ($i=0;$i<count($section_data);$i++){
        $sectionId = $section_data[$i]['sectionId'];
        $sectionTitle = $section_data[$i]['sectionTitle'];
    }
    for ($i=0;$i<count($kathapheak_data);$i++){
        $kathapheakId = $kathapheak_data[$i]['kathapheakId'];
        $kathapheakTitle = $kathapheak_data[$i]['kathapheakTitle'];
    }
    for ($i=0;$i<count($metra_data);$i++){
        $metraOrder = $metra_data[$i]['metraOrder'];
        $metraTitle = $metra_data[$i]['metraTitle'];
        $metraDescription = $metra_data[$i]['metraDescription'];
    }
    array_push(
        $detail,array(
            "referenceIdkh"=>$referenceIdkh,
            "referenceTitle"=>$referenceTitle,
            "referenceDate"=>$referenceDate,
            "referenceDescription"=>$referenceDescription,
            "kromId"=>$kromId,
            "kromDateuse"=>$kromDateuse,
            "kromTitle"=>$kromTitle,
            "kromDescrtiption"=>$kromDescrtiption,
            "kinthiId"=>$kinthiId,
            "kinthiTitle"=>$kinthiTitle,
            "metikaId"=>$metikaId,
            "metikaTitle"=>$metikaTitle,
            "chapterId"=>$chapterId,
            "chapterTitle"=>$chapterTitle,
            "sectionId"=>$sectionId,
            "sectionTitle"=>$sectionTitle,
            "kathapheakId"=>$kathapheakId,
            "kathapheakTitle"=>$kathapheakTitle,
            "metraOrder"=>$metraOrder,
            "metraTitle"=>$metraTitle,
            "metraDescription"=>$metraDescription
        )
    );
    // Here convert from array into json object
    $myJSON = json_encode($detail, JSON_PRETTY_PRINT);
    echo $myJSON;
}
if (isset($_GET['keyword']) && $_GET['keyword']=="metraDetail"){
    $detail = array();
    $metra_data = db_get("metra","WHERE metraOrder='".$_GET['id']."' AND kathapheakId='".$_GET['subid']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
    $kathapheak_data = db_get("kathapheak","WHERE kathapheakId='".$metra_data[0]['kathapheakId']."' AND Status=1","","ORDER BY kathapheakId ASC","",DB_NAME2);
    $section_data = db_get("section","WHERE sectionId='".$kathapheak_data[0]['sectionId']."' AND Status=1","","ORDER BY sectionId ASC","",DB_NAME2);
    $chapter_data = db_get("chapter","WHERE chapterId='".$section_data[0]['chapterId']."' AND Status=1","","ORDER BY chapterId ASC","",DB_NAME2);
    $metika_data = db_get("metika","WHERE metikaId='".$chapter_data[0]['metikaId']."' AND Status=1","","ORDER BY metikaId ASC","",DB_NAME2);
    $kinthi_data = db_get("kinthi","WHERE kinthiId='".$metika_data[0]['kinthiId']."' AND Status=1","","ORDER BY kinthiId ASC","",DB_NAME2);
    $krom_data = db_get("krom","WHERE kromId='".$kinthi_data[0]['kromId']."'","","ORDER BY kromId ASC","",DB_NAME2);
    $ref_data = db_get("reference","WHERE KromId='".$krom_data[0]['kromId']."'","","ORDER BY KromId ASC","",DB_NAME2);
     #ref array
     $referenceIdkh = array();
     $referenceTitle = array();
     $referenceDate = array();
     $referenceDescription = array();

    for ($i=0;$i<count($ref_data);$i++){
        array_push($referenceIdkh,$ref_data[$i]['referenceIdkh']);
        array_push($referenceTitle,$ref_data[$i]['referenceTitle']);
        array_push($referenceDate,$ref_data[$i]['referenceDate']);
        array_push($referenceDescription,$ref_data[$i]['referenceDescription']);
    }
    for ($i=0;$i<count($krom_data);$i++){
        $kromId = $krom_data[$i]['kromId'];
        $kromDateuse = $krom_data[$i]['kromDateuse'];
        $kromTitle = $krom_data[$i]['kromTitle'];
        $kromDescrtiption = $krom_data[$i]['kromDescrtiption'];
    }
    for ($i=0;$i<count($kinthi_data);$i++){
        $kinthiId = $kinthi_data[$i]['kinthiId'];
        $kinthiTitle = $kinthi_data[$i]['kinthiTitle'];

    }
    for ($i=0;$i<count($metika_data);$i++){
        $metikaId = $metika_data[$i]['metikaId'];
        $metikaTitle = $metika_data[$i]['metikaTitle'];
    }
    for ($i=0;$i<count($chapter_data);$i++){
        $chapterId = $chapter_data[$i]['chapterId'];
        $chapterTitle = $chapter_data[$i]['chapterTitle'];
    }
    for ($i=0;$i<count($section_data);$i++){
        $sectionId = $section_data[$i]['sectionId'];
        $sectionTitle = $section_data[$i]['sectionTitle'];
    }
    for ($i=0;$i<count($kathapheak_data);$i++){
        $kathapheakId = $kathapheak_data[$i]['kathapheakId'];
        $kathapheakTitle = $kathapheak_data[$i]['kathapheakTitle'];
    }
    for ($i=0;$i<count($metra_data);$i++){
        $metraOrder = $metra_data[$i]['metraOrder'];
        $metraTitle = $metra_data[$i]['metraTitle'];
        $metraDescription = $metra_data[$i]['metraDescription'];
    }
    array_push(
        $detail,array(
            "referenceIdkh"=>$referenceIdkh,
            "referenceTitle"=>$referenceTitle,
            "referenceDate"=>$referenceDate,
            "referenceDescription"=>$referenceDescription,
            "kromId"=>$kromId,
            "kromDateuse"=>$kromDateuse,
            "kromTitle"=>$kromTitle,
            "kromDescrtiption"=>$kromDescrtiption,
            "kinthiId"=>$kinthiId,
            "kinthiTitle"=>$kinthiTitle,
            "metikaId"=>$metikaId,
            "metikaTitle"=>$metikaTitle,
            "chapterId"=>$chapterId,
            "chapterTitle"=>$chapterTitle,
            "sectionId"=>$sectionId,
            "sectionTitle"=>$sectionTitle,
            "kathapheakId"=>$kathapheakId,
            "kathapheakTitle"=>$kathapheakTitle,
            "metraOrder"=>$metraOrder,
            "metraTitle"=>$metraTitle,
            "metraDescription"=>$metraDescription
        )
    );
    // Here convert from array into json object
    $myJSON = json_encode($detail, JSON_PRETTY_PRINT);
    echo $myJSON;
}
//GET metra data for search
if (isset($_GET['keyword']) && $_GET['keyword']=="searchs"){
    $metras = array();
    $keyword = $_GET['id'];
    #get metra from keyword
    $res = db_get("metra","Where metraDescription LIKE'%".$keyword."%'","","ORDER BY kathapheakId ASC","","salatraju_annotation");
    
    
    for ($i=0;$i<count($res);$i++){
        #reset array
        $referenceIdkh = array();
        $referenceTitle = array();
        $referenceDate = array();
        $referenceDescription = array();
        #get references from metra
        $kathapheak = db_get("kathapheak","Where kathapheakId='".$res[$i]["kathapheakId"]."' AND Status=1 ","","ORDER BY sectionId ASC","","salatraju_annotation");
        $section = db_get("section","Where sectionId='".$kathapheak[0]["sectionId"]."' AND Status=1 ","","ORDER BY chapterId ASC","","salatraju_annotation");
        $chapter = db_get("chapter","Where chapterId='".$section[0]["chapterId"]."' AND Status=1 ","","ORDER BY metikaId ASC","","salatraju_annotation");
        $metika = db_get("metika","Where metikaId='".$chapter[0]["metikaId"]."' AND Status=1 ","","ORDER BY kinthiId ASC","","salatraju_annotation");
        $kinthi = db_get("kinthi","Where kinthiId='".$metika[0]["kinthiId"]."' AND Status=1 ","","ORDER BY kromId ASC","","salatraju_annotation");
        
        $krom = db_get("krom","Where kromId='".$kinthi[0]["kromId"]."'","","ORDER BY kromId ASC","","salatraju_annotation");
        $k_reference = db_get("reference","Where KromId='".$krom[0]["kromId"]."'","","","","salatraju_annotation");
        for ($k_i = 0;$k_i<count($k_reference);$k_i++){
            array_push($referenceIdkh,$k_reference[$k_i]['referenceIdkh']);
            array_push($referenceTitle,$k_reference[$k_i]['referenceTitle']);
            array_push($referenceDate,$k_reference[$k_i]['referenceDate']);
            array_push($referenceDescription,$k_reference[$k_i]['referenceDescription']);
        }
        #post_content
        $content = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["metraDescription"],ENT_QUOTES));
        $content = str_replace("\n",chr(32),$content);
        $content = str_replace("\r",chr(32),$content);
        array_push(
            $metras,array(
                'metraId'=>$res[$i]["metraId"],
                'metraOrder'=>$res[$i]["metraOrder"],
                'metraTitle'=>$res[$i]["metraTitle"],
                'Description'=>$content,
                'kathapheakId'=>$res[$i]["kathapheakId"],
                'kathapheakTitle'=>$kathapheak[0]['kathapheakTitle'],
                'sectionId'=>$section[0]['sectionId'],
                'sectionTitle'=>$section[0]['sectionTitle'],
                'chapterId'=>$chapter[0]['chapterId'],
                'chapterTitle'=>$chapter[0]['chapterTitle'],
                'metikaId'=>$metika[0]['metikaId'],
                'metikaTitle'=>$metika[0]['metikaTitle'],
                'kinthiId'=>$kinthi[0]['kinthiId'],
                'kinthiTitle'=>$kinthi[0]['kinthiTitle'],
                'kromId'=>$krom[0]['kromId'],
                'kromTitle'=>$krom[0]['kromTitle'],
                'kromDescrtiption'=>$krom[0]['kromDescrtiption'],
                'referenceIdkh'=>$referenceIdkh,
                'referenceTitle'=>$referenceTitle,
                'referenceDate'=>$referenceDate,
                'referenceDescription'=>$referenceDescription

            )
        );
    }
    $myJSON = json_encode($metras, JSON_PRETTY_PRINT);
    echo $myJSON;
}
?>