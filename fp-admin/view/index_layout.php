<?php
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if (isset($_POST['added'])){
    if(isset($_POST['ItemsId'])){
        $itemsid = $_POST['ItemsId'];
    }else{
        $itemsid = "";
    }
    if(isset($_POST['Start'])){
        $start = $_POST['Start'];
    }else{
        $start = "";
    }
    if(isset($_POST['Path'])){
        $path = $_POST['Path'];
    }else{
        $path = "";
    }
    if(isset($_POST['Length'])){
        $lenght = $_POST['Length'];
    }else{
        $lenght = "";
    }
    if(isset($_POST['Caption'])){
        $caption = $_POST['Caption'];
    }else{
        $caption = "";
    }
    if(isset($_POST['android'])){
        $android = $_POST['android'];
    }else{
        $android = "";
    }
    if(isset($_POST['ios'])){
        $ios = $_POST['ios'];
    }else{
        $ios = "";
    }
    $orders = count(db_get('index_layout'))+1;
    $val = array("Label"=>$_POST['Label'],
                "Type"=>$_POST['Type'],
                "TypeLayout"=>$_POST['TypeLayout'],
                "ItemsId"=>$itemsid,
                "Length"=>$lenght,
                "Start"=>$start,
                "Path"=>$path,
                "Caption"=>$caption,
                "Orders"=>$orders,
                "Android"=>$android,
                "Ios"=>$ios
                );
    db_insert('index_layout',$val);
    header("Location: ".VIRTUAL_PATH."index.php/index_layout.php");
}else if (isset($_POST['Update'])){
     for ($i=0;$i<count($_POST['Orders']);$i++){
         $cond = array("Id"=>$_POST['Id'][$i]);
         $val = array("Orders"=>$_POST['Orders'][$i]);
         db_update('index_layout',$val,$cond);
     }
     header("Location: ".VIRTUAL_PATH."index.php/index_layout.php");
}else if (isset($_POST['Updated'])){
    if(isset($_POST['ItemsId'])){
        $itemsid = $_POST['ItemsId'];
    }else{
        $itemsid = 0;
    }
    if(isset($_POST['Start'])){
        $start = $_POST['Start'];
    }else{
        $start = 0;
    }
    if(isset($_POST['Path'])){
        $path = $_POST['Path'];
    }else{
        $path = "";
    }
    if(isset($_POST['Length'])){
        $lenght = $_POST['Length'];
    }else{
        $lenght = 0;
    }
    if(isset($_POST['Caption'])){
        $caption = $_POST['Caption'];
    }else{
        $caption = "";
    }
    $val = array(
    "Label"=>$_POST['Label'],
    "TypeLayout"=>$_POST['TypeLayout'],
    "ItemsId"=>$itemsid,
    "Length"=>$lenght,
    "Start"=>$start,
    "Path"=>$path,
    "Caption"=>$caption,
    );
    $cond = array("Id"=>$_POST['Id']);
    db_update('index_layout',$val,$cond);
    header("Location: ".VIRTUAL_PATH."index.php/index_layout.php");
}else if (isset($_GET['delid'])){
    $cond = array('Id'=>$_GET['delid']);
    db_delete('index_layout',$cond);
    header("Location: ".VIRTUAL_PATH."index.php/index_layout.php");
}else if (isset($_GET['editid'])){
    $res = db_get('index_layout','Where Id="'.$_GET['editid'].'"');
    if ($res[0]['TypeLayout'] == 1){
        $layout_page = "1 per row";
    }elseif ($res[0]['TypeLayout'] == 2){
        $layout_page = "2 per row";
    }elseif ($res[0]['TypeLayout'] == 3){
        $layout_page = "3 per row";
    }elseif ($res[0]['TypeLayout'] == 4){
        $layout_page = "4 per row";
    }elseif ($res[0]['TypeLayout'] == 5){
        $layout_page = "1 big and list on the right";
    }
    if ($res[0]['Type'] == "slideshow"){#Slideshow
        $option = '
        <label>Caption
        <input type="text" name="Caption" value="'.$res[0]['Caption'].'"/>
        </label>
        <label>Path to the folder of slideshow
        <input type="text" name="Path" value="'.$res[0]['Path'].'" placeholder="leave it blank for the default path"></label>
        
        ';
    }elseif ($res[0]['Type'] == "apps"){#apps
        $option='
        <label>Short Decription
            <textarea name="Caption" required>'.$res[0]['Caption'].'</textarea>
            
        </label>
        <label>Link Android
            <input type="text" name="android" value="'.$res[0]['Android'].'" required>
        </label>
        <label>Link iOS
            <input type="text" name="ios" value="'.$res[0]['Ios'].'" required>
        </label>
        ';
    }elseif ($res[0]['Type'] == "catergories"){#category
        $res_cat1 = db_get('catergories','where Id="'.$res[0]['ItemsId'].'"') ;
        $res_cat = db_get('catergories');
        $option = '<label>Categories\' name
        <select name="ItemsId">
        <option value="'.$res[0]['ItemsId'].'">'.$res_cat1[0]['Name'].'</option>
        ';
        for ($i = 0;$i<count($res_cat);$i++){
               $option .= '<option value="'.$res_cat[$i]['Id'].'">'.$res_cat[$i]['Name'].'</option>';
        }
        $option .= '
        </select></label>
        <label>Number of article / video</label>
        <input type="text" name="Length" value="'.$res[0]['Length'].'" required>';
    }else{#any thing else
        $option = '
        <label>Number of article / video
        <input type="text" name="Length" value="'.$res[0]['Length'].'" required></label>
        <label>Id article you want to start
        <input type="text" name="Start" value="'.$res[0]['Start'].'" required></label>
        ';
    }
    #form to edit
    echo '
    <div class="small-6 columns big-menu small-offset-3">
    <form action="'.VIRTUAL_PATH.'index.php/index_layout.php" method="POST">
        <label>Content\'s Type
        <input type="Text" value="'.$res[0]['Type'].'" readonly/>
        <input type="hidden" name="Id" value="'.$res[0]['Id'].'"/>
        <label for="">Article\'s Label
        <input type="text" name="Label" value="'.$res[0]['Label'].'"/>
        </label>
        
        </label>Layout\'s Type
        <select name="TypeLayout">
            <option value="'.$res[0]['TypeLayout'].'">'.$layout_page.'</option>
            <option value="1">1 per row</option>
            <option value="2">2 per row</option>
            <option value="3">3 per row</option>
            <option value="4">4 per row</option>
            <option value="5">1 big and list on the right</option>
        </select>
        <label>
        '.$option.'
        <input class="success button expanded" type="submit" name="Updated" value="Save">
    </form>
    </div>
    ';
    
}else{
$res = db_get('index_layout','','','Order by Orders');
if (count($res) != 0){
?>
    <div class="table-scroll">
        <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
            <thead>
                <tr><th>Content's Type</th><th>Content's Label</th><th>Layout's Type</th>
                <th>Number to show</th><th>Article Start</th>
                <th>Path</th><th>Caption</th><th>Order</th>
                <th></th></tr>
            </thead>
            <tbody>
            <form action="<?=VIRTUAL_PATH?>index.php/index_layout.php" method="POST">
            <?php
            for ($i=0;$i<count($res);$i++){
                if ($res[$i]['ItemsId'] != 0){
                    $res_cat1 = db_get('catergories','where Id="'.$res[$i]['ItemsId'].'"') ;
                    if (count($res_cat1)>0){
                        $caption = $res_cat1[0]['Name'];
                    }
                }else{
                    $caption = $res[$i]['Caption'];
                }
                
                if ($res[$i]['TypeLayout'] == 1){
                    $layout_page = "1 per row";
                }elseif ($res[$i]['TypeLayout'] == 2){
                    $layout_page = "2 per row";
                }elseif ($res[$i]['TypeLayout'] == 3){
                    $layout_page = "3 per row";
                }elseif ($res[$i]['TypeLayout'] == 4){
                    $layout_page = "4 per row";
                }elseif ($res[$i]['TypeLayout'] == 5){
                    $layout_page = "1 big and list on the right";
                }
                if ($res[$i]['Type'] == "slideshow" && $res[$i]['Path'] == ""){
                    $path_page = "Default";
                }else{
                    $path_page = $res[$i]['Path'];
                }
                echo '
                <tr><td>'.$res[$i]['Type'].'</td><td>'.$res[$i]['Label'].'</td><td>'.$layout_page.'</td><td>'.$res[$i]['Length'].'</td>
                <td>'.$res[$i]['Start'].'</td><td>'.$path_page.'</td><td>'.$caption.'</td>
                <td>
                <input type="hidden" name="Id['.$i.']" value="'.$res[$i]['Id'].'">
                <input type="text" name="Orders['.$i.']" value="'.$res[$i]['Orders'].'">
                </td>
                <td>
                <a class="primary button" href="?editid='.$res[$i]['Id'].'" >Edit</a>
                <a class="alert button" href="#" onclick="msgbox('."'Do you want to delete this?','?delid=".$res[$i]['Id']."','_self','yesno'".')" >Delete</a>
                </td>
                </tr>
                ';
            }
            ?>
            <tr><td colspan="9"><input class="success button expanded" type="submit" name="Update" value="Save"></td></tr>
            </form>
            </tbody>
        </table>
    </div>
<?php
}
?>
<button class="w3-button w3-green w3-large" onclick="document.getElementById('addnew').style.display='block'">Add new layout</button>
<div  id="addnew" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">
        <div class="w3-center"><br>
            <span onclick="document.getElementById('addnew').style.display='none'" class="w3-button w3-xlarge w3-hover-red w3-display-topright" title="Close Modal">&times;</span>
        </div>
        <form class="w3-container"" action="<?=VIRTUAL_PATH?>index.php/index_layout.php" method="POST">
            <label for="">Article's Label
                <input type="text" name="Label" />
            </label>
            <label for="">Article's type
                <select name="Type" id="Type" onchange="ajaxs('options','<?=DOMAIN?>/fp-admin/view/index_options.php','Get',['Type'])">
                    <option></option>
                    <option value="post">Post</option>
                    <option value="page">Page</option>
                    <option value="catergories">Category</option>
                    <option value="videos">Video</option>
                    <option value="slideshow">Slideshow</option>
                    <option value="apps">Apps</option>
                </select>
            </label>
            <label for="">Layout's type
                <select name="TypeLayout" id="TypeLayout">
                    <option value="1">1 per row</option>
                    <option value="2">2 per row</option>
                    <option value="3">3 per row</option>
                    <option value="4">4 per row</option>
                    <option value="5">1 big and list on the right</option>
                </select>
            </label>
            <div id="options"></div>
            <input type="submit" class="button" value="Add" name="added">
        </form>
        
    </div>
</div>
<?php
}#end form
}#end permission
?>