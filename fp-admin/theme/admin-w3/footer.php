<!-- Footer -->
<?php

if(VIEW_PAGE != 'login'){
?>
</div>
<footer class="text-center box-pa-20 grey-color">
    	&copy;2015 - <?php echo date('Y'); ?> Virgo FreedomTeam Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a>
</footer>
<?php
}
?>



  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>
<!-- SCRIPTS -->
<script src="<?=VIRTUAL_PATH?>canvasjs/canvasjs.min.js"></script>
  
</body>
</html>