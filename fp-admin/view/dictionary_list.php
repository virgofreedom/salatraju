<?php
if(isset($_GET['delid'])){
    $data = array(
        'Id'=>$_GET['delid']
    );
    db_delete('dictionnary',$data);
    header("Location: ".THIS_PAGE);
}
?>
<div class="small-12 columns big-menu">
<?php
$alphabet = db_get('alphabet_khmer');
for($i=0;$i<count($alphabet);$i++){
    if ($i == 0){
        echo "<a href='".THIS_PAGE."?group=".$alphabet[$i]['Word']."'>".$alphabet[$i]['Word']."</a>";    
    }else{
        echo " | <a href='".THIS_PAGE."?group=".$alphabet[$i]['Word']."'>".$alphabet[$i]['Word']."</a>";
    }
    
}

?>
<form action="<?=THIS_PAGE?>" method="POST">
    <div class="input-group">
            <label>ស្វែងរកពាក្យគន្លឹះ</label>
            <input class="input-group-field" type="text" name="word">
        <div class="">
            <input type="submit" class="button small" value="ស្វែងរក" name="submit">
        </div>
    </div>

</form>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
<tr>
<th>Id</th><th>Word</th><th>Definition</th><th>Other</th>
</tr>
<?php
if(isset($_POST['submit'])){
    
    $res = db_get('dictionnary','WHERE Word = "'.$_POST['word'].'"','','ORDER BY Word');
}elseif (!isset($_GET['group'])){
    $res = db_get('dictionnary','WHERE Word LIKE "'.$alphabet[0]['Word'].'%"','','ORDER BY Word');
}else{
    $res = db_get('dictionnary','WHERE Word LIKE "'.$_GET['group'].'%"','','ORDER BY Word');
}
if(isset($_GET['Word'])){
    $res = db_get('dictionnary','WHERE Word = "'.$_GET['Word'].'"','','ORDER BY Word');
}
$duplicate = array();
for($i=0;$i<count($res);$i++){
    $duplicate_data = db_get('dictionnary','WHERE Word = "'.$res[$i]['Word'].'"','','ORDER BY Word');
    array_push($duplicate,$duplicate_data[0]['Word']);
    $definition = htmlspecialchars_decode($res[$i]['Description'],ENT_QUOTES);
echo '
<tr>
<td>'.$res[$i]['Id'].'</td><td>'.$res[$i]['Word'].'</td><td>'.$definition.'</td><td>
<a href="dictionary_add.php?Id='.$res[$i]['Id'].'">Edit</a> |';
if ($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
echo '
<a href="#" onclick="msgbox('."'Do you want to delete this word?','?delid=".$res[$i]['Id']."','_self','yesno'".')" >Delete</a>';
}
echo '
</td>
</tr>
';
}
?>
</table>

</div>