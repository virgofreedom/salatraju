
<script>

if (typeof(Storage) == "undefined") {
  // Sorry! No Web Storage support..
  document.getElementById("id01").innerHTML = "Sorry! No Web Storage support..";
}

//load legal index
function load_legal_index(receiver){
  
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/legalindex";

  var showout="";
  
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.krom.length;i++){
      showout += '<a href="<?=DOMAIN?>legal_index/krom/'+arr.krom[i].kromId+'" class="w3-button w3-block w3-left-align">'+arr.krom[i].kromTitle+'</a>';
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  
}
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
  
  
  sessionStorage.KromTitle = "";
  sessionStorage.KinthiTitle="";sessionStorage.MetikaTitle="";sessionStorage.ChapterTitle="";sessionStorage.SectionTitle="";sessionStorage.KatapheakTitle="";
}
//load Krom
function load_krom(receiver,id){
  
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/krom/"+id;

  var showout="";
  
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    
      //Do loop over array and add style output
    for (var i = 0; i < arr.kinthi.length;i++){
      if (arr.kinthi[i].kinthiTitle == "0" || arr.kinthi[i].kinthiTitle == ""){
      
      window.open("<?=DOMAIN?>legal_index/kinthi/"+arr.kinthi[i].kinthiId,"_self");
      
      }else{
        showout += '<a href="<?=DOMAIN?>legal_index/kinthi/'+arr.kinthi[i].kinthiId+'" class="w3-button w3-block w3-left-align">'+arr.kinthi[i].kinthiTitle+'</a>';
        sessionStorage.KromId = arr.kinthi[0].kromId;sessionStorage.KromTitle= "<i class='fa fa-arrow-right'></i> " + arr.kinthi[0].kromTitle;
      }
    
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  sessionStorage.KinthiTitle="";sessionStorage.MetikaTitle="";sessionStorage.ChapterTitle="";sessionStorage.SectionTitle="";sessionStorage.KatapheakTitle="";
  goback();
}
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
  
}
//load kinthi
function load_kinthi(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/kinthi/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  if (arr.metika[0].kinthiTitle == "0" || arr.metika[0].kinthiTitle == ""){
        sessionStorage.KinthiTitle="";
      }else{
        sessionStorage.KinthiId = arr.metika[0].kinthiId;sessionStorage.KinthiTitle= "<i class='fa fa-arrow-right'></i> " +arr.metika[0].kinthiTitle;
      }
      sessionStorage.MetikaTitle="";sessionStorage.ChapterTitle="";sessionStorage.SectionTitle="";sessionStorage.KatapheakTitle="";
  //Do loop over array and add style output
  for (var i = 0; i < arr.metika.length;i++){
    
    if (arr.metika[i].metikaTitle == "0" || arr.metika[i].metikaTitle == ""){
      
      window.open("<?=DOMAIN?>legal_index/metika/"+arr.metika[i].metikaId,"_self");
      
    }else{
      showout += '<a href="<?=DOMAIN?>legal_index/metika/'+arr.metika[i].metikaId+'" class="w3-button w3-block w3-left-align">'+arr.metika[i].metikaTitle+'</a>';
      
    }
      
    goback();
  }
  
}

document.getElementById(receiver).innerHTML = showout;

}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load metika
function load_metika(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/metika/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  if (arr.chapter[0].metikaTitle == "0" || arr.chapter[0].metikaTitle == ""){
        sessionStorage.MetikaTitle="";
      }else{
        sessionStorage.MetikaId = arr.chapter[0].metikaId;sessionStorage.MetikaTitle= "<i class='fa fa-arrow-right'></i> " +arr.chapter[0].metikaTitle;
      }
      sessionStorage.ChapterTitle="";sessionStorage.SectionTitle="";sessionStorage.KatapheakTitle="";
  //Do loop over array and add style output
  for (var i = 0; i < arr.chapter.length;i++){
    
    if (arr.chapter[i].chapterTitle == "0" || arr.chapter[i].chapterTitle == ""){
      
      window.open("<?=DOMAIN?>legal_index/chapter/"+arr.chapter[i].chapterId,"_self");
      
    }else{
      showout += '<a href="<?=DOMAIN?>legal_index/chapter/'+arr.chapter[i].chapterId+'" class="w3-button w3-block w3-left-align">'+arr.chapter[i].chapterTitle+'</a>';
      
    }
    goback();
  }
  
}

document.getElementById(receiver).innerHTML = showout;

}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}


//load chapter
function load_chapter(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/chapter/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  if (arr.section[0].chapterTitle == 0 || arr.section[0].chapterTitle == ""){
        sessionStorage.ChapterTitle="";
      }else{
        sessionStorage.ChapterId = arr.section[0].chapterId;sessionStorage.ChapterTitle= "<i class='fa fa-arrow-right'></i> " +arr.section[0].chapterTitle;
      }
      sessionStorage.SectionTitle="";sessionStorage.KatapheakTitle="";
  //Do loop over array and add style output
  for (var i = 0; i < arr.section.length;i++){
    
    if (arr.section[i].sectionTitle == "0" || arr.section[i].sectionTitle == ""){
      
      window.open("<?=DOMAIN?>legal_index/section/"+arr.section[i].sectionId,"_self");
      
    }else{
      showout += '<a href="<?=DOMAIN?>legal_index/section/'+arr.section[i].sectionId+'" class="w3-button w3-block w3-left-align">'+arr.section[i].sectionTitle+'</a>';
      
    }
    goback();
  }
  
}

document.getElementById(receiver).innerHTML = showout;

}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load section
function load_section(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/section/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  if(arr.kathapheak[0].sectionTitle == "0" || arr.kathapheak[0].sectionTitle == ""){
        sessionStorage.SectionTitle="";
      }else{
        sessionStorage.SectionId = arr.kathapheak[0].sectionId;sessionStorage.SectionTitle= "<i class='fa fa-arrow-right'></i> " +arr.kathapheak[0].sectionTitle;
      }

      sessionStorage.KatapheakTitle="";
  //Do loop over array and add style output
  for (var i = 0; i < arr.kathapheak.length;i++){
    
    if (arr.kathapheak[i].kathapheakTitle == "0" || arr.kathapheak[i].kathapheakTitle == ""){
      
      window.open("<?=DOMAIN?>legal_index/kathapheak/"+arr.kathapheak[i].kathapheakId,"_self");
      
    }else{
      showout += '<a href="<?=DOMAIN?>legal_index/kathapheak/'+arr.kathapheak[i].kathapheakId+'" class="w3-button w3-block w3-left-align">'+arr.kathapheak[i].kathapheakTitle+'</a>';
      
    }
    goback();
  }
  
}

document.getElementById(receiver).innerHTML = showout;

}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

//load kathapheak
function load_kathapheak(receiver,id){

var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/kathapheak/"+id;

var showout="";

xmlhttp.onreadystatechange=function() {
  if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
  var arr = JSON.parse(xmlhttp.responseText);
  //Do loop over array and add style output
  for (var i = 0; i < arr.metra.length;i++){
      showout += '<a href="<?=DOMAIN?>legal_index/metra/'+arr.metra[i].metraId+'" class="w3-button w3-block w3-left-align">'+arr.metra[i].metraTitle+'</a>';
  }
  if (arr.metra[0].kathapheakTitle == "" || arr.metra[0].kathapheakTitle == "0"){
    sessionStorage.KatapheakTitle="";
  }else{
    sessionStorage.KatapheakId=arr.metra[0].kathapheakId;sessionStorage.KatapheakTitle="<i class='fa fa-arrow-right'></i> " +arr.metra[0].kathapheakTitle;
  }
  goback();
}

document.getElementById(receiver).innerHTML = showout;

}
xmlhttp.open("GET", url, true);
xmlhttp.send();
}

// load metra detail
function load_metra(receiver,id){
  var xmlhttp = new XMLHttpRequest();
  var url = "<?=DOMAIN?>api/annotations/metra/"+id;

  var showout="";

  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
    var arr = JSON.parse(xmlhttp.responseText);
    //Do loop over array and add style output
    for (var i = 0; i < arr.detail.length;i++){
      var kinthi_t = "";var metika_t = "";var chapter_t = "";var section_t = "";var kathapheak_t ="";
      if (arr.detail[i].kinthiTitle == '0'){ kinthi_t = "";}else{kinthi_t = "<a href='<?=DOMAIN?>legal_index/kinthi/"+arr.detail[i].kinthiId+"'>"+arr.detail[i].kinthiTitle +"</a>";}
      if (arr.detail[i].metikaTitle == '0'){ metika_t = "";}else{metika_t = "<a href='<?=DOMAIN?>legal_index/metika/"+arr.detail[i].metikaId+"'>" + ">>" +arr.detail[i].metikaTitle +"</a>";}
      if (arr.detail[i].chapterTitle == '0'){ chapter_t = "";}else{chapter_t = "<a href='<?=DOMAIN?>legal_index/chapter/"+arr.detail[i].chapterId+"'>" + ">>" +arr.detail[i].chapterTitle +"</a>";}
      if (arr.detail[i].sectionTitle == '0'){ section_t = "";}else{section_t = "<a href='<?=DOMAIN?>legal_index/section/"+arr.detail[i].sectionId+"'>" + ">>" +arr.detail[i].sectionTitle +"</a>";}
      if (arr.detail[i].kathapheakTitle == '0'){ kathapheak_t ="";}else{kathapheak_t = "<a href='<?=DOMAIN?>legal_index/section/"+arr.detail[i].kathapheakId+"'>" + ">>" +arr.detail[i].kathapheakTitle +"</a>";}
      var ref_list = "<ul>";
      var date_ref = "";
      for (var y = 0;y < arr.detail[i].referenceDate.length;y++){
          if(arr.detail[i].referenceDate[y] =="0000-00-00"){
              date_ref = "";
          }else{
              date_ref = " ចុះថ្ងៃទី "+arr.detail[i].referenceDate[y];
          }
          ref_list += "<li>បានទ្រង់យល់ "+arr.detail[i].referenceTitle[y]+" "+ arr.detail[i].referenceIdkh[y] 
          + date_ref +" "+arr.detail[i].referenceDescription[y]+" </li>";
      }
      ref_list += "</ul>";
      var showout = '<center><h5>'+arr.detail[i].metraTitle+'</h5></center>'+arr.detail[i].metraDescription;
    }
    
  }
  
  document.getElementById(receiver).innerHTML = showout;
  goback();
  }
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

</script>
<script>
//click "Enter" to search
// var input = document.getElementById("word");

// input.addEventListener("keyup", function(event) {
//   if (event.keyCode === 13) {
//    event.preventDefault();
//    document.getElementById("btn_search").click();
//   }
// });
function keyEnter(btnSearch){
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById(btnSearch).click();
  }
}
</script>
<!-- Form  -->

<div class="small-12 medium-12 large-12 columns post-block index-post" id="annotaion" style="margin-top:10px;">
<h4><a href="<?=DOMAIN?>legal_index">Traju Legal Index</a></h4>
    
    <div class="input-group">
            <label>ស្វែងតាមរកពាក្យគន្លឹះ</label>
            <input class="input-group-field" type="text" id="word" onkeyup="keyEnter('btn_search')">
        <div class="">
          <button class="button small" onclick='ann_search("word","listkrom")' id="btn_search">ស្វែករក</button>
        </div>
    </div>
</div>

<div id="id01"></div>
<div class="w3-col l12 m12 s12 index-post" id="navigate">
<div id="legal"><a href="<?=DOMAIN?>legal_index"><i class="fa fa-balance-scale"></i></a></div>
<div id="krom"></div><div id="kinthi"></div><div id="metika"></div>
<div id="chapter"></div><div id="section"></div><div id="katapheak"></div>
</div>
<div class="small-12 medium-12 large-12 columns post-block index-post" id="listkrom"></div>

<!-- End Form -->

<script>
<?php

if($_GET['legal_index'] == "krom"){
  echo "load_krom('listkrom','".$_GET['id']."');";  
}elseif($_GET['legal_index'] == "kinthi"){
  echo "load_kinthi('listkrom','".$_GET['id']."');";  
}elseif ($_GET['legal_index'] == "metika"){
  echo "load_metika('listkrom','".$_GET['id']."');";  
}elseif ($_GET['legal_index'] == "chapter"){
  echo "load_chapter('listkrom','".$_GET['id']."');";  
}elseif ($_GET['legal_index'] == "section"){
  echo "load_section('listkrom','".$_GET['id']."');";  
}elseif ($_GET['legal_index'] == "kathapheak"){
  echo "load_kathapheak('listkrom','".$_GET['id']."');";  
}elseif ($_GET['legal_index'] == "metra"){
  echo "load_metra('listkrom','".$_GET['id']."');";  
}else{
echo "load_legal_index('listkrom');";

}

?>

<!-- Load session Storage to make the go back button -->
function goback(){
document.getElementById("krom").innerHTML = "<a href='<?=DOMAIN?>legal_index/krom/"+sessionStorage.KromId+"'>  "+sessionStorage.KromTitle+"</a>";
document.getElementById("kinthi").innerHTML = "<a href='<?=DOMAIN?>legal_index/kinthi/"+sessionStorage.KinthiId+"'>"+sessionStorage.KinthiTitle+"</a>";
document.getElementById("metika").innerHTML = "<a href='<?=DOMAIN?>legal_index/metika/"+sessionStorage.MetikaId+"'>"+sessionStorage.MetikaTitle+"</a>";
document.getElementById("chapter").innerHTML = "<a href='<?=DOMAIN?>legal_index/chapter/"+sessionStorage.ChapterId+"'>"+sessionStorage.ChapterTitle+"</a>";
document.getElementById("section").innerHTML = "<a href='<?=DOMAIN?>legal_index/section/"+sessionStorage.SectionId+"'>"+sessionStorage.SectionTitle+"</a>";
document.getElementById("katapheak").innerHTML = "<a href='<?=DOMAIN?>legal_index/katapheak/"+sessionStorage.KatapheakId+"'>"+sessionStorage.KatapheakTitle+"</a>";

}

//Annotation search
function ann_search(word,receiver){
var xmlhttp = new XMLHttpRequest();
var url = "<?=DOMAIN?>api/annotations/searchs/" + document.getElementById(word).value;

  xmlhttp.onreadystatechange=function() {
    var out = "<h5>លទ្ធផលការស្វែករក :</h5>";    
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
          var arr = JSON.parse(xmlhttp.responseText);
          //Do loop over array and add style output
          for (var i = 0; i < arr.metras.length;i++){
              var ref_title = "[";
              var ref_id = "[";
              var ref_date = "[";
              var ref_desc = "[";
              for (var y = 0; y < arr.metras[i].referenceTitle.length;y++){
                  if(y ==0){
                    ref_title += "'" + arr.metras[i].referenceTitle[y] + "'";
                    ref_id += "'" + arr.metras[i].referenceIdkh[y] + "'";
                    ref_date += "'" + arr.metras[i].referenceDate[y] + "'";
                    ref_desc += "'" + arr.metras[i].referenceDescription[y] + "'";
                  }else{
                    ref_title += ",'" + arr.metras[i].referenceTitle[y] + "'";
                    ref_id += ",'" + arr.metras[i].referenceIdkh[y] + "'";
                    ref_date += ",'" + arr.metras[i].referenceDate[y] + "'";
                    ref_desc += ",'" + arr.metras[i].referenceDescription[y] + "'";
                  }
                  
              }
              ref_title += "]";
              ref_id += "]";
              ref_date += "]";
              ref_desc += "]";
            out += 
            '<div class="small-12 medium-12 large-12 columns post-block index-post"><a href="#" onclick="'+
            "ann_detail('listkrom','"+ arr.metras[i].kromTitle +"'," + ref_title + ","+ref_id + ","+ ref_date + ","+ ref_desc + ",'"+
            arr.metras[i].kromDescrtiption + "','" +arr.metras[i].kinthiTitle + "','"+arr.metras[i].metikaTitle+ "','"+arr.metras[i].chapterTitle+
            "','"+arr.metras[i].sectionTitle+ "','"+arr.metras[i].kathapheakTitle+ "','"+arr.metras[i].metraTitle+ "','"+ arr.metras[i].Description
            +"');"
            +'"><p><b>' + 
            arr.metras[i].metraTitle + '</b> (មាត្រា '+ arr.metras[i].metraOrder +' នៃ '+ arr.metras[i].kromTitle +') </p></a></div>';
          }
          if (arr.metras.length == 0) {
            out += '<div class="small-12 medium-12 large-12 columns post-block index-post">ព៉ុមានទិន្ន័យដែលស្វែងរកទេ!</div>';
          }
      }
      
      document.getElementById(receiver).innerHTML = out;
  }
  
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
  
}

function ann_detail(receiver,kromtitle,ref_title,ref_id,ref_date,ref_desc,kromdesc,kinthi,metika,chapter,section,kathapheak,metra,metradesc){
    
    var kinthi_t = "";var metika_t = "";var chapter_t = "";var section_t = "";var kathapheak_t ="";
    if (kinthi == '0'){ kinthi_t = "";}else{kinthi_t = kinthi;}
    if (metika == '0'){ metika_t = "";}else{metika_t = ">>" +metika;}
    if (chapter == '0'){ chapter_t = "";}else{chapter_t = ">>" +chapter;}
    if (section == '0'){ section_t = "";}else{section_t = ">>" +section;}
    if (kathapheak == '0'){ kathapheak_t ="";}else{kathapheak_t = ">>" +kathapheak;}
    var ref_list = "<ul>";
    var date_ref = "";
    for (var i = 0;i < ref_title.length;i++){
        if(ref_date[i] =="0000-00-00"){
            date_ref = "";
        }else{
             date_ref = " ចុះថ្ងៃទី "+ref_date[i];
        }
        ref_list += "<li>បានទ្រង់យល់ "+ref_title[i]+" "+ ref_id[i] + date_ref +" "+ref_desc[i]+" </li>";
    }
    ref_list += "</ul>";
    var out = '<h4 class="w3-center">'+metra+'</h4>'; 
    out += metradesc;
    out += "<div class='row'><a href='<?=DOMAIN?>legal_index'>" + "<< បង្ហាញក្រមទាំងអស់</a></div>";
    document.getElementById(receiver).innerHTML = out;
}
</script>