<?php
$permission = array("Admin","Superuser","Annotation");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    unset($_SESSION['annotation']);
    if (isset($_POST['krom_title'])){
        $Krom = $_POST['krom_title'];
    }else{
        $Krom = "";
    }

    $krom_title="";
    $krom_data = db_get("krom","","","","",DB_NAME2);
    if (!isset($_POST['krom_title'])){
    ?>
        <div class="small-12 columns big-menu">
        <script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>
        <form action="<?=THIS_PAGE?>" method="POST">
            <div class="row">
                <div class="small-12">
                    ឈ្មោះក្រម:*
                    <label for="title" id="krom_div">
                        <select id="krom_title" name="krom_title" onchange="toinput('krom_div','krom_title','new');" required>
                            <option value=""></option>
                            <option value="new">Add New</option>
                            <?php
                            for($i=0;$i<count($krom_data);$i++){
                                echo '<option value="'.$krom_data[$i]['kromTitle'].'">'.$krom_data[$i]['kromTitle'].'</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <input type="submit" class="button right" name="next" value="Next"/>
                    
                </div>  
            </div>
        </form>
<?php
if (isset($_POST['Update'])){
    
    for ($i=0;$i<count($_POST['Orders']);$i++){
        $cond = array("kromId"=>$_POST['Id'][$i]);
        $val = array("Orders"=>$_POST['Orders'][$i]);
        db_update('krom',$val,$cond,DB_NAME2);
    }
    header("Location: ".THIS_PAGE);
}
?>
<div class="small-12 columns big-menu">
<form action="<?=THIS_PAGE?>" method="POST">
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">

    <thead>
        <th>ឈ្មោះក្រម</th><th>Orders</th><th></th>
    </thead>
    <tbody>
    <?php
    if (isset($_GET['action'])) {
        if ($_GET['action'] == 'hide'){
            $data = array("Status" => -1);
            $cond = array("kromId" => $_GET['id']);
            $get_data = db_get_where("krom",$cond,'',DB_NAME2);
            $data_log = array(
                "UserId"=> $_SESSION['infos']['id'],
                "Action"=> $_GET['action'],
                "ItemName"=>"krom",
                "ItemTitle"=>$get_data[0]['kromTitle'],
                "Date"=>date('Y-m-d h:i:s')
            );
            db_insert('annotation_deleted_list',$data_log);
            db_update('krom',$data,$cond,DB_NAME2);
            header("Location: ".THIS_PAGE);
        }elseif($_GET['action'] == 'show'){
            $data = array("Status" => 1);
            $cond = array("kromId" => $_GET['id']);
            $get_data = db_get_where("krom",$cond,'',DB_NAME2);
            $data_log = array(
                "UserId"=> $_SESSION['infos']['id'],
                "Action"=> $_GET['action'],
                "ItemName"=>"krom",
                "ItemTitle"=>$get_data[0]['kromTitle'],
                "Date"=>date('Y-m-d h:i:s')
            );
            db_insert('annotation_deleted_list',$data_log);
            db_update('krom',$data,$cond,DB_NAME2);
            header("Location: ".THIS_PAGE);
        }elseif($_GET['action'] == 'deleted'){
            $cond = array("kromId" => $_GET['id']);
            $get_data = db_get_where("krom",$cond,'',DB_NAME2);
            $data_log = array(
                "UserId"=> $_SESSION['infos']['id'],
                "Action"=> $_GET['action'],
                "ItemName"=>"krom",
                "ItemTitle"=>$get_data[0]['kromTitle'],
                "Date"=>date('Y-m-d h:i:s')
            );
            db_insert('annotation_deleted_list',$data_log);
            db_delete('krom',$cond,DB_NAME2);
            header("Location: ".THIS_PAGE);
        }
    }
        for($i=0;$i<count($krom_data);$i++){
            echo '<tr><td>'.$krom_data[$i]['kromTitle'].'</td>';
            $permission = array("Admin","Superuser");
            if (in_array($_SESSION['infos']['role'],$permission)){
            echo '
            <td>
            <input type="hidden" name="Id['.$i.']" value="'.$krom_data[$i]['kromId'].'" readonly>
            <input type="text" name="Orders['.$i.']" value="'.$krom_data[$i]['Orders'].'">
            </td>';
            echo '<td>';
                if ($krom_data[$i]['Status'] == 1){
                    echo '<a onclick="msgbox('."'តើពិតជាចង់លាក់".$krom_data[$i]['kromTitle']."នេះមែន?','".THIS_PAGE."?action=hide&id=".$krom_data[$i]['kromId']."','_self','yesno'".')">លាក់</a> | ';
                }else{
                    echo '<a onclick="msgbox('."'តើពិតជាចង់បង្ហោះ".$krom_data[$i]['kromTitle']."នេះមែន?','".THIS_PAGE."?action=show&id=".$krom_data[$i]['kromId']."','_self','yesno'".')">បង្ហោះ</a> | ';
                }
                
                    echo '<a onclick="msgbox('."'តើពិតជាចង់លុប".$krom_data[$i]['kromTitle']."នេះមែន?','".THIS_PAGE."?action=deleted&id=".$krom_data[$i]['kromId']."','_self','yesno'".')">លុប</a>';
            }
            
            echo '</td></tr>';
        }
        
    ?>
    <tr><td colspan="3"><input class="w3-button w3-green" type="submit" name="Update" value="Save"></td></tr>
    </tbody>
</table>
</form>
</div>
    <?php
    }else{#else Krom
        $insert_krom = array(
                        "kromTitle"=>$Krom
                            );
        if (isset($_POST['reference'])){#update reference
            
            if ($_POST['ref_date']==""){
                $ref_date = "0000/00/00";
            }else{
                $ref_date = $_POST['ref_date'];
            }
            
            $cond = array("referenceId"=>$_POST['ref_id']);
            $data = array(
                "referenceIdkh"=>$_POST['ref_idkh'],
                "referenceTitle"=>$_POST['ref_title'],
                "referenceDate"=>$ref_date,
                "referenceDescription"=>$_POST['ref_desc']
            );
            db_update("reference",$data,$cond,DB_NAME2);
        }elseif(isset($_POST['reference_del'])){
            
        }
        $value_krom = db_get("krom","Where kromTitle = '".$Krom."'","","","",DB_NAME2);
        if (count($value_krom)>0){
            #Get krom ID
            $krom_id = $value_krom[0]['kromId'];
            $krom_title = $value_krom[0]['kromTitle'];
            $krom_des = $value_krom[0]['kromDescrtiption'];
            $krom_dateuse = $value_krom[0]['kromDateuse'];
        }else{
            #add new krom
            db_insert("krom",$insert_krom,DB_NAME2);
            $krom_data_after = db_get("krom","Where kromTitle = '".$Krom."'","","","",DB_NAME2);
            $krom_id = $krom_data_after[0]['kromId'];
        }
    ?>
    <div class="small-12 columns big-menu">
    <form action="annotation_kinthi.php" method="POST"/>
        
            <div class="row">
                <div class="small-4">
                    <label for="title">អត្តលេខរបស់ក្រម:*
                        <input type="text" name="krom_id" value="<?=$krom_id?>" readonly/>
                    </label>
                    <label for="title">ឈ្មោះក្រម:*
                        <input type="text" name="krom_title" value="<?=$krom_title?>"/>
                    </label>
                    <label for="title">ថ្ងៃដាក់ឲ្យប្រើប្រាស់:*
                        <input type="date" name="krom_dateuse" value="<?=$krom_dateuse?>"/>
                    </label>
                </div>
                <div class="small-8">
                    <label for="title">ការពិពណ៌នា(Description):
                        <textarea name="krom_description" id="krom_description" cols="30" rows="10">
                        <?=trim($krom_des)?>
                        </textarea>
                    </label>
                </div>
            </div>

            <div class="row">
                <fieldset>
                    <legend>ទិដ្ធាការ</legend>
                    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
                        <thead>
                            <tr>
                                <th>លិខិតលេខ</th><th>ចំណងជើង</th><th>ថ្ងៃខែឆ្នាំ</th><th>ការពិពណ៌នា</th><th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $ref = db_get("reference","WHERE KromId='$krom_id'","","","",DB_NAME2);
                                for ($i=0;$i<count($ref);$i++){
                                    echo '<tr>';
                                        echo '<td>'.$ref[$i]['referenceIdkh'].'</td>';
                                        echo '<td>'.$ref[$i]['referenceTitle'].'</td>';
                                        echo '<td>'.$ref[$i]['referenceDate'].'</td>';
                                        echo '<td>'.$ref[$i]['referenceDescription'].'</td>';
                                        echo '<td>'.
                                        
                                        '<a href="annotation_ref.php?edit_id='.$ref[$i]['referenceId'].'&krom_title='.$krom_title.'">កែប្រែ</a> 
                                        |';
                                        $permission = array("Admin","Superuser");
                                        if (in_array($_SESSION['infos']['role'],$permission)){
                                        echo'
                                        <a href="#" onclick="msgbox('."'តើពិតជាចង់លុបអត្ថបទយោងនេះមែន?','annotation_ref.php?del_id=".$ref[$i]['referenceId']."&krom_title=".$krom_title."','_self','yesno'".')" >លុប</a>
                                        ';
                                        }
                                        echo '</td>';
                                    echo '</tr>';
                                }
                            ?>
                            
                        </tbody>
                    </table>
                    <button class="button" type="button"
                    onclick= "addRef('reference');" >បន្ថែម</button>
                    <div id="reference">
                        <div class="small-4 columns">
                            <label for="title">លិខិតលេខ
                                <input type="text" name="ref_idkh[0]" />
                            </label>
                        </div>
                        <div class="small-4 columns">
                            <label for="title">ចំណងជើង
                                <input type="text" name="ref_title[0]" class="reference"/>
                            </label>
                        </div>
                        <div class="small-4 columns">
                            <label for="title">ថ្ងៃខែឆ្នាំ
                                <input type="date" name="ref_date[0]"/>
                            </label>
                        </div>
                        <div class="small-12">
                            <label for="title">ការពិពណ៌នា(Description)
                                <textarea name="ref_desc[0]" cols="30" rows="3"></textarea>
                            </label>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row">
                <div class="small-12">
                    <input type="submit" class="button right" name="kinthi" value="Next"/>  
                </div>  
            </div>
        
    
    </div>
    <?php  
    }#Endif Krom 
#start kinthi
?>

<?php
}
?>