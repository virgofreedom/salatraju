<?php
include '../fp-admin/config/config.php';
$myObj = new \stdClass();
$category = array();
$post= array();
$doc_url="";
$len_ifram = 0;
if (isset($_GET["id"])){#show content by category
    $res = db_get("fp_posts","Where Catergories Like '%".$_GET["id"]."%'");
    $res_cat = db_get("catergories","Where Id = '".$_GET["id"]."'");
    
    for ($i=0;$i<count($res);$i++){
        #post_content
        $content = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["PostContent"],ENT_QUOTES));
        $content = str_replace("\n",chr(32),$content);
        $content = str_replace("\r",chr(32),$content);
        #post_title
        $title = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["PostTitle"],ENT_QUOTES));
        $title = str_replace("\n",chr(32),$title);
        $title = str_replace("\r",chr(32),$title);
        #doc_url
        $start_index = strrpos($res[$i]["PostContent"],"http://drive.google.com");
        #echo "=>";
        $end_index = strrpos($res[$i]["PostContent"],'/view');
        #echo "=>";
        if ($end_index == ""){
            $end_index = strrpos($res[$i]["PostContent"],'/preview');
            $len = $end_index - $start_index + 8;
        }else{
            $len = $end_index - $start_index + 5;
            if ($len < 0){
                $end_index = strrpos($res[$i]["PostContent"],'/preview');
                $len = $end_index - $start_index + 8;
            }
        }
        if ($len<60){
            $doc_url = "";
        }else{
            $doc_url = substr($res[$i]["PostContent"],$start_index,$len);
        }
        #echo $doc_url."<br>";
        array_push($post,array(
            'category_id'=>$_GET["id"],
            'category_name'=>$res_cat[0]['Name'],
            'PostId'=>$res[$i]['PostId'],
            'PostAuthor'=>$res[$i]['PostAuthor'],
            'PostDate'=>$res[$i]['PostDate'],
            'PostTitle'=>$title,
            'PostContent'=>$content,
            'PostStatus'=>$res[$i]['PostStatus'],
            'PostType'=>$res[$i]['PostType'],
            'DocUrl'=>$doc_url
        ));
    }
    
    $myJSON = json_encode(array("category"=>$post), JSON_PRETTY_PRINT);
}else{
    $res = db_get("catergories");
    for ($i=0;$i<count($res);$i++){
        array_push(
            $category,array(
                'category_id'=>$res[$i]["Id"],
                'cartegory_name'=>$res[$i]["Name"]
            )
        );
    }
    $myJSON = json_encode(array("category"=>$category), JSON_PRETTY_PRINT);
}


echo $myJSON;