<?php
include '../fp-admin/config/config.php';


$myObj = new \stdClass();
$metras = array();

// get key krom request
if (isset($_GET['keyword'])){
    $keyword = $_GET['keyword'];
    #get metra from keyword
    $res = db_get("metra","Where metraDescription LIKE'%".$keyword."%' OR metraTitle LIKE'%".$keyword."%'","","ORDER BY kathapheakId ASC","",DB_NAME2);
    
    
    for ($i=0;$i<count($res);$i++){
        #reset array
        $referenceIdkh = array();
        $referenceTitle = array();
        $referenceDate = array();
        $referenceDescription = array();
        #get references from metra
        $kathapheak = db_get("kathapheak","Where kathapheakId='".$res[$i]["kathapheakId"]."' AND Status=1 ","","ORDER BY sectionId ASC","",DB_NAME2);
        $section = db_get("section","Where sectionId='".$kathapheak[0]["sectionId"]."' AND Status=1 ","","ORDER BY chapterId ASC","",DB_NAME2);
        $chapter = db_get("chapter","Where chapterId='".$section[0]["chapterId"]."' AND Status=1 ","","ORDER BY metikaId ASC","",DB_NAME2);
        $metika = db_get("metika","Where metikaId='".$chapter[0]["metikaId"]."' AND Status=1 ","","ORDER BY kinthiId ASC","",DB_NAME2);
        $kinthi = db_get("kinthi","Where kinthiId='".$metika[0]["kinthiId"]."' AND Status=1 ","","ORDER BY kromId ASC","",DB_NAME2);
        
        $krom = db_get("krom","Where kromId='".$kinthi[0]["kromId"]."'","","ORDER BY kromId ASC","",DB_NAME2);
        $k_reference = db_get("reference","Where KromId='".$krom[0]["kromId"]."'","","","",DB_NAME2);
        for ($k_i = 0;$k_i<count($k_reference);$k_i++){
            array_push($referenceIdkh,$k_reference[$k_i]['referenceIdkh']);
            array_push($referenceTitle,$k_reference[$k_i]['referenceTitle']);
            array_push($referenceDate,$k_reference[$k_i]['referenceDate']);
            array_push($referenceDescription,$k_reference[$k_i]['referenceDescription']);
        }
        #post_content
        $content = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["metraDescription"],ENT_QUOTES));
        $content = str_replace("\n",chr(32),$content);
        $content = str_replace("\r",chr(32),$content);
        array_push(
            $metras,array(
                'Id'=>$res[$i]["metraId"],
                'metraOrder'=>$res[$i]["metraOrder"],
                'metraTitle'=>$res[$i]["metraTitle"],
                'Description'=>$content,
                'kathapheakId'=>$res[$i]["kathapheakId"],
                'kathapheakTitle'=>$kathapheak[0]['kathapheakTitle'],
                'sectionId'=>$section[0]['sectionId'],
                'sectionTitle'=>$section[0]['sectionTitle'],
                'chapterId'=>$chapter[0]['chapterId'],
                'chapterTitle'=>$chapter[0]['chapterTitle'],
                'metikaId'=>$metika[0]['metikaId'],
                'metikaTitle'=>$metika[0]['metikaTitle'],
                'kinthiId'=>$kinthi[0]['kinthiId'],
                'kinthiTitle'=>$kinthi[0]['kinthiTitle'],
                'kromId'=>$krom[0]['kromId'],
                'kromTitle'=>$krom[0]['kromTitle'],
                'kromDescrtiption'=>$krom[0]['kromDescrtiption'],
                'referenceIdkh'=>$referenceIdkh,
                'referenceTitle'=>$referenceTitle,
                'referenceDate'=>$referenceDate,
                'referenceDescription'=>$referenceDescription

            )
        );
    }
}else{
    $result = 'No Request';
}

// Here convert from array into json object
    $myJSON = json_encode(array("metras"=>$metras), JSON_PRETTY_PRINT);
echo $myJSON;

