<div class="big-menu">
    <div class="w3-container w3-teal">
    <h5>ការចុះឈ្មោះអ្នកប្រើប្រាស់</h5>
    </div>

    <form class="w3-container" action="<?=THIS_PAGE?>" method="POST">
    <label class="w3-text-teal"><b>នាមត្រកូល*</b></label>
    <input class="w3-input w3-border w3-light-grey" type="text" name="lastName" required>

    <label class="w3-text-teal"><b>នាមខ្លួន*</b></label>
    <input class="w3-input w3-border w3-light-grey" type="text" name="firstName" required>

    <label class="w3-text-teal"><b>Email*</b></label>
    <input class="w3-input w3-border w3-light-grey" type="text" name="email" required>

    <label class="w3-text-teal"><b>Phone Number</b></label>
    <input class="w3-input w3-border w3-light-grey" type="text" name="phoneNumber">

    <label class="w3-text-teal"><b>Password*</b></label>
    <input class="w3-input w3-border w3-light-grey" type="text" name="password" id="password" required>

    <button class="w3-btn w3-blue-grey" type="Button" onclick="password_generate('password')">Generate Password</button>

    <label class="w3-text-teal"><b>Plan*</b></label>
    <select class="w3-select" name="plansId" required>
    <option value="" disabled selected>ជ្រើសរើសគម្រោង១</option>
    <?php
    $res_plan = db_get("plans","","","","",DB_NAME2);
    for($i=0;$i<count($res_plan);$i++){
        echo'
        <option value="'.$res_plan[$i]['Id'].'">'.$res_plan[$i]['plansName'].'</option>
        ';
    }
    ?>
    </select>

    <button class="w3-btn w3-blue-grey" name="add">បង្កើតគណនី</button>
    </form>
</div>
<script>
    function password_generate(receiver){
        var randomstring = Math.random().toString(36).slice(-8);
        document.getElementById(receiver).value = randomstring;
    }
</script>

<?php
if(isset($_POST['add'])){
    $lastName = $_POST['lastName'];
    $firstName = $_POST['firstName'];
    $email = $_POST['email'];
    $phoneNumber = $_POST['phoneNumber'];
    $password = $_POST['password'];
    $plansId = $_POST['plansId'];
}
?>