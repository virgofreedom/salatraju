<?php
$permission = array("Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
$dir = PHYSICAL_PATH_SITE.'themes/';
$files1 = scandir($dir);
$res = db_get('themes_use');
for ($y=0;$y<count($files1);$y++){
    if ($files1[$y] == $res[0]['Title']){
        $label_ac = "Activated";
        $btn_color = "flat-green";
    }else{
        $label_ac = "Activate";
        $btn_color = "button-green";
    }
    $path = $dir . $files1[$y];
    if (is_dir($path)){
        if ($files1[$y] != '.' && $files1[$y] != '..'){
?>
<div class="small-12 columns big-menu">
        
            <div class="small-12 columns title-row">
                <span class="section-title">
                <?php
                echo $files1[$y];
                ?>
                </span>
            </div>
        <div class="small-5 columns">
            <div class="small-12 columns">
            <a href="#" class="grey-color">
                <img class="rounded thumbnail" src="<?=VIRTUAL_PATH_SITE?>themes/<?=$files1[$y]?>/icon.png" alt="<?=$files1[$y]?>">
            </a>
            </div>
        </div>
        <div class="small-4 columns">
            <?php
            readfile($path.'/readme.txt'); 
            ?>
        </div>
        <div class="small-3 columns">
            <a href="<?=ADMIN_NAV_PATH?>theme_add.php?theme=<?=$files1[$y]?>" class="<?=$btn_color?>"><?=$label_ac?></a>
            <a href="themes_customize.php?id=<?=$res[$i]['Id']?>" class="button-blue">Customize</a>
        </div>
</div>
<?php       
        }
    }    
}
}
?>