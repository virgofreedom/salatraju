<?php
    if(isset($_GET['action'])){
        if($_GET['action'] == "edit"){
            $btnName = "Edit";
            $res_plan = db_get("plans","WHERE Id='".$_GET['id']."'","","","",DB_NAME2);
            $plansId = $_GET['id'];
            $plansName = $res_plan[0]['plansName'];
            $plansPrice = $res_plan[0]['plansPrice'];
            $plansDuration = $res_plan[0]['plansDuration'];
        }else{
            $btnName = "Add";
            $plansName = "";
            $plansPrice = "";
            $plansDuration = "";
            $plansId = "";
            $cond = array(
                "Id"=>$_GET['id']
            );
            $res_plan = db_get("plans","WHERE Id='".$_GET['id']."'","","","",DB_NAME2);

            action_log("delete","plans annotation",$res_plan[0]['plansName']);
            db_delete('plans',$cond,DB_NAME2);
            header("Location: ".THIS_PAGE);
        }
        
    }else{
        $btnName = "Add";
        $plansName = "";
        $plansPrice = "";
        $plansDuration = "";
        $plansId = "";
    }
?>
<div class="big-menu">
    <div class="w3-container w3-teal">
    <h5>Add New Plan</h5>
    </div>

    <form class="w3-container" action="<?=THIS_PAGE?>" method="POST">
    <label class="w3-text-teal"><b>Plans Name</b></label>
    <input class="w3-input w3-border w3-light-grey" value="<?=$plansId?>" name="plansId" type="text" hidden style="display:none;">
    <input class="w3-input w3-border w3-light-grey" value="<?=$plansName?>" name="plansName" type="text" required>

    <label class="w3-text-teal"><b>Plans Price</b></label>
    <input class="w3-input w3-border w3-light-grey" value="<?=$plansPrice?>" name="plansPrice" type="text" required>

    <label class="w3-text-teal"><b>Duration in Days</b></label>
    <input class="w3-input w3-border w3-light-grey" value="<?=$plansDuration?>" name="plansDuration" type="text" required>

    <button class="w3-btn w3-blue-grey" name="<?=$btnName?>"><?=$btnName?></button>
    </form>
</div>
<div class="big-menu">
    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <thead>
            <tr><th>Id</th><th>Plans Name</th><th>Plans Price</th><th>Plans Duration</th><th></th></tr>
        </thead>
        <tbody>
            <?php
            $res_plan = db_get("plans","","","","",DB_NAME2);
            if (count($res_plan) == 0){
                echo '<tr><td colspan="5"><center>មិនទាន់ទិន្ន័យនៅឡើយទេ!</center></td></tr>';
            }
            for($i=0;$i<count($res_plan);$i++){
                echo'
                <tr><td>'.$res_plan[$i]['Id'].'</td><td>'.$res_plan[$i]['plansName'].'</td><td>'.$res_plan[$i]['plansPrice'].'</td><td>'.$res_plan[$i]['plansDuration'].'</td>
                <td><a href="'.THIS_PAGE.'?action=edit&&id='.$res_plan[$i]['Id'].'">Edit</a> | <a onclick="msgbox('."'តើពិតជាចង់លុប".$res_plan[$i]['plansName']."នេះមែន?','".THIS_PAGE."?action=deleted&id=".$res_plan[$i]['Id']."','_self','yesno'".')">Delete</a></td></tr>
                ';
            }
            ?>
        </tbody>
    </table>
</div>
<?php
if (isset($_POST['Add'])){
    $data = array(
        "plansName"=>$_POST["plansName"],
        "plansPrice"=>$_POST["plansPrice"],
        "plansDuration"=>$_POST["plansDuration"]
    );
    db_insert("plans",$data,DB_NAME2);
    action_log("add","plans annotation",$_POST["plansName"]);
    header("Location: ".THIS_PAGE);
}elseif (isset($_POST['Edit'])){
    $data = array(
        "plansName"=>$_POST["plansName"],
        "plansPrice"=>$_POST["plansPrice"],
        "plansDuration"=>$_POST["plansDuration"]
    );
    $cond = array(
        "Id"=>$_POST['plansId']
    );
    db_update("plans",$data,$cond,DB_NAME2);
    action_log("Update","plans annotation",$_POST["plansName"]);
    header("Location: ".THIS_PAGE);
}

?>