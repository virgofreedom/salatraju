
<?php
include PHYSICAL_PATH.'library/admin.php';
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if(isset($_GET['delid'])){
    $data = array(
        'Id'=>$_GET['delid']
    );
    db_delete('sidebar_set',$data);
    header("Location: ".THIS_PAGE);
}else if(isset($_GET['dellid'])){
    $data = array(
        'Id'=>$_GET['dellid']
    );
    $val = array(
        'Position'=>'right'
    );
    $res = db_get_where('sidebar_set',$data);
    if($res[0]['Position']== 'both'){
        db_update('sidebar_set',$val,$data);
    }else{
        db_delete('sidebar_set',$data);
    }
    header("Location: ".THIS_PAGE);
}else if(isset($_GET['delrid'])){
    $data = array(
        'Id'=>$_GET['delrid']
    );
    $val = array(
        'Position'=>'left'
    );
    $res = db_get_where('sidebar_set',$data);
    if($res[0]['Position']== 'both'){
        db_update('sidebar_set',$val,$data);
    }else{
        db_delete('sidebar_set',$data);
    }
    header("Location: ".THIS_PAGE);
}
?>
<div class="small-12 columns big-menu sub-left-side-bar">
<h4>ឧបករណ៍របស់ផ្នែកចំហៀង</h4>
<ul class="menu">
<?php

$res = db_get('sidebar_items');
$res_set = db_get('sidebar_set','','','order by Orders ASC');
$arr_left = array();
$arr_right = array();
for ($i=0; $i<count($res);$i++){
            $sign = '<a href="'.ADMIN_NAV_PATH.'sidebar_add.php?sb='.$res[$i]['SidebarId'].'"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>';
?>
    <li class="small-3 columns left">
    <div class="small-10 columns" >
    <?php 
    echo $res[$i]['SidebarName'] 
    ?>
    </div>
    <div class="small-1 left columns">
        
            <?=$sign?>
        
    </div>
    </li>
<?php
}
for($y=0;$y<count($res_set);$y++){
    $arr_item = array(
                'Id'=>$res_set[$y]['Id'],
                'Label'=>$res_set[$y]['Label']
            );
        if ($res_set[$y]['Position'] == 'right'){
            array_push($arr_right,$arr_item);
        }else if($res_set[$y]['Position'] == 'left'){
            array_push($arr_left,$arr_item);
        }else if($res_set[$y]['Position'] == 'both'){
            array_push($arr_right,$arr_item);
            array_push($arr_left,$arr_item);
        }
}

?>
    <li class="small-3 columns left">
        <div class="small-12 columns" >
            <a href="<?=ADMIN_NAV_PATH?>sidebar_list.php">តារាងរាយឧបករណ៍</a>
        </div>
    </li>
    </ul>
   
</div>
<div class="small-5 small-offset-1 columns big-menu sub-left-side-bar">
<h4>ផ្នែកខាងឆ្នេង</h4>
<ul class="menu">
    <?php
    for($le=0;$le<count($arr_left);$le++){
    ?>
    <li class="small-12 columns">
        <div class="small-8 columns" >
        <?=$arr_left[$le]['Label']?>
        </div>
        <div class="small-3 columns">
        <a href="<?=ADMIN_NAV_PATH?>sidebar_add.php?edit=<?=$arr_left[$le]['Id']?>"><i class="fa fa-lg fa-pencil-square green-color" aria-hidden="true"></i></a>
        
        <a href="#" onclick="msgbox('Do you want to delete this from left sidebar?','?dellid=<?=$arr_left[$le]['Id']?>','_self','yesno')" ><i class="fa fa-lg fa-times-circle-o red-color" aria-hidden="true"></i></a>
        </div>
    </li>
    <?php
    }
    ?>
</ul>
</div>
<div class="small-5 columns big-menu sub-left-side-bar left">
<h4>ផ្នែកខាងស្តាំ</h4>
<ul class="menu">
    <?php
    for($ri=0;$ri<count($arr_right);$ri++){
    ?>
    <li class="small-12 columns">
        <div class="small-8 columns" >
        <?=$arr_right[$ri]['Label']?>
        </div>
        <div class="small-3 columns">
        <a href="<?=ADMIN_NAV_PATH?>sidebar_add.php?edit=<?=$arr_right[$ri]['Id']?>"><i class="fa fa-lg fa-pencil-square green-color" aria-hidden="true"></i></a>
        
        <a href="#" onclick="msgbox('Do you want to delete this from right sidebar?','?delrid=<?=$arr_right[$ri]['Id']?>','_self','yesno')" ><i class="fa fa-lg fa-times-circle-o red-color" aria-hidden="true"></i></a>
        </div>
    </li>
    <?php
    }
    ?>
</ul>
</div>
<?php
}#end permission
?>