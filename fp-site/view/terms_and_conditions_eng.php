<div class="small-12 medium-12 large-12 columns index-post">  
    <h4 class="w3-center kh-moullight">Terms and Conditions of Signing up the Traju Legal Index App</h4>
    
    <p style="text-indent: 50px;">Welcome to Traju Legal Index App of Sala Traju Association. These terms govern your own use of our app. So, please read them carefully:</p>
    <ul style="list-style-type: none;" >
        <li class="kh-moullight">I. General Conditions<br>
        <p>This terms are the Agreement between users and Sala Traju Association to the use of Traju legal Index App.</p>
	    <p>The terms of signing up our app are explicitly in accordance with the applicable law of Kingdom of Cambodia</p>
    </li>
        <li class="kh-moullight">II. Registration<br>
        <p>By registering to the App, you can use all provided services without registering to other services again.</p>
        <p>You totally agree that:</p>
        <ul style="padding-left:100px">
            <li>You are granted a limited license to use the services solely for personal needs and agree to secure your own username and passwords. </li>
            <li>You will not register by using inaccurate information or possessing many accounts.</li>
        </ul>
    </li>
        <li class="kh-moullight">III. Termination of Use<br>
        <p>If you are in breach of any these terms, we reserve the rights, in our sole discretion, to terminate and suspense your right to access or use of the app.</p>
    </li>
        <li class="kh-moullight">IV. Administration<br>
        <p>You can update the present details or re-set the necessary information in your profile. The new setting will be applied for your conducts in our services.
</p>
    </li>
       <li class="kh-moullight">V. Data<br>
       <p>All your provided data will be used in necessary moment without harming your own benefits.</p>
    </li>
        <li class="kh-moullight">VI. Intellectual Property  Right<br>
        <p>We are the owner of all intellectual Property Rights in the Platform and in the material published on it.  In accessing the Platform you agree that you will access its contents and use the services solely for your personal, non-commercial use or distributed without our prior written consent.</p>
    </li>
        <li class="kh-moullight">VII. Responsibilities<br>
        <p>You are solely responsible for the information that you provide in signing up the app and exclusively guarantee that you don’t infringe the copyrights, impersonate to someone or provide inaccurate information. If we find that you violate this term, you will be responsible for civil liability.</p>
        <p>If you commit illegal acts which are abusive both directly or indirectly to other’s benefits, you will also be liable to the courts.</p>
        <p>For any litigations, The Courts of Kingdom of Cambodia are competent.</p>
    </li>

        <li class="kh-moullight">VIII. Modification of terms<br>
        <p>Sala Traju Association reserve the right, at our discretion, to modify these terms of signing up the app on going forward basic at any time in accordance with the law of Kingdom of Cambodia.</p>
    </li>
    </ul>
</div>