<div class="small-12 columns big-menu w3-padding-large ">
<?php
$permission = array("Admin","Superuser","LegalUpdate","AdminLegalUpdate");
$db_name = "salatraju_legal_update";
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    if (!isset($_GET['pubid'])){
?>

<h4 class="w3-center">តារាងរាយព្រឹត្តិបត្រ</h4>
<table class="w3-table w3-bordered w3-striped w3-border test w3-hoverable">
<tbody><tr class="w3-green">
    <th>ចំណងជើង</th>
    <th></th>
</tr>
</tbody>
<tbody>
<?php



    $data_pub = db_get('publications','',"GROUP BY Id","","",$db_name);
    for ($y=0;$y<count($data_pub);$y++){
        echo '
        <tr><td><a href="'.VIRTUAL_PATH.'index.php/lg_update_list?pubid='.$data_pub[$y]['Id'].'">'.$data_pub[$y]["Title"].'</a></td></tr>
        ';
    }

}
?>
</tbody>
</table>
<?php
if (isset($_GET['pubid'])){
    $prior = array("Admin","Superuser","AdminLegalUpdate");
    if (in_array($_SESSION['infos']['role'],$prior)){
        $data_meteka = db_get('Meteka','WHERE PublicationId="'.$_GET['pubid'].'"',"","","",$db_name);
    }else{
        $data_meteka = db_get('Meteka','WHERE Author="'.$_SESSION['infos']['id'].'" AND PublicationId="'.$_GET['pubid'].'"',"","","",$db_name);
    }
    
?>
<h4 class="w3-center">តារាងរាយមាតិកា</h4>
<table class="w3-table w3-bordered w3-striped w3-border test w3-hoverable">
<tbody><tr class="w3-green">
    <th>ចំណងជើង</th>
    <th>ចំណងជើងរង</th>
    <th>អត្ថបទសង្ខេប</th>
    <th>ស្ថានភាព</th>
    <th></th>
</tr>
</tbody>
<tbody>
<?php

for($i=0;$i<count($data_meteka);$i++){
    if ($data_meteka[$i]['Status'] == 1){
        $status = 'មិនទាន់ចុះផ្សាយ';
    }elseif ($data_meteka[$i]['Status'] == 2){
        $status = 'បានចុះផ្សាយ';
    }
    echo '
    <tr>
    <td><a href="'.VIRTUAL_PATH.'index.php/lg_update_meteka?metekaid='.$data_meteka[$i]['Id'].'">'.$data_meteka[$i]['MetekaTitle'].'</a></td><td>'.$data_meteka[$i]['MetekaSubTitle'].'</td><td>'.htmlspecialchars_decode($data_meteka[$i]['ShortDescription'],ENT_QUOTES).'</td>
    <td>'.$status.'</td>
    <td><a href="#" class="w3-button w3-red" onclick="msgbox('."'Do you really want to delete this?','".THIS_PAGE."?DelmetekaId=".$data_meteka[$i]['Id']."&pubid=".$_GET['pubid']."','_self','yesno'".');">Delete</a></td>
    </tr>
    ';
}
?>
</tbody>
</table>
<?php   
if (isset($_GET['DelmetekaId'])){
    $cond = array("Id"=>$_GET['DelmetekaId']);
    $meteka = db_get("Meteka","WHERE Id='".$_GET['DelmetekaId']."'","","","",$db_name);
    activity_log("Legal Update","Deleted Meteka :'".$meteka[0]['MetekaTitle']."'" );
    db_delete('Meteka',$cond,$db_name);
    
    header("Location: ".VIRTUAL_PATH."index.php/lg_update_list?pubid=".$_GET['pubid']);
}
}
}
?>
</div>