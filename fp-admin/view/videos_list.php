<?php
$permission = array("Admin","Superuser","Editor");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if (isset($_GET['id'])){
    $res = db_get('videos','Where Id="'.$_GET['id'].'"');
    $Id = $res[0]['Id'];
    $title = $res[0]['Title'];
    $Url = $res[0]['Url'];
?>
<div class="small-12 columns big-menu">
    <h4>Video from youtube only</h4>
    <form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
        <div class="small-12">
            <label for="title">Video's Title*
                <input type="hidden" value="<?=$Id?>" name="Id"/>
                <input type="text" name="Title" placeholder="Please enter title here." value="<?=$title?>" required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">Youtube's url*
                <input type="text" name="Url" value="<?=$Url?>" placeholder="Example: https://youtu.be/cu4aA9OfwS4" onkeyup="youtube_preview('ypreview',this.value)" required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <input class="button" type="submit" name="submit" value="Save">
        </div>
    </div>
    </form>
    <h4>Youtube preview</h4>
    <iframe width="560" height="315" frameborder="0" allowfullscreen id="ypreview"></iframe>
    
</div>
<?php
}elseif (isset($_POST['submit'])){
    $val = array(
    'Title' => $_POST["Title"],
    'Url' => $_POST["Url"]
    );
    $cond = array('Id'=>$_POST['Id']);
    db_update('videos',$val,$cond);
    header("Location: ".THIS_PAGE);
}elseif (isset($_GET['delid'])){
    $cond = array('Id'=>$_GET['delid']);
    db_delete('videos',$cond);
    header("Location: ".THIS_PAGE);
}else{
    db2table('videos',"Video's List","yes");
}

}#end permission