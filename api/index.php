<?php
/*
Needed item
- title : string; Done
- created_date : string; Done
- category : string; Done
- content : string
- doc_url : string // PDF document link
- author : string; Done
*/
include '../fp-admin/config/config.php';
if (isset($_GET["api"])){
    $res = db_get("fp_posts","Where PostId='".$_GET["api"]."'");
}else{
    $res = db_get("fp_posts");
}


$myObj = new \stdClass();
$api = array();
$cat_name = array();
$arr_test["Id"] = array();$arr_test["PostAuthor"]=array();
$arr_test["PostTitle"] = array();$arr_test["PostContent"] = array();
$arr_test["PostStatus"] = array();$arr_test["PostType"] = array();
$arr_test["PostModify"] = array();$arr_test["Catergories"] = array();
$arr_test["DocUrl"] = array();
for($i=0;$i<count($res);$i++){
    $start_index = stripos($res[$i]["PostContent"],"http://drive.google.com");
    $end_index = stripos($res[$i]["PostContent"],'/view');
    $len = $end_index - $start_index + 5;
    $doc_url = substr($res[$i]["PostContent"],$start_index,$len);
    $res_user = db_get("fp_users","Where Id='".$res[$i]["PostAuthor"]."'");
    if (count($res_user)>0){
        $au_name = $res_user[0]["LastName"]." ".$res_user[0]["FirstName"];
    }else{
        $au_name = "";
    }
    $content = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["PostContent"],ENT_QUOTES));
    $content = str_replace("\n",chr(32),$content);
    $content = str_replace("\r",chr(32),$content);
    if ($res[$i]["PostStatus"] == "1"){
        $PostStatus = "Saved";
    }else if ($res[$i]["PostStatus"] == "2"){
        $PostStatus = "Published";
    }else if ($res[$i]["PostStatus"] == "3"){
        $PostStatus = "Modified";
    }else{
        $PostStatus = "Unknow";
    }
    
    

    if ($res[$i]['Catergories'] == "" || $res[$i]['Catergories'] == NULL){
        
    }else{
        $cat = explode(';',$res[$i]['Catergories']);
        if (count($cat) == 1){
            $name_cat = db_get('catergories',"Where Id='".$cat[0]."'");
            for($z=0;$z<count($name_cat);$z++){
                array_push($arr_test["Catergories"],$name_cat[$z]['Name']);
            }
            
        }else if(count($cat) > 1){
            $cat_name = array();
            for($y=0;$y<count($cat);$y++){
                $name_cat = db_get('catergories',"Where Id='".$cat[$y]."'");
                for($z=0;$z<count($name_cat);$z++){
                    array_push($cat_name,$name_cat[$z]['Name']);
                }   
            }
            
            
        }
    }
    array_push(
        $api,array(
            'PostId'=>$res[$i]["PostId"],
            'PostAuthor'=>$au_name,
            'PostTitle'=>html_entity_decode($res[$i]["PostTitle"],ENT_QUOTES),
            'PostContent'=>$content,
            'PostStatus'=>$PostStatus,
            'DocUrl'=>$doc_url,
            'PostType'=>$res[$i]['PostType'],
            'PostModify'=>$res[$i]['PostModify'],
            'Catergories'=>$res[$i]['Catergories'],
        )
    );
}
//print_r($arr_test)
$myJSON = json_encode(array("api"=>$api), JSON_PRETTY_PRINT);
echo $myJSON;
/*
$myObj->id = $res[0]["PostId"];
$myObj->PostAuthor = $res[0]["PostAuthor"];
$myObj->PostDate = $res[0]["PostDate"];
//$myObj->PostTitle = $res[0]["PostTitle"];
//$myObj->PostContent = $res[0]["PostContent"];
$myJSON = json_encode($myObj);

echo $myJSON;
*/
?>
