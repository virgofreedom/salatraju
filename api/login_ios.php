<?php

include '../fp-admin/config/config.php';

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$myObj = new \stdClass();
$userinfos = "";

$login_email = $_POST["loginEmail"];
$login_password = $_POST["loginPassword"];
$device_id = $_POST["deviceId"];
$data = array("Email"=>$login_email);
$action = $_POST['action'];
$cond_device = array("Email"=>$login_email,"DeviceId"=>$device_id);
$val_device = array("Email"=>$login_email,"DeviceId"=>$device_id);
if ($action == "login"){//Login

    $res_device = db_get_where('devices_in_use',$data);
    $count_email = count($res_device);
    if ( $count_email == 0){//Not found email so add email and device
        db_insert("devices_in_use",$val_device);
        $login_data = login();
    }else{//found email so count
        if ($count_email <= 10){// Limit 2 devices
            $limit_device = db_get_where("devices_in_use",$cond_device);
            if(count($limit_device) == 1 ){// if found email and device id so login
                $userinfos = login();
            }else{// if email and device id not found so count email again and if email < 2 add new device id
                if ($count_email < 10){//add device id change also limit here too.
                    db_insert("devices_in_use",$val_device);
                    $userinfos = login();
                }else{
                    http_response_code(400);
                    $userinfos=array(
                        "error"=> 3,
                        "reason"=> "Your Device is over limit"
                    );
                }
                
            }
        }
    }
        
    
}elseif ($action == "logout"){//logout action here
    http_response_code(200);
    db_delete("devices_in_use",$cond_device);
        $userinfos=array(
            "action"=> "logout",
            "reason"=> "Logout Successfully!"
        );
}

function login(){
    $login_email = $_POST["loginEmail"];
    $login_password = $_POST["loginPassword"];
    $data = array("Email"=>$login_email);
    $result = db_get_where('subscribers',$data);
    if (count($result) > 0){
        for ($i=0;$i<count($result);$i++){
            if (crypt($login_password,KEY_ENCRYPT)  == $result[$i]['Password'])
            { //password correct
                $email = $result[$i]['Email'];
                $LastNameKh = $result[$i]['LastNameKh'];
                $FirstNameKh = $result[$i]['FirstNameKh'];
                $LastName = $result[$i]['LastName'];
                $FirstName = $result[$i]['FirstName'];
                $DateJoin = $result[$i]['DateJoin'];
                $UserId = $result[$i]['UserId'];
                $versioncode = 5;
                $link2update = "https://apps.apple.com/us/app/traju-law/id1536176748";
                $income = db_get('Income','WHERE UserId="'.$result[$i]['UserId'].'"');
                $DateRenew = $income[0]['DateRegistration'];
                $DateExpires = $income[0]['DateExpired'];
                // Here is the specially fornate string response.
            $userinfos=array(
                "userid"=>$UserId,
                "email"=>$email,
                "lastname"=>$LastName,
                "firstname"=>$FirstName,
                "lastnamekh"=>$LastNameKh,
                "firstnamekh"=>$FirstNameKh,
                "datejoin"=>$DateJoin,
                "dateexpires"=>$DateExpires,
                "version"=>$versioncode,
                "link2update"=>$link2update,
            );
            // set response code - 200 OK
                http_response_code(200);
            }else{
                // password not correct
                $userinfos=array(
                    "error"=> 1,
                    "reason"=> "your email and password is not match with our records. please enter again."
                );
                // set response code - 200 OK
                http_response_code(400);
            }
        }
        
    }else{
        // set response code - 200 OK
        http_response_code(400);
        $userinfos=array(
            "error"=> 2,
            "reason"=> "No data found!"
        );
    }
    return $userinfos;
}



$myJSON = json_encode($userinfos, JSON_PRETTY_PRINT);
echo $myJSON;