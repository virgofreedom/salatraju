<?php
$res = db_get('view_logs','Where Date="'.date("Y-m-d").'" AND IpAddress="'.$_SERVER['REMOTE_ADDR'].'"');
// echo count($res)."<br>";
// var_dump($_SESSION);

#check the session if exist or not
if (isset($_SESSION['userinfos']))
{//if session password exist

    $today = date("d");
    $today_date = strtotime(date("Y-m-d"));
    $last_visit = date_create($_SESSION['userinfos']['LastVisit']);
    $last_visitt = date_format($last_visit,"Y-m-d");
    $last_visited = date_format($last_visit,"d");
    $expire_date = strtotime($_SESSION['userinfos']['DateExpired']);
    $expire_date_Y = date_format($expire_date,"Y");
    $expire_date_m = date_format($expire_date,"m");
    $expire_date_d = date_format($expire_date,"d");
    

    
    

    $logout = '';
    $cond = array(
        'Email'=>$_SESSION['userinfos']['Email'],
        'Password'=>$_SESSION['userinfos']['Password']
    );
    $result = db_get_where('subscribers',$cond);
    
    
    if (count($result) == 0){
        echo 'unset1';
        #the session is difference or not correct send to login page
        unset($_SESSION['userinfos']);
        unset($_SESSION['usererror']);
        header("Location: ".DOMAIN.key($_GET));
    }
    if ($today != $last_visited){
        echo 'unset2';
        unset($_SESSION['userinfos']);
        unset($_SESSION['usererror']);
        header("Location: ".DOMAIN.key($_GET));
    }
    if (date("Y") == $expire_date_Y){
        if (date("m") == $expire_date_m){    
            if (date("d") != $expire_date_d){    
                
                $val = array(
                    "Status"=>2
                );
                $cond = array(
                    "Email"=>$_SESSION['userinfos']['Email']
                );
                db_update ('subscribers',$val,$cond);
                $_SESSION['userinfos']['Status'] = 2;

            }
        }
    }
    // $Id = $_SESSION['userinfos']['Id'];
    // $UserId = $_SESSION['userinfos']['UserId'];
    // $LastName = $_SESSION['userinfos']['LastName'];
    // $FirstName = $_SESSION['userinfos']['FirstName'];
    // $Sex = $_SESSION['userinfos']['Sex'];
    // $Email = $_SESSION['userinfos']['Email'];
    // $Password = $_SESSION['userinfos']['Password'];
    // $DateJoin = $_SESSION['userinfos']['DateJoin'];
    // $DateExpired = $_SESSION['userinfos']['DateExpired'];
    // $LastVisit = $_SESSION['userinfos']['LastVisit'];
    
    if ($_SESSION['userinfos']['Status'] == 0){
        $status = 'មិនទាន់ផ្ទៀងផ្ទាត់ / Pending';
    }elseif ($_SESSION['userinfos']['Status'] == 1){
        $status = 'ផ្ទៀងផ្ទាត់រួចរាល់ / Activated';
    }elseif ($_SESSION['userinfos']['Status'] == 2){
        $termnate_date = date('d-m-Y', strtotime($_SESSION['userinfos']['DateExpired']. ' + 14 days')); //Set day limit here 
       
        $status = '<b class="w3-red">ផុតកំណត់ គណនីនឹងបិទនៅកាលបរិច្ចេទ'. $termnate_date.'/ Expired, account will terminate at ' . $termnate_date ."</b>";
    }elseif ($_SESSION['userinfos']['Status'] == 3){
        $status = 'គណនីបានបិទ / Account Terminated';
    }
    // Calculate the expired date
    if ($_SESSION['userinfos']['Status'] == 1){
        if ($expire_date < $today_date){
            // echo 'account expired!';
            $termnate_date = date('d-m-Y', strtotime($_SESSION['userinfos']['DateExpired']. ' + 14 days')); //Set day limit here 
            $status = '<b class="w3-red">ផុតកំណត់ គណនីនឹងបិទនៅកាលបរិច្ចេទ'. $termnate_date.'/ Expired, account will terminate at ' . $termnate_date ."</b>";
            $cond = array(
                'Email'=>$_SESSION['userinfos']['Email'],
            );
            $val = array(
                'Status'=>2
            );
            db_update ('subscribers',$val,$cond);
        }
    }
    // Compare for the new account (Status = 0)
    // If created date + 14 is smaller than today date
    // the account is terminate
    if ($_SESSION['userinfos']['Status'] == 0){ // Account terminate after 14 days of registration
        $Date = $_SESSION['userinfos']['DateJoin'];
        $termnate_date = strtotime(date('Y-m-d', strtotime($Date. ' + 14 days'))); //Set day limit here 
        if ($termnate_date < $today_date){
            //  account has been terminated
            $status = 'គណនីបានបិទ / Account Terminated';
            $cond = array(
                'Email'=>$_SESSION['userinfos']['Email'],
            );
            $val = array(
                'Status'=>3
            );
            db_update ('subscribers',$val,$cond);
        }
    }
    if ($_SESSION['userinfos']['Status'] == 2){ //if account expired so will terminate account in 14 day from the expired date
        $termnate_date = strtotime(date('Y-m-d', strtotime($_SESSION['userinfos']['DateExpired']. ' + 14 days'))); //Set day limit here 
        if ($termnate_date < $today_date){
            //  account has been terminated
            $status = 'គណនីបានបិទ / Account Terminated';
            $cond = array(
                'Email'=>$_SESSION['userinfos']['Email'],
            );
            $val = array(
                'Status'=>3
            );
            db_update ('subscribers',$val,$cond);
        }
    }
    echo '
    <div class="small-12 medium-12 large-12 columns post-block index-post">';
?>
<div class="w3-center"><h4 class="kh-moullight">ព័ត៌មានអ្នកប្រើប្រាស់ / User's Information</h4></div>

<div class="small-6 text-right columns" style="border-right:solid 1px;">
    <div class="row">
        <span class="kh-moullight">លេខសម្គាល់ខ្លួន /User's Id</span>
    </div>
    <div class="row">
        <span class="kh-moullight">នាមត្រកូល / Last name</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">នាមខ្លួន / First name</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">ភេទ / Sex</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">អ៊ីម៉ែល / Email</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">ចូលជាអ្នកប្រើប្រាស់ពី / Member since</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">ការចុះឈ្មោះចូលចុងក្រោយ / Your last login</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">ថ្ងៃផុតកំណត់ប្រើប្រាស់ / Expiry date</span>
    </div>
    <div class="row">
        <span class="kh-moullight ">ស្ថានភាព / Status</span>
    </div>
</div>
<div class="small-6 text-left columns">
    <div class="row">
        <span><?=$_SESSION['userinfos']['UserId']?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['LastName']?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['FirstName']?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['Sex']?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['Email']?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['DateJoin']?></span>
    </div>
    <div class="row">
        <span><?=$last_visitt?></span>
    </div>
    <div class="row">
        <span><?=$_SESSION['userinfos']['DateExpired']?></span>
    </div>
    <div class="row">
        <span><?=$status?></span>
    </div>
</div>
<div class="small-12 text-left columns w3-center "><a class=" kh-content button success" href="<?=DOMAIN?>subscribe_logout">ចាកចេញ / Logout</a></div>
<?php
    echo '</div>';
    if ($_SESSION['userinfos']['Status'] == 3){
        die;
    }// Compare for the new account (Status = 0)
    // If created date + 14 is smaller than today date
    // the account is terminate
    if ($_SESSION['userinfos']['Status'] == 0){
        $Date = $_SESSION['userinfos']['DateJoin'];
        $termnate_date = strtotime(date('Y-m-d', strtotime($Date. ' + 14 days')));
        if ($termnate_date < $today_date){
            //  account has been terminated
           die;
        }
    }
    if ($_SESSION['userinfos']['Status'] == 2){ //if account expired so will terminate account in 14 day from the expired date
        $termnate_date = strtotime(date('Y-m-d', strtotime($_SESSION['userinfos']['DateExpired']. ' + 14 days'))); //Set day limit here 
        if ($termnate_date < $today_date){
            //  account has been terminated
           die;
        }
    }
}else{//if not check the number of use today
    if (count($res) > 10 && key($_GET)!= 'subscribe_form' && key($_GET)!='subscribe_login' && key($_GET) != 'subscribe_loging'){
    echo '
            <div class="small-12 medium-12 large-12 columns post-block index-post">
            <div class="w3-center"><h4 class="kh-moullight">
                សូមអភ័យទោស ការចូលមើលវេបសាយរបស់លោកអ្នក សម្រាប់ថ្ងៃនេះគ្រប់ចំនួនកំណត់! <br>
                <a href="'.DOMAIN.'subscribe_form">សូមចុចត្រង់នេះ ដើម្បីធ្វើការចុះឈ្មោះ!</a><br>
                ​​ សូមអរគុណ!
            </h4></div>
            </div>
        ';  
        include PHYSICAL_PATH_SITE.'themes/'.THEME_TITLE.'/include/footer.php';
        die;
    }
}

?>