                
            </div>
            <!-- End  content area-->
          </div>
      
      <!-- Start Right Sidebar -->
      
      <div class="small-12 medium-3 large-3 columns">
          <div class="index-post">       
            <?php
            /*
            if (isset($_SESSION['userinfos'])){
              
              echo '
                <b class="kh-moullight">ព័តមានសមាជិក / User\'s Information</b>

                
                
                    <div class="row">
                        <span>'.$_SESSION['userinfos']['LastName'].'</span>
                    </div>
                    <div class="row">
                        <span>'.$_SESSION['userinfos']['FirstName'].'</span>
                    </div>
                    <div class="row">
                        <span>'.$_SESSION['userinfos']['Sex'].'</span>
                    </div>
                    <div class="row">
                        <span>'.$_SESSION['userinfos']['Email'].'</span>
                    </div>
                <div class="w3-center"><a class="kh-content button success" href="'.DOMAIN.'subscribe_logout">ចាកចេញ / Logout</a></div>

              ';
            }else{
              echo '<p><b>សមាជិកភាព</b></p>';
              echo '<a href="'.DOMAIN.'subscribe_login">ចុះឈ្មោះចូល / Member Login</a>';
            }
            */
            ?>
            
          </div>
      </div>
      <?php
      sidebar_right('index-post','medium-3 large-3');
      ?>
      <!-- End Right Sidebar -->
      <footer class="small-12 columns">
              <div class="small-12 medium-4 large-4 columns text-left">
              Sala Traju Association<br>
              #31C, Street 222, Boeung Raing, Daun Penh, <br>
              Phnom Penh 13253 Cambodia<br>
              Tell : 023 541 888, <br>E-mail: traju.infos@gmail.com

            </div>
            <div class="small-12 medium-4 large-4 columns ">
              Design by Freedomteam &copy;2016-2017 salatraju<br>
              <a href="<?=DOMAIN?>fp-admin/index.php/login.php" style="color:white;">Admin login</a> | 
            </div>
            
            <div class="small-12 medium-4 large-4 columns ">
            <a href="https://www.facebook.com/sala.traju/"><i class="fa fa-facebook-official fa-2x" style="color:white;" aria-hidden="true"></i>
            </a>
            <a href="https://twitter.com/SalaTraju">
            <i class="fa fa-twitter-square fa-2x" style="color:white;" aria-hidden="true"></i>
            </a>
            <a href="https://www.youtube.com/channel/UCF8s227_vV_Tsl5fvgwVfDQ"><i class="fa fa-youtube fa-2x" style="color:white;" aria-hidden="true"></i></a>
            <a href="https://www.linkedin.com/in/sala-traju-association-37a258128" ><i class="fa fa-linkedin-square fa-2x" style="color:white;" aria-hidden="true"></i>
            <a href="https://www.instagram.com/sala.traju/"><i class="fa fa-instagram fa-2x" style="color:white;" aria-hidden="true"></i></a>
</a><br><span>សូមរក្សាទំនាក់ទំនងជាមួយយើង</span>
            </div>
              
            </div>
            
            
      </footer>
      </div>
      <!-- End page content-->
    </div>
    <!-- End off-canvas-wrapper-inner -->
  </div>
    <!-- End off-canvas-wrapper -->

    
    
    
    <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/vendor/jquery.js"></script>
    <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/vendor/what-input.js"></script>
    <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/vendor/foundation.min.js"></script>
    <script src="<?=VIRTUAL_PATH?>js/app.js"></script>
  </body>
</html>