<div class="small-12 medium-12 large-12 columns post-block index-post">
<?php
if (!isset($_GET['Id'])){
?>

<script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>

    <h4>1.ជីវប្រវត្តិរូបផ្ទាល់ខ្លួន</h4>
<form action="member_add.php" method="POST" enctype="multipart/form-data"/>
    <div class="row">
        <div class="small-5 columns">
            <img src="<?php echo VIRTUAL_PATH . "img/picID.jpg";?>" id="img_prev"/>
        </div>
        <div class="small-6 columns left">
			<input type="file" name="FileToUpload" id="FileToUpload" onchange="img_view(this,'#img_prev','128','156')"/><br/>
	    </div>
    </div>
    <div class="row">
        <div class="small-5 columns">
            <label for="title">នាម គោត្តនាម(ខ្មែរ):*
                <input type="text" name="Name" value=""  required/>
            </label>
        </div>
        <div class="small-5 columns">
            <label for="title">នាម គោត្តនាម(អក្សរឡាតាំង):*
                <input type="text" name="Name_Latin" value=""  required/>
            </label>
        </div>
        <div class="small-2 columns">
            <label for="title">ភេទ:*
                <select name="Sex" id="Sex">
                    <option value="ប្រុស">ប្រុស</option>
                    <option value="ស្រី">ស្រី</option>
                </select>
            </label>
        </div>
    </div>
    
    <div class="row">
        <div class="small-6 columns">
            <label for="title">ថ្ងៃខែឆ្នាំកំណើត:*
                <input type="date" name="Dob" placeholder="ខែ/ថ្ងៃ/ឆ្នាំ" value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">ទីកន្លែងកំណើត:*
                <input type="text" name="Pob" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">មុខរបរបច្ចុប្បន្ន:*
                <input type="text" name="Job" value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">អង្គភាព:*
                <input type="text" name="Company" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">អាសយដ្ឋានបច្ចុប្បន្ន:*
                <input type="text" name="Address" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">លេខទូរសព្ទ:*
                <input type="text" name="Phone" value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">E-mail:*
                <input type="text" name="Email" value=""  required/>
            </label>
        </div>
    </div>
</div>
<div class="small-12 medium-12 large-12 columns post-block index-post">
    <h4>2.ស្ថានភាពគ្រួសារ</h4>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">ឈ្មោះឪពុក:*
                <input type="text" name="Dad_Name"  value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">មុខរបរ:*
                <input type="text" name="Dad_Job" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">ឈ្មោះម្តាយ:*
                <input type="text" name="Mom_Name" value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">មុខរបរ:*
                <input type="text" name="Mom_Job" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">ឈ្មោះប្តី/ប្រពន្ធ:*
                <input type="text" name="Spouse_Name"  value=""  required/>
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">មុខរបរ:*
                <input type="text" name="Spouse_Job" value=""  required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">អាសយដ្ឋានបច្ចុប្បន្ន:
                <input type="text" name="Fam_Address"  value=""/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">លេខទូរសព្ទ:
                <input type="text" name="Fam_Phone"  value="" />
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">E-mail:
                <input type="text" name="Fam_Email" value=""/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
        <label for="title">អ្នកដែលអាចទាក់ទងបានករណីចាំបាច់</label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">ឈ្មោះ:
                <input type="text" name="Contact_Name"  value="" />
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">ត្រូវជា:
                <input type="text" name="Relation" value=""/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">លេខទូរសព្ទ:
                <input type="text" name="Contact_Phone" value="" />
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">E-mail:
                <input type="text" name="Contact_Email" value=""/>
            </label>
        </div>
    </div>
</div>
<div class="small-12 medium-12 large-12 columns post-block index-post">
    <h4>3.ព័ត៌មានពាក់ព័ន្ធនឹងសមាគម</h4>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">-តើអ្នកស្គាល់សមាគមសាលាត្រាជូតាមរយៈអ្វី?
                <input type="text" name="How" placeholder="" value="" />
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">-ហេតុអ្វីបានជាអ្នកចាប់អារម្មណ៍ចូលជាសមាជិកសមាគមសាលាត្រាជូ?
                <input type="text" name="Why" placeholder="" value="" />
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">-តើអ្នកធ្លាប់ទទួលបានព័ត៌មានអ្វីខ្លះពីសមាគមសាលាត្រាជូ?
                <input type="text" name="Use" placeholder="" value="" />
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <label for="title">-តើអ្នករំពឹងថានឹងទទួលបានអ្វីខ្លះពីសមាគមសាលាត្រាជូ?
                <input type="text" name="Think" placeholder="" value="" />
            </label>
        </div>
    </div>
    <p>
        ខ្ញុំសូមធានាអះអាងថា ប្រវត្តិរូបរបស់ខ្ញុំ និងព័ត៌មានខាងលើ គឺពិតជាត្រឹមត្រូវប្រាកដមែន ក្នុងករណី​ខុស​​ពី​ការពិត ខ្ញុំសូមទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។
	សូម លោកប្រធានសមាគមសាលាត្រាជូ មេត្តាអនុញ្ញាតឱ្យរូបខ្ញុំបាទ/នាងខ្ញុំ ចូលជា៖
    </p>
    <div class="row">
        <div class="small-6 columns">
            <label for="title">សមាជិកសកម្ម
                <input type="radio" name="status" placeholder="" value="សមាជិកសកម្ម" />
            </label>
        </div>
        <div class="small-6 columns">
            <label for="title">សមាជិកគាំទ្រ
                <input type="radio" name="status" placeholder="" value="សមាជិកគាំទ្រ" />
            </label>
        </div>
    </div>
    <p>របស់សមាគមសាលាត្រាជូ ដោយក្តីអនុគ្រោះបំផុត។</p>
    <p>ខ្ញុំបាទ/នាងខ្ញុំ សូមសន្យាថា គោរព និងអនុវត្តតាមលក្ខន្តិកៈ និងបទបញ្ជាផ្ទៃ​ក្នុងរបស់សមាគម​សាលា​ត្រាជូ។</p>
    
    <div class="row">
        <div class="small-12">
            <input type="submit" class="button right" name="Submit" value="យល់ព្រម"/>
            
        </div>  
    </div>
</form>


<?php
}else{
    
}
?>
</div>