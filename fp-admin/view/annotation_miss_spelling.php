<div class="small-12 columns big-menu">
    <?php
    if(isset($_POST['save'])){
        $wrong_word = $_POST['Word'];
        $correct_word = $_POST['UpdateWord'];
        $correct_krom = array();
        $correct_kinthi = array();
        $correct_metika = array();
        $correct_chapter = array();
        $correct_section = array();
        $correct_kathapheak = array();
        $correct_metraTitle = array();
        $correct_metraDescription = array();
        $res_krom = db_get('krom',"WHERE kromTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_kinthi = db_get('kinthi',"WHERE kinthiTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_metika = db_get('metika',"WHERE metikaTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_chapter = db_get('chapter',"WHERE chapterTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_section = db_get('section',"WHERE sectionTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_kathapheak = db_get('kathapheak',"WHERE kathapheakTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_metraTitle = db_get('metra',"WHERE metraTitle LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        $res_metraDescription = db_get('metra',"WHERE metraDescription LIKE '%$wrong_word%'",'',"","",DB_NAME2);
        // Krom
        if (count($res_krom)==0){
            echo '<h4>There is no data found in krom!</h4>';
            
        }else{
            for ($i=0;$i<count($res_krom);$i++){
                array_push($correct_krom,
                    array(
                        'kromId'=>$res_krom[$i]['kromId'],
                        'kromTitle'=>str_ireplace($wrong_word,$correct_word,$res_krom[$i]['kromTitle'])
                        )
                );
            }
            echo '<h4>ក្រមដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_krom);$i++){
                $cond = array("kromId"=>$correct_krom[$i]['kromId']);
                $val = array("kromTitle"=>$correct_krom[$i]['kromTitle']);
                db_update('krom',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_krom[$i]['kromTitle'].'</td></tr>';
            }
            
            echo '</table>';
        }

        // Kinthi
        if (count($res_kinthi)==0){
            echo '<h4>There is no data found in Kinthi!</h4>';
            
        }else{
            for ($i=0;$i<count($res_kinthi);$i++){
                array_push($correct_kinthi,
                    array(
                        'kinthiId'=>$res_kinthi[$i]['kinthiId'],
                        'kinthiTitle'=>str_ireplace($wrong_word,$correct_word,$res_kinthi[$i]['kinthiTitle'])
                        )
                );
            }
            echo '<h4>គន្ថីដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_kinthi);$i++){
                $cond = array("kinthiId"=>$correct_kinthi[$i]['kinthiId']);
                $val = array("kinthiTitle"=>$correct_kinthi[$i]['kinthiTitle']);
                db_update('kinthi',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_kinthi[$i]['kinthiTitle'].'</td></tr>';
            }
            echo '</table>';
            
        }

        // Metika
        if (count($res_metika)==0){
            echo '<h4>There is no data found in metika!</h4>';
            
        }else{
            for ($i=0;$i<count($res_metika);$i++){
                array_push($correct_metika,
                    array(
                        'metikaId'=>$res_metika[$i]['metikaId'],
                        'metikaTitle'=>str_ireplace($wrong_word,$correct_word,$res_metika[$i]['metikaTitle'])
                        )
                );
            }
            echo '<h4>មាតិកា ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_metika);$i++){
                $cond = array("metikaId"=>$correct_metika[$i]['metikaId']);
                $val = array("metikaTitle"=>$correct_metika[$i]['metikaTitle']);
                db_update('metika',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_metika[$i]['metikaTitle'].'</td></tr>';
            }
            echo '</table>';
        }

        // Chapter
        if (count($res_chapter)==0){
            echo '<h4>There is no data found in chapter!</h4>';
            
        }else{
            for ($i=0;$i<count($res_chapter);$i++){
                array_push($correct_chapter,
                    array(
                        'chapterId'=>$res_chapter[$i]['chapterId'],
                        'chapterTitle'=>str_ireplace($wrong_word,$correct_word,$res_chapter[$i]['chapterTitle'])
                        )
                );
            }
            echo '<h4>ជំពូក ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_chapter);$i++){
                $cond = array("chapterId"=>$correct_chapter[$i]['chapterId']);
                $val = array("chapterTitle"=>$correct_chapter[$i]['chapterTitle']);
                db_update('chapter',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_chapter[$i]['chapterTitle'].'</td></tr>';
            }
            echo '</table>';
        }

        // section
        if (count($res_section)==0){
            echo '<h4>There is no data found in section!</h4>';
            
        }else{
            for ($i=0;$i<count($res_section);$i++){
                array_push($correct_section,
                    array(
                        'sectionId'=>$res_section[$i]['sectionId'],
                        'sectionTitle'=>str_ireplace($wrong_word,$correct_word,$res_section[$i]['sectionTitle'])
                        )
                );
            }
            echo '<h4>ផ្នែក ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_section);$i++){
                $cond = array("sectionId"=>$correct_section[$i]['sectionId']);
                $val = array("sectionTitle"=>$correct_section[$i]['sectionTitle']);
                db_update('section',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_section[$i]['sectionTitle'].'</td></tr>';
            }
            echo '</table>';
        }

        // kathapheak
        if (count($res_kathapheak)==0){
            echo '<h4>There is no data found in kathapheak!</h4>';
           
        }else{
            for ($i=0;$i<count($res_kathapheak);$i++){
                array_push($correct_kathapheak,
                    array(
                        'kathapheakId'=>$res_kathapheak[$i]['kathapheakId'],
                        'kathapheakTitle'=>str_ireplace($wrong_word,$correct_word,$res_kathapheak[$i]['kathapheakTitle'])
                        )
                );
            }
            echo '<h4>កថាភាគ ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_kathapheak);$i++){
                $cond = array("kathapheakId"=>$correct_kathapheak[$i]['kathapheakId']);
                $val = array("kathapheakTitle"=>$correct_kathapheak[$i]['kathapheakTitle']);
                db_update('kathapheak',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_kathapheak[$i]['kathapheakTitle'].'</td></tr>';
            }
            echo '</table>';
        }

        // Metra Title
        if (count($res_metraTitle)==0){
            echo '<h4>There is no data found in metraTitle!</h4>';
           
        }else{
            for ($i=0;$i<count($res_metraTitle);$i++){
                array_push($correct_metraTitle,
                    array(
                        'metraId'=>$res_metraTitle[$i]['metraId'],
                        'metraTitle'=>str_ireplace($wrong_word,$correct_word,$res_metraTitle[$i]['metraTitle'])
                        )
                );
            }
            echo '<h4>ចំណងជើងមាត្រា ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_metraTitle);$i++){
                $cond = array("metraId"=>$correct_metraTitle[$i]['metraId']);
                $val = array("metraTitle"=>$correct_metraTitle[$i]['metraTitle']);
                db_update('metra',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_metraTitle[$i]['metraTitle'].'</td></tr>';
            }
            echo '</table>';
        }

        // Metra Description
        if (count($res_metraDescription)==0){
            echo '<h4>There is no data found in metraDescription!</h4>';
            return;
        }else{
            for ($i=0;$i<count($res_metraDescription);$i++){
                array_push($correct_metraDescription,
                    array(
                        'metraId'=>$res_metraDescription[$i]['metraId'],
                        'metraDescription'=>str_ireplace($wrong_word,$correct_word,$res_metraDescription[$i]['metraDescription'])
                        )
                );
            }
            echo '<h4>និយមន័យមាត្រា ដែលបានកែហើយ!</h4>';
            echo '<table>';
            for($i=0;$i<count($correct_metraDescription);$i++){
                $cond = array("metraId"=>$correct_metraDescription[$i]['metraId']);
                $val = array("metraDescription"=>$correct_metraDescription[$i]['metraDescription']);
                db_update('metra',$val,$cond,DB_NAME2);
                echo '<tr><td>'.$correct_metraDescription[$i]['metraDescription'].'</td></tr>';
            }
            echo '</table>';
        }

    }else{
    ?>
    <form action="<?=THIS_PAGE?>" method="POST">
        <div class="row">
            <div class="small-12">
                <label for="title">Wrong word:*
                    <input type="text" name="Word" value="" placeholder="" required/>
                </label>
            </div>
        </div>
        <div class="row">
        <label for="title">Correct word:*
                    <input type="text" name="UpdateWord" value="" placeholder="" required/>
        </label>
        </div>
        
        <div class="row">
            <div class="small-12">
                <input type="submit" class="button right" name="save" value="Save"/>
            </div>  
        </div>
    </form>
    <?php
    }
    
    ?>
</div>