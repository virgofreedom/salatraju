<?php

include 'config/config.php';
startSession();
include 'library/admin.php';

if (isset($_GET['Id'])){
    $cond = array("Id"=>$_GET['Id']);
    $res = db_get_where('registration',$cond);
    $Name = $res[0]['KhName'];$Name_Latin = $res[0]['EnName'];$Sex = $res[0]['Sex'];$Dob = format_date($res[0]['Dob'],"d-m-Y");
    $Pob = $res[0]['Pob'];$Job = $res[0]['Job'];$Company = $res[0]['Company'];$Address = $res[0]['Address'];
    $Phone = $res[0]['Phone'];$Email = $res[0]['Email'];$Dad_Name = $res[0]['DadName'];
    $Dad_Job = $res[0]['DadJob'];$Mom_Name = $res[0]['MomName'];$Mom_Job = $res[0]['MomJob'];$Spouse_Name = $res[0]['SpouseName'];
    $Spouse_Job = $res[0]['SpouseJob'];$Fam_Address = $res[0]['FamAdd'];$Fam_Phone = $res[0]['FamPhone'];
    $Fam_Email = $res[0]['FamEmail'];$Contact_Name = $res[0]['ContactName'];$Relation = $res[0]['Relationship'];
    $Contact_Phone = $res[0]['ContactPhone'];$Contact_Email = $res[0]['ContactEmail'];$How = $res[0]['How'];
    $Why = $res[0]['Why'];$Use = $res[0]['Used'];$Think = $res[0]['Think'];$status = $res[0]['Status'];
    $img_path = VIRTUAL_PATH."ckeditor/plugins/imageuploader/uploads/members/".$res[0]['Img'];
    $day = format_date($res[0]["Date"],"d");
    $m = khmer_month(format_date($res[0]["Date"],"m"));
    $year = format_date($res[0]["Date"],"Y");
    
}
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?=$titile?></title>
        <link rel="stylesheet" href="http://salatraju.com/fp-site/themes/salatraju/khmerfonts/font.css">
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/khmerfonts/font.css" />
        
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/foundation.css" />
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/souris.css" />
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/w3.css" /><!-- w3.css use for do the slideshow-->
        <link rel="stylesheet" href="http://salatraju.com/fp-site/themes/salatraju/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/main.css">
        <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/virgo.css" />
        
        <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/vendor/modernizr.js"></script>
        <script src="<?=VIRTUAL_PATH?>js/virgo.js"></script>
        <style>
        p{
            text-align: justify;
            text-indent:5%;
            font-family: Kh_content;
        }
        </style>
    </head>
    
    <body style="width:695.3px;height:825px;">
        <div class="small-12 medium-12 large-12 columns post-block index-post">
            <div class="row">
            <div style="width:50%;float:left;">
                <div style="width:128px;">
                    <img src="<?=VIRTUAL_PATH?>img/logo_salatraju.png" alt="logo salatraju">
                    <h6 style="text-align: center;fon-size:small;line-height:0.5;">សមាគមសាលាត្រាជូ</h6>
                    <h6 style="text-align: center;font-size:10px;line-height:0.5;">SALATRAJU ASSOCIATION</h6>
                </div>
                
            </div>
            <div style="width:50%;float:left;">
                <h6 style="text-align: center;font-family: Kh_MuolLight;font-size:18px">ព្រះរាជាណាចក្រកម្ពុជា</h6>
                <h6 style="text-align: center;font-family: Kh_MuolLight;font-size:14px">ជាតិ​ សាសនា ព្រះមហាក្សត្រ</h6>
            </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <h6 style="text-align: center;font-family: Kh_MuolLight;font-size:12px">ពាក្យសុំចូលជាសមាជិក</h6>    
                </div>
            </div>
            <div class="row">
                <div class="right">
                    <img src="<?=$img_path?>" alt="Your Pic is here" style="width:128px;">
                </div>
                
            </div>
            <h5>១.ប្រវត្តិរូបផ្ទាល់ខ្លួន</h5>
            <p>ខ្ញុំបាទ/នាងខ្ញុំឈ្មោះ <b><?=$Name?></b> អក្សរឡាតាំង <b><?=$Name_Latin?></b> ភេទ <b><?=$Sex?></b></p>
            <p>ថ្ងៃខែឆ្នាំកំណើត <b><?=$Dob?></b> ទីកន្លែងកំណើត <b><?=$Pob?></b></p>
            <p>មុខរបរបច្ចុប្បន្ន <b><?=$Job?></b> អង្គភាព <b><?=$Company?></b></p>
            <p>អាសយដ្ឋានបច្ចុប្បន្ន <b><?=$Address?></b> </p>
            <p>លេខទូរសព្ទ <b><?=$Phone?></b> អ៊ីម៉ែល <b><?=$Email?></b></p>
            <h5>២.ស្ថានភាពគ្រួសារ</h5>
            <p>ឪពុកឈ្មោះ <b><?=$Dad_Name?></b> មុខរបរ <b><?=$Dad_Job?></b></p>
            <p>ម្តាយឈ្មោះ <b><?=$Mom_Name?></b> មុខរបរ <b><?=$Mom_Job?></b></p>
            <p>ប្រពន្ធ/ប្តីឈ្មោះ <b><?=$Spouse_Name?></b> មុខរបរ <b><?=$Spouse_Job?></b></p>
            <p>អាសយដ្ឋានបច្ចុប្បន្ន <b><?=$Fam_Address?></b></p>
            <p>លេខទូរសព្ទ <b><?=$Fam_Phone?></b> អ៊ីម៉ែល <b><?=$Fam_Email?></b></p>
            <p>អ្នកដែលអាចទាក់ទងបានករណីចាំបាច់ ឈ្មោះ <b><?=$Contact_Name?></b> ត្រូវជា <b><?=$Relation?></b></p>
            <p>លេខទូរសព្ទ <b><?=$Contact_Phone?></b> អ៊ីម៉ែល <b><?=$Contact_Email?></b></p>
            <h5>៣.ព័ត៌មានពាក់ព័ន្ធនឹងសមាគម</h5>
            <p>-តើអ្នកស្គាល់សមាគមសាលាត្រាជូតាមរយៈអ្វី?</p>
            <p><b><?=$How?></b></p>
            <p>-ហេតុអ្វីបានជាអ្នកចាប់អារម្មណ៍ចូលជាសមាជិកសមាគមសាលាត្រាជូ??</p>
            <p><b><?=$Why?></b></p>
            <p>-តើអ្នកធ្លាប់ទទួលបានព័ត៌មានអ្វីខ្លះពីសមាគមសាលាត្រាជូ??</p>
            <p><b><?=$Use?></b></p>
            <p>-តើអ្នករំពឹងថានឹងទទួលបានអ្វីខ្លះពីសមាគមសាលាត្រាជូ??</p>
            <p><b><?=$Think?></b></p>
            <p style="text-indent:10%">ខ្ញុំសូមធានាអះអាងថា ប្រវត្តិរូបរបស់ខ្ញុំ និងព័ត៌មានខាងលើ គឺពិតជាត្រឹមត្រូវប្រាកដមែន ក្នុងករណី​ខុស​​ពី​ការពិត ខ្ញុំសូមទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។</p>
            <p>សូម លោកប្រធានសមាគមសាលាត្រាជូ មេត្តាអនុញ្ញាតឱ្យរូបខ្ញុំបាទ/នាងខ្ញុំ ចូលជា៖ <b><?=$status?></b> របស់សមាគមសាលាត្រាជូ ដោយក្តីអនុគ្រោះបំផុត។</p>
            <p>ខ្ញុំបាទ/នាងខ្ញុំ សូមសន្យាថា គោរព និងអនុវត្តតាមលក្ខន្តិកៈ និងបទបញ្ជាផ្ទៃ​ក្នុងរបស់សមាគម​សាលា​ត្រាជូ។</p>
            <div style="width:50%;float:right;">
                <h5 style="text-align: right;font-family: Kh_content;">រាជធានីភ្នំពេញ ថ្ងៃទី <?=$day?> ខែ <?=$m?> ឆ្នាំ <?=$year?></h5>
                <h5 style="text-align: center;font-family: Kh_MuolLight;">ហត្ថលេខាសាមីខ្លួន</h5>
            </div>
            <div style="clear:both"></div><br><br>
            <p style="text-align: left;font-family: Kh_content;font-size:10px">ឯកសារភ្ជាប់៖</p>
            <p style="text-align: left;font-family: Kh_content;font-size:10px">-ជីវប្រវត្តិសង្ខេប (CV)  ចំនួន ១ ច្បាប់</p>
            <p style="text-align: left;font-family: Kh_content;font-size:10px">-អត្តសញ្ញាណបណ្ណថតចម្លង ចំនួន ១ ច្បាប់</p>
            
        </div>
    </body>
</html>