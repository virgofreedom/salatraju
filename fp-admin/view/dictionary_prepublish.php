
<?php
if(isset($_GET['delid'])){
    $data = array(
        'Id'=>$_GET['delid']
    );
    db_delete('dictionnary',$data);
    header("Location: ".THIS_PAGE);
}elseif (isset($_GET['published'])){
    $cond = array("Id"=>$_GET['published']);
    $val = array("Status"=>1);
    db_update('dictionnary',$val,$cond);
}
?>
<div class="small-12 columns big-menu">

<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
<tr>
<th>Id</th><th>Word</th><th>Definition</th><th>Other</th>
</tr>
<?php
$res = db_get('dictionnary','WHERE Status = "0"');
for($i=0;$i<count($res);$i++){
    
    $definition = htmlspecialchars_decode($res[$i]['Description'],ENT_QUOTES);
echo '
<tr>
<td>'.$res[$i]['Id'].'</td><td>'.$res[$i]['Word'].'</td><td>'.$definition.'</td><td>
<a href="?published='.$res[$i]['Id'].'">Publish</a> |';
if ($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
echo '
<a href="#" onclick="msgbox('."'Do you want to delete this word?','?delid=".$res[$i]['Id']."','_self','yesno'".')" >Delete</a>';
}
echo '
</td>
</tr>
';
}
?>
</table>
</div>