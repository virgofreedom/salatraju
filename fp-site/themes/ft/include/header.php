<!doctype html>
<html lang="en">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-R89ZM4Z2KW"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-R89ZM4Z2KW');
</script>
<!--End Global site tag (gtag.js) - Google Analytics -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?=DOMAIN?>fp-admin/img/logo.png">
    <?php
    
    $titile = "សាលាត្រាជូ";
    $content ="";
  //  $content = "សមាគមសាលាត្រាជូមានសេចក្ដីរីករាយសូមនាំមកជូននូវគេហទំព័រថ្មីដែលផ្ទុកទៅដោយឯកសារជំនួយស្មារតី អត្ថបទច្បាប់ បទវិចារណកថា វីដេអូអប់រំច្បាប់ បទបង្ហាញច្បាប់ ព្រមទាំងព័ត៌មានជាច្រើនដទៃទៀតដែលមានសារៈសំខាន់ដល់សាធារណជនទូទៅ ក៏ដូចជាអ្នកសិក្សាស្វែងយល់អំពីច្បាប់ និងវិស័យដែលពាក់ព័ន្ធ។ ដោយឡែក អត្ថបទ និងឯកសារជំនួយស្មារតីត្រូវបាន និងកំពុងរៀបចំទៅតាមរចនាបទថ្មីប្រកបដោយលក្ខណៈវិទ្យាសាស្ត្រ និងស្តង់ដា។";
    $str_cont = '';
    $id = 0;
    if (key($_GET) != "" && key($_GET) != "login" && key($_GET) != "under_construction" ){
      
      //Get data from database
      if(isset($_GET['post'])){
        $id = $_GET['post'];
        $cond = array(
        'PostId'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
      }
      if(isset($_GET['page'])){
        $id = $_GET['page'];
        $cond = array(
        'PostTitle'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
      }
      if(isset($_GET['mem'])){
        $id = $_GET['mem'];
        $cond = array(
        'PostId'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
        
      }
      $url = DOMAIN.key($_GET).'/'.$id;
      $page_exception = array("lexique","member","annotation","member_add","about_lexique","gallery","more","legal_index","legal_update");
      if (in_array(key($_GET),$page_exception)){
          $id = "";
          if (VIEW_PAGE == "lexique"){
              $titile = "វាក្យស័ព្ទច្បាប់";
              $content ='«វាក្យស័ព្ទច្បាប់អេឡិចត្រូនិក» ​ឬ «Online Legal Terminology» គឺជាគម្រោងថ្មីមួយរបស់សមាគមសាលាត្រាជូ​ ដែលត្រូវបានរចនាឡើង ក្នុងគោលបំណងផ្ដល់ភាពងាយស្រួលក្នុងការស្វែងរកនិយមន័យនៃ​ពាក្យបច្ចេកទេសច្បាប់នីមួយៗ ព្រមទាំងដើម្បីលើកកម្ពស់ការសិក្សា​ស្រាវជ្រាវច្បាប់នៅក្នុងប្រទេសកម្ពុជា។';            
              if (isset($_GET['word'])){
                $word = $_GET['word'];
                $url = DOMAIN . "lexique.php?word=$word";
              }else{
                $url = DOMAIN . "lexique";
              }
          }else{
            $titile=VIEW_PAGE;
            $content='';  
          }
          
      }else{
            
          for($i=0;$i<count($res);$i++){
            if (key($_GET) != "trajulaw"){
              if ($res[$i]['PostTitle'] != ""){
                $titile = $res[$i]['PostTitle'];
              }else{
                $titile=key($_GET);
              }
          
            $str_cont = htmlspecialchars_decode($res[$i]['PostContent'],ENT_QUOTES);
            }else{
              $titile=key($_GET);
            }
            $sub_content = substr($str_cont,stripos($str_cont,'<p'),stripos($str_cont,'</p>') + 4);
            #find if there is image inside the paragraph or not.
            if(stripos($sub_content,"<img")!=""){
                $len_img = stripos($sub_content,"/>") - stripos($sub_content,"<img");
                $img = substr($sub_content,strrpos($sub_content,"<img"),$len_img);#. 'class="post-thumb w3-border w3-padding" />';
                $len_code = strripos($img,".") - strripos($img,"/");
                $code_img = substr($img,strripos($img,"/") + 1,$len_code+3);
            }else{#make the default image
                $code_img = '62a6153d.jpg';
            }
            $fb_img = "https://salatraju.com/fp-admin/ckeditor/plugins/imageuploader/uploads/thumbnail/thumb_".$code_img;
          if (stripos($str_cont,'img') == '' ){
            $content = substr($str_cont,stripos($str_cont,'<p>'),stripos($str_cont,'</p>'));
          }else{
            $s = substr($str_cont,stripos($str_cont,'</p>') + 4);
            $subb_content = substr($s,stripos($s,'<p>'),stripos( $s,'</p>'));
            $content = substr($subb_content,stripos($subb_content,'<p>') + 3,stripos($subb_content,'</p>')-3);
          }
          
        }
        
        
      }

    
    ?>
    <meta property="description" content="<?=$content?>"/>
    <!--Facebook meta -->
    
    <meta property="fb:app_id" content="960541120783899" />
    <meta property="og:site_name" content="Salatraju"/>
    <meta property="og:url" content="<?=$url?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?=$titile?>"/>
    <meta property="og:image" content="<?=$fb_img?>"/>
    <meta property="og:description" content="<?=$content?>"/>
    <!-- Facebook SDK -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&autoLogAppEvents=1&version=v3.1&appId=960541120783899';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <?php
    }else{
      ?>
      <meta property="description"        content="សមាគមសាលាត្រាជូមានសេចក្ដីរីករាយសូមនាំមកជូននូវគេហទំព័រថ្មីដែលផ្ទុកទៅដោយឯកសារជំនួយស្មារតី អត្ថបទច្បាប់ បទវិចារណកថា វីដេអូអប់រំច្បាប់ បទបង្ហាញច្បាប់ ព្រមទាំងព័ត៌មានជាច្រើនដទៃទៀតដែលមានសារៈសំខាន់ដល់សាធារណជនទូទៅ ក៏ដូចជាអ្នកសិក្សាស្វែងយល់អំពីច្បាប់ និងវិស័យដែលពាក់ព័ន្ធ។ ដោយឡែក អត្ថបទ និងឯកសារជំនួយស្មារតីត្រូវបាន និងកំពុងរៀបចំទៅតាមរចនាបទថ្មីប្រកបដោយលក្ខណៈវិទ្យាសាស្ត្រ និងស្តង់ដា។" />
      <!--Facebook meta -->

      <meta property="og:url"                content="http://salatraju.com/" />
      <meta property="og:type"               content="article" />
      <meta property="og:title"              content="សាលាត្រាជូ"/>
      <meta property="og:description"        content="សមាគមសាលាត្រាជូមានសេចក្ដីរីករាយសូមនាំមកជូននូវគេហទំព័រថ្មីដែលផ្ទុកទៅដោយឯកសារជំនួយស្មារតី អត្ថបទច្បាប់ បទវិចារណកថា វីដេអូអប់រំច្បាប់ បទបង្ហាញច្បាប់ ព្រមទាំងព័ត៌មានជាច្រើនដទៃទៀតដែលមានសារៈសំខាន់ដល់សាធារណជនទូទៅ ក៏ដូចជាអ្នកសិក្សាស្វែងយល់អំពីច្បាប់ និងវិស័យដែលពាក់ព័ន្ធ។ ដោយឡែក អត្ថបទ និងឯកសារជំនួយស្មារតីត្រូវបាន និងកំពុងរៀបចំទៅតាមរចនាបទថ្មីប្រកបដោយលក្ខណៈវិទ្យាសាស្ត្រ និងស្តង់ដា។" />
      <meta property="og:image"              content="<?=$fb_img?>">
    <?php
    }
    ?>
    <!--End Facebook meta -->
    <title><?=$titile?></title>
    <script src="<?=VIRTUAL_PATH_SITE?>themes/ft/js/w3.js"></script>
    <script src="<?=VIRTUAL_PATH_SITE?>js/virgo.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/ft/khmerfonts/font.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/ft/css/foundation.min.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/ft/css/w3.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/ft/css/virgo.css">
    <script src="<?=VIRTUAL_PATH_SITE?>themes/ft/js/jquery.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function () {
        //Disable full page
        
        // $("body").on("contextmenu",function(e){
        //     return false;
        // });
        
        // $('body').bind('cut copy paste', function (e) {
        // e.preventDefault();
        // });
        
        //Disable part of page
        /*
        $("#annotaion").on("contextmenu",function(e){
            return false;
        });
        $('#annotaion').bind('cut copy paste', function (e) {
        e.preventDefault();
        });
        */
    });
    </script>
   
</head>
<body class="kh-content">

    <!-- Header -->
    <header class="banner w3-bottombar border-traju">
        
    
    <!-- Sidebar -->
    <div class="w3-sidebar w3-bar-block w3-col s12 m3 l3" id="sidebar" style="background-color:#f3e7d1;display:none">
        <button class="w3-bar-item w3-button" onclick="w3_close('sidebar')"><img src="<?=VIRTUAL_PATH?>img/logo.png" style="width:50px" alt="Logo"> &times;</button> 
        <!-- Search -->
        <button class="w3-bar-item w3-button w3-large " onclick="w3_open('search')">
                    <i class="fa fa-search" aria-hidden="true"></i></button>
        <div style="background-color:#f3e7d1;display:none" class="w3-padding" id="search">
                <form action="<?=DOMAIN?>search" class="w3-container" method="post"> 
                    <input type="text" name="word" class="w3-input" placeholder="Search key word">
                    <button type="submit" class="w3-btn w3-blue-grey">ស្វែងរក</button>
                    <button type="button" class="w3-btn w3-blue-grey" onclick="w3_close('search')">&times;</button>
                </form>
        </div>
        <!-- End Search-->
        <div class="w3-bottombar border-traju">
        <?php 
            $limit = "";
            include INCLUDE_PATH_SITE.'menu_accor.php'; 
        ?>
        </div>
        <?php
            sidebar_right('w3-bottombar border-traju','');
        ?>
        <button class="w3-bar-item w3-button" onclick="w3_close('sidebar')"><img src="<?=VIRTUAL_PATH?>img/logo.png" style="width:50px" alt="Logo"> &times;</button> 
    </div>
    <!-- End Sidebar -->
    <!-- Title -->
    <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2" >
        
        
        <div class="row">
            <div class="small-3 columns">
                <button class="w3-button w3-large kh-content traju-text-color" onclick="w3_open('sidebar')">
                    
                    <i class="fa fa-bars fa-2x show-for-small-only" aria-hidden="true"></i>
                    <b class="hide-for-small-only">មាតិកា</b>
                </button>
                    
            </div>
            <div class="small-6 columns w3-center">
                <a href="<?=DOMAIN?>"><img src="<?=VIRTUAL_PATH_SITE?>themes/ft/img/banner.png" alt="letter" style="width:400px"/></a>
            </div>
            <div class="small-3 columns w3-right-align traju-text-color w3-padding-24">
                
                <a href="<?=DOMAIN?>fp-admin/index.php/login.php"><i class="fa fa-user fa-2x show-for-small-only" aria-hidden="true"></i></a>
                    <!--a class="hide-for-small-only" href="<=DOMAIN?>subscribe_login"><h5>គណនីខ្ញុំ</h5></a-->
                
                

            </div>
        </div>
    </div>
    <!-- End Title -->
    
        <!-- Navigation -->
        
        <!-- End Navigation -->
    </header>
    <div class="small-12 medium-10 large-8 medium-offset-1 large-offset-2" style="min-height: 80%;margin-bottom:-80px;">
        <!-- Signin -->
        <!-- div class="row">
            <div class="small-6 hide-for-medium-up w3-right-align w3-border-right columns">
                <a href="#">Signin</a>
            </div>
            <div class="small-6 hide-for-medium-up w3-right-left columns">
                <a href="#">Subscribe</a>
            </div>
        </div -->
        <!-- End signin -->