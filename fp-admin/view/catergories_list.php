<?php
include PHYSICAL_PATH.'library/admin.php';
$result = db_get('catergories');
$count = count($result);
?>
<div class="small-12 columns big-menu">
<form action="<?=THIS_PAGE?>" method="POST">
<button class="button" type="button" data-toggle="in_body">Add New</button>
    <div class="small-10 dropdown-pane" id="in_body" data-dropdown data-auto-focus="true">
        <div class="row">
            <div class="medium-10 columns">
                <label>Name
                    <input name="Name" type="text" required>
                </label>
                <label>English Name
                    <input name="EnName" type="text">
                </label>
            </div>
        </div>
        <div class="row">
            <div class="medium-10 columns">
                <label>Description :
                    <textarea name="Description" id="" cols="30" rows="10"></textarea>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="medium-10 columns ">
                    <input type="submit" class="button button-green right" name="Save" value="Save">
            </div>
        </div>
</form>
</div>
<?php
if (isset($_POST['Save'])){
    $data = array(
        'Name'=> $_POST['Name'],
        'EnName'=> $_POST['EnName'],
        'Description'=>$_POST['Description']
    );
    db_insert('catergories',$data);
    header('location: '.THIS_PAGE);
}else if (isset($_POST['Update'])){
    $val = array(
        'Name'=> $_POST['Name'],
        'EnName'=> $_POST['EnName'],
        'Description'=>$_POST['Description']
    );
    $cond = array(
        'Id'=> $_POST['id']
    );
    db_update('catergories',$val,$cond);
    header('location: '.THIS_PAGE);
}else if(isset($_GET['delid'])){
    $data = array(
        'Id'=>$_GET['delid']
    );
    db_delete('catergories',$data);
    header("Location: ".THIS_PAGE);

}else if(isset($_GET['id']) && $_GET['id']!= ""){
    $res = db_get('catergories','Where Id="'.$_GET['id'].'"');
    for($i=0;$i<count($res);$i++){
        $name = $res[$i]['Name'];
        $enname = $res[$i]['EnName'];
        $Des =  $res[$i]['Description'];
    }
    echo '
    <form action="'.THIS_PAGE.'" method="POST">
    <input name="id" type="hidden" value="'.$_GET['id'].'" required>
        <div class="row">
        
            <div class="medium-10 columns">
                <label>Name
                    <input name="Name" type="text" value="'.$name.'" required>
                </label>
                    <label>English Name
                    <input name="EnName" type="text" value="'.$enname.'" required>
                </label>
            </div>
            
            
            
        </div>
        <div class="row">
            <div class="medium-10 columns">
                <label>Description :
                    <textarea name="Description" id="" cols="30" rows="10">'.$Des.'</textarea>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="medium-10 columns ">
                    <input type="submit" class="button button-green right" name="Update" value="Update">
            </div>
        </div>
</form>
    ';
}
?>
<!-- List from here -->
<div class="small-12 columns big-menu">
<h3>CATERGORIES LIST</h3>
<div class="table-scroll">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr >
        <th class="small-6 center">Name</th><th class="small-5 center">Description</th><th class="center">Posts</th>
        </tr>
        <?php
        
        for($i=0;$i<$count;$i++)
        {
 
        echo'
        <tr class="small">
        <td>'.$result[$i]['Name'].'
        <a href="'.THIS_PAGE.'?id='.$result[$i]['Id'].'" >Edit </a> |
        <a href="#" onclick="msgbox('."'Do you want to delete this catergory?','?delid=".$result[$i]['Id']."','_self','yesno'".')" >Delete</a> |
        <a href="'.ADMIN_NAV_PATH.'sidebar_add.php?ct='.$result[$i]['Id'].'" >Add to Sidebar </a> |
        </td><td>'.$result[$i]['Description'].'</td><td></td>
        
        </tr>
        ';
        }
        ?>
  </table>
</div>
</div>