<?php
function db2table($table,$caption="",$edit="no"){
    $res = db_get($table);
    $cols = array();
if (count($res)>0){#table must have at least 1 entry
    $index_key = array_keys($res[0]);
    for ($i=0;$i<count($index_key);$i++){
        if (is_string($index_key[$i]) == 1){
            array_push($cols,$index_key[$i]);
        }
    }
}
$colspan = count($cols);
?>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
<?php
    if($caption != ""){
?>
    <tr><th colspan="<?=$colspan?>"><?=$caption?></th>
    </tr>
<?php
    }
?>
    <tr>
<?php
    for ($y=0;$y<count($cols);$y++){
        echo '<th>'.$cols[$y].'</th>';
    }
?>
    </tr>
    </thead>
    <tbody>
<?php
    for ($y=0;$y<count($res);$y++){
        echo '
        <tr>';
        for($i=0;$i<count($cols);$i++){
            echo '<td>'.$res[$y][$cols[$i]].'</td>';
        }
        if ($edit == "yes"){
        echo '<td>';
        echo '
        <a href="'.THIS_PAGE.'?id='.$res[$y]['Id'].'" class="small">Edit </a> |
        <a href="#" onclick="msgbox('."'Do you want to delete this post?','".THIS_PAGE."?delid=".$res[$y]['Id']."','_self','yesno'".')" class="small">Delete</a>
        ';
        echo '</td>';
        }
        echo'</tr>
        ';
    }
?>  
    </tbody>
</table>
<?php
}#End db2table();
function count_by_field($table,$field,$order=""){
    $res_field = db_get($table,"","Group by $field",$order);
    $res = db_get($table);
    $cols = array();
if (count($res)>0){#table must have at least 1 entry
    $index_key = array_keys($res[0]);
    for ($i=0;$i<count($index_key);$i++){
        if (is_string($index_key[$i]) == 1){
            array_push($cols,$index_key[$i]);
        }
    }
}
$colspan = count($cols);
$count = count($res);
?>
<table>
    <thead>
<?php
    
?>
    <tr><th colspan="<?=$colspan?>">Total of <?=$field?> is <?=$count?></th></tr>
<?php

?>
    <tr>
<?php
    for ($y=0;$y<count($cols);$y++){
        echo '<th>'.$cols[$y].'</th>';
    }
?>
    </tr>
    </thead>
    <tbody>
<?php
    for ($y=0;$y<count($res_field);$y++){
        echo '
        <tr>';
            for($i=0;$i<count($cols);$i++){
                echo '<td>'.$res[$y][$cols[$i]].'</td>';
            }
        echo'</tr>
        ';
    }
?>  

    </tbody>
</table>
<?php
}#End count_by_field
?>