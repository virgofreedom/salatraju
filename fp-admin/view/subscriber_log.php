<?php 
if (isset($_GET['user'])){
    $res = db_get('subscriber_log','Where SubscriberId="'.$_GET['user'].'"','','ORDER BY DateTime DESC');
?>
<div class="small-12 columns big-menu">
<h3>subscribers' Activity log</h3>

<div class="table-scroll">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr >
        <th class="small-1 center">Id</th><th class="center">Name</th><th class="center">Titile / Word</th><th>Date Time</th>
        
        </tr>
        <?php
        if (count($res)==0){
            echo '
            <tr><td colspan="10" class="center">There are no record found!</td></tr>
            ';
        }
        for($i=0;$i<count($res);$i++)
        {
            #ViewType == 1 == Post and #ViewType == 2 == Lexique
            $res_sub = db_get('subscriber',"Where Id='".$res[$i]["SubscriberId"]."'");
            if ($res[$i]['ViewType'] == 1){
                $res_title = db_get('fp_posts',"Where PostId='".$res[$i]['ViewId']."'");
                $title = $res_title[0]['PostTitle'];
            }else if ($res[$i]['ViewType'] == 2){
                $res_title = db_get('dictionnary',"Where Id='".$res[$i]['ViewId']."'");
                $title = $res_title[0]['Word'];
            }else{
                $title = "Unknow";
            }
            echo '<tr>
            <td>'.$res[$i]['Id'].'</td>
            <td>'.$res_sub[0]['LastName'].' '.$res_sub[0]['FirstName'].'</td>
            <td>'.$title.'</td>
            <td>'.format_date($res[$i]['DateTime'],"d-m-Y H:i:s").'</td>
            </tr>';
        }
        ?>
  </table>
</div>
<?php
}else{
    #get all the data here form database
    $request_url = THIS_PAGE;
    $res_total = db_get('subscriber_log');
    $res = Pager_db("subscriber_log","","","ORDER BY DateTime DESC",100,"pages");
?>
<div class="small-12 columns">
<?php
		Pager_Nav_Admin($res_total,5,'pages',$request_url);
?>
</div>
<!-- List from here -->
<div class="small-12 columns big-menu">
<h3>subscribers' Activity log</h3>

<div class="table-scroll">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr >
        <th class="small-1 center">Id</th><th class="center">Name</th><th class="center">Titile / Word</th><th>Date Time</th>
        
        </tr>
        <?php
        if (count($res)==0){
            echo '
            <tr><td colspan="10" class="center">There are no record found!</td></tr>
            ';
        }
        for($i=0;$i<count($res);$i++)
        {
            #ViewType == 1 == Post and #ViewType == 2 == Lexique
            $res_sub = db_get('subscriber',"Where Id='".$res[$i]["SubscriberId"]."'");
            if ($res[$i]['ViewType'] == 1){
                $res_title = db_get('fp_posts',"Where PostId='".$res[$i]['ViewId']."'");
                $title = $res_title[0]['PostTitle'];
            }else if ($res[$i]['ViewType'] == 2){
                $res_title = db_get('dictionnary',"Where Id='".$res[$i]['ViewId']."'");
                $title = $res_title[0]['Word'];
            }else{
                $title = "Unknow";
            }
            echo '<tr>
            <td>'.$res[$i]['Id'].'</td>';
            if (count($res_sub)>0){
                echo '<td>'.$res_sub[0]['LastName'].' '.$res_sub[0]['FirstName'].'</td>';
            }else{
                echo '<td></td>';
            }
            
            
            echo '<td>'.$title.'</td>
            <td>'.format_date($res[$i]['DateTime'],"d-m-Y H:i:s").'</td>
            </tr>';
        }
        ?>
  </table>
</div>
<div class="small-12 columns">
<?php
		Pager_Nav_Admin($res_total,5,'pages',$request_url);
?>
</div>
<?php
}
?>