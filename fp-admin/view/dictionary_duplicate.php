<?php
if(isset($_GET['delid'])){
    $data = array(
        'Id'=>$_GET['delid']
    );
    db_delete('dictionnary',$data);
    header("Location: ".THIS_PAGE);
}
?>
<div class="small-12 columns big-menu">
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
<tr>
<th>Id</th><th>Word</th><th>Definition</th><th>Other</th>
</tr>

<?php
$res = db_get('dictionnary');
$duplicate = array();
for($i=0;$i<count($res);$i++){
    $duplicate_data = db_get('dictionnary','WHERE Word = "'.$res[$i]['Word'].'"','','ORDER BY Word');
    if(count($duplicate_data)>1){
        for($y=0;$y<count($duplicate_data);$y++){
            array_push($duplicate,array(
                "Id"=>$duplicate_data[$y]["Id"],
                "Word"=>$duplicate_data[$y]["Word"],
                "Description"=>$duplicate_data[$y]['Description']
            ));
        }
            
        
    }
    
}

for($y=0;$y<count($duplicate);$y++){
    $definition = htmlspecialchars_decode($duplicate[$y]['Description'],ENT_QUOTES);
    echo '
    <tr>
    <td>'.$duplicate[$y]['Id'].'</td><td>'.$duplicate[$y]['Word'].'</td><td>'.$definition.'</td><td>';
    if ($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
        echo '
        <a href="#" onclick="msgbox('."'Do you want to delete this word?','?delid=".$duplicate[$y]['Id']."','_self','yesno'".')" >Delete</a>';
        }
echo '
</td>
</tr>
';
}
?>
</table>
</div>