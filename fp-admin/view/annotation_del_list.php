<div class="small-12 columns big-menu">
<?php
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    $kinthi = db_get("kinthi","Where Status=-2","","","",DB_NAME2);
    $methika = db_get("metika","Where Status=-2","","","",DB_NAME2);
    $chapter = db_get("chapter","Where Status=-2","","","",DB_NAME2);
    $section = db_get("section","Where Status=-2","","","",DB_NAME2);
    $kathapheak = db_get("kathapheak","Where Status=-2","","","",DB_NAME2);
    if(isset($_GET['rest'])){
        //Do the chage the Status to 1
        $cond = array($_GET['step']."Id"=>$_GET['rest']);
        $data = array(
        "Status"=> 1
        );
        db_update($_GET['step'],$data,$cond,DB_NAME2); 
        msgbox('ពាក្យបានដាក់បញ្ចូលទៅក្នុងបញ្ជីវិញហើយ។',THIS_PAGE);
    }
    if(isset($_GET['perm'])){
        $cond = array($_GET['step']."Id"=>$_GET['perm']);
        db_delete($_GET['step'],$cond,DB_NAME2);
        msgbox('ពាក្យនេះបានលុបចេញពី database រួចរាក់ហើយ។',THIS_PAGE);
    }
    if(isset($_GET['step'])){

        if($_GET['step'] == "kinthi"){
            $title = "គន្ថី";
        }elseif($_GET['step'] == "metika"){
            $title = "មាតិកា";
        }elseif($_GET['step'] == "chapter"){
            $title = "ជំពូក";
        }elseif($_GET['step'] == "section"){
            $title = "ផ្នែក";
        }elseif($_GET['step'] == "kathapheak"){
            $title = "កថាភាគ";
        }
?>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
            <th>Id</th><th>ចំណងជើង<?=$title?></th><th></th>
        </tr>
    </thead>
    <tbody>
<?php
        $res = db_get($_GET['step'],"Where Status=-2","","","",DB_NAME2);
        for($i=0;$i<count($res);$i++){
            echo'
            <tr>
                <td>'.$res[$i][$_GET['step'].'Id'].'</td><td>'.$res[$i][$_GET['step'].'Title'].'</td><td><a href="?step='.$_GET['step'].'&rest='.$res[$i][$_GET['step'].'Id'].'">Restore</a> 
                | <a onclick="msgbox('."'តើពិតជាចង់លុបវាមែន?បើលុបហើយមិនអាចយកមកវិញបានទេ!','".THIS_PAGE."?step=".$_GET['step']."&perm=".$res[$i][$_GET['step'].'Id']."','_self','yesno'".');" href="#">Permanantly delete</a></td>
            </tr>
            ';
        }
?>
    </tbody>
</table>
<?php
    }else{
?>

<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
            <th>ផ្នែគ</th><th>ចំនួន</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="?step=kinthi">គន្ថី</a></td><td><?php echo count($kinthi);?></td>
        </tr>
        <tr>
            <td><a href="?step=metika">មាតិកា</a></td><td><?php echo count($methika);?></td>
        </tr>
        <tr>
            <td><a href="?step=chapter">ជំពូក</a></td><td><?php echo count($chapter);?></td>
        </tr>
        <tr>
            <td><a href="?step=section">ផ្នែក</a></td><td><?php echo count($section);?></td>
        </tr>
        <tr>
            <td><a href="?step=kathapheak">កថាភាគ</a></td><td><?php echo count($kathapheak);?></td>
        </tr>
    </tbody>
</table>
<?php
    }
}

?>
</div>