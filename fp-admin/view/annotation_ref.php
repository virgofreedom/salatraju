<div class="small-12 columns big-menu">
<?php
if(isset($_GET['edit_id'])){
    $edit_id = $_GET['edit_id'];
    $ref = db_get("reference","WHERE referenceId='$edit_id'","","","",DB_NAME2);
    
    $kh_id = $ref[0]['referenceIdkh'];
    $ref_title = $ref[0]['referenceTitle'];
    $ref_date = $ref[0]['referenceDate'];
    $ref_des = $ref[0]['referenceDescription'];
?>
<form action="annotation.php" method="POST">
    <input type="text" name="krom_title" value="<?=$_GET['krom_title']?>" readonly hidden/>
    <input type="text" name="ref_id" value="<?=$edit_id?>" readonly hidden/>
    <div id="reference">
        <div class="small-4 columns">
            <label for="title">លិខិតលេខ
                <input type="text" name="ref_idkh" value="<?=$kh_id?>"/>
            </label>
        </div>
        <div class="small-4 columns">
            <label for="title">ចំណងជើង
                <input type="text" name="ref_title" value="<?=$ref_title?>"/>
            </label>
        </div>
        <div class="small-4 columns">
            <label for="title">ថ្ងៃខែឆ្នាំ
                <input type="date" name="ref_date" value="<?=$ref_date?>"/>
            </label>
        </div>
        <div class="small-12">
            <label for="title">ការពិពណ៌នា(Description)
                <textarea name="ref_desc" cols="30" rows="3"><?=trim($ref_des);?></textarea>
            </label>
        </div>
        
                <div class="small-12">
                    <input type="submit" class="button right" name="reference" value="Save"/>  
                </div>  
        
    </div>
</form>
<?php
}elseif(isset($_GET['del_id'])){
    $cond = array("referenceId"=>$_GET['del_id']);
    db_delete("reference",$cond,DB_NAME2);
?>
    <form action="annotation.php" method="POST" id="myForm">
        <input type="text" name="krom_title" value="<?=$_GET['krom_title']?>" readonly/>
        
    </form>
    
    <script type="text/javascript">
        window.onload=function(){
            document.getElementById("myForm").submit();
        }
    </script>
    
<?php
}
?>
</div>