<?php
$permission = array("Admin","Superuser","Editor");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if (isset($_POST['submit'])){
    $val = array("Title"=>$_POST['Title'],
                "Url"=>$_POST['Url'],
                "Date"=>date("Y-m-d"));
    db_insert('videos',$val);
    echo '
    <div class="small-12 columns big-menu">
    <h4>
    Videos saved!
    </h4>
    <a class="button" href="'.THIS_PAGE.'">Add more</a>
    </div>
    ';
die;
}
?>
<div class="small-12 columns big-menu">
    <h4>Video from youtube only</h4>
    <form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
        <div class="small-12">
            <label for="title">Video's Title*
                <input type="text" name="Title" placeholder="Please enter title here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">Youtube's url*
                <input type="text" name="Url" placeholder="Example: https://youtu.be/cu4aA9OfwS4" onkeyup="youtube_preview('ypreview',this.value)" required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <input class="button" type="submit" name="submit" value="Save">
        </div>
    </div>
    </form>
    <h4>Youtube preview</h4>
    <iframe width="560" height="315" frameborder="0" allowfullscreen id="ypreview"></iframe>
    
</div>
<?php
}#end permission
?>