<?php
include '../fp-admin/config/config.php';
if(SESSION == true){
    session_start();//session start
}
header("Content-Type: application/json; charset=UTF-8");
$myJSON = new \stdClass();
$email = $_POST['email'];
$password = $_POST['password'];
$login = array();
// Here convert from array into json object
$data = array("Email"=>$email);
$result = db_get_where('subscribers',$data);
if (count($result) > 0){
    for ($i=0;$i<count($result);$i++){
        if (crypt($password,KEY_ENCRYPT)  == $result[$i]['Password'])
        {
            // Send result out
            array_push($login,array("email"=>$email,"status"=>1));
            $_SESSION['users_pass'] = $result[$i]['Password'];
        }else{
            array_push($login,array("email"=>0,"status"=>0));        
        }
    }
    
}else{
    array_push($login,array("email"=>0,"status"=>0));
}

$myJSON = json_encode(array("krom"=>$login), JSON_PRETTY_PRINT);
echo $myJSON;
?>