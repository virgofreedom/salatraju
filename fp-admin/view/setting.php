<?php

$path = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpg";
$path1 = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".png";
$path2 = PHYSICAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpeg";

if (file_exists($path)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpg";
}elseif (file_exists($path1)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".png";
}elseif (file_exists($path2)) {
    $img_src = VIRTUAL_PATH ."img/users/".$_SESSION['infos']['id'].".jpeg";
}else{
    $img_src = VIRTUAL_PATH."img/picID.jpg";
}
?>
<div class="small-12 columns big-menu">
    <div class="row">
        <div class="small-6 columns left">
            <img src="<?=$img_src?>" class=" w3-margin-right" > <br><span>ដើម្បីឲ្យរូបឃើញមកស្អាត​ សូមជ្រើសរើសរូបដែលមានទំហំ 128pixels x 156pixels</span>
        </div>
    </div>
    <form action="<?=THIS_PAGE?>" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="small-4 columns left">
                    <input type="file" name="fileToUpload" id="fileToUpload">
            </div>
            <div class="small-4 columns left">
                    <input class="button" type="submit" Name="imgchange" value="Save">
            </div>
        </div>
    </form>

    <form action="<?=THIS_PAGE?>" method="POST">
        <div class="row">
            <div class="small-4 columns left">
                Last Name : <input type="text" Name="lastname" placeholder="Last Name" value="<?=$_SESSION['infos']['last_name']?>">
            </div>
            <div class="small-4 columns left">
                First Name : <input type="text" Name="firstname" placeholder="First Name" value="<?=$_SESSION['infos']['first_name']?>">
            </div>
            <div class="small-4 columns left">
                <br><input class="button" type="submit" Name="submit" value="Save">
            </div>
        </div>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
        <div class="row">
            <div class="small-8 columns left">
                New Password <input type="password" Name="password" placeholder="Password">
            </div>
            <div class="small-4 columns left">
                <br><input class="button" type="submit" Name="change" value="Change">
            </div>
        </div>
    </form>

<?php
if(isset($_POST['change'])){
    $password = $_POST['password'];
    $encryptpass = crypt($password,KEY_ENCRYPT);
    $cond = array("Id"=>$_SESSION['infos']['id']);
    $data = array("Password"=>$encryptpass,"PlainPassword"=>$password);
    db_update('fp_users',$data,$cond);
    echo '<script>window.open("logout.php","_self");</script>';
}
if (isset($_POST['submit'])){
    $cond = array("Id"=>$_SESSION['infos']['id']);
    $data = array("FirstName"=>$_POST['firstname'],"LastName"=>$_POST['lastname']);
    db_update('fp_users',$data,$cond);
    echo '<script>window.open("logout.php","_self");</script>';
}
if (isset($_POST['imgchange'])){
    $info = pathinfo($_FILES["fileToUpload"]["name"]);
    $ext = $info['extension'];
    if ($ext == "JPG"){
        $ext = "jpg";
    }elseif($ext == "PNG"){
        $ext = 'png';
    }elseif($ext == "JPEG"){
        $ext = "jpeg";
    }
    // check if target path exist or not if not create it
    $target_dir = PHYSICAL_PATH ."img/users/";
    if (!file_exists($target_dir)){
        mkdir ($target_dir);
    }
    // set filename
    $filename = $_SESSION['infos']['id'].".".$ext;
    $target_file = $target_dir . $filename;
    // Check if image file is a actual image or fake image
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        $uploadOk = 1;
    } else {
        $uploadOk = 0;
    }
    // Allow certain file formats
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "JPG" && $imageFileType != "PNG" && $imageFileType != "JPEG") {
        echo "This image's type not allowed";
        $uploadOk = 0;
    }
    if ($uploadOk == 0) {
        echo "Your image has not been uploads it! please try again.";
    
    } else { // if everything is ok, try to upload file
        /////////////create img follow the extension/////////////
        $uploadedfile = $_FILES['fileToUpload']['tmp_name'];
        if($imageFileType=="jpg" || $imageFileType=="jpeg" || $imageFileType == "JPG")
        {
            $src = imagecreatefromjpeg($uploadedfile);
        }
        else if($imageFileType=="png")
        {
            $src = imagecreatefrompng($uploadedfile);
        }
        
        list($width,$height)=getimagesize($uploadedfile);
        ////////////////big img//////////////////
        $newwidth=128;
        $newheight=156;
        // $newheight=($height/$width)*$newwidth;
        // $newheight=($width/$height)*$newwidth;
        
        $tmp=imagecreatetruecolor($newwidth,$newheight);
        imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,
    $width,$height);
        imagejpeg($tmp,$target_file,100);
        imagedestroy($tmp);
        echo '<script>window.open("'.THIS_PAGE.'","_self");</script>';
    }
}
?>
</div>