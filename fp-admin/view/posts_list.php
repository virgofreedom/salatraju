<?php
include PHYSICAL_PATH.'library/admin.php';
$permission = array("Admin","Superuser","Editor","Author");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if(isset($_GET['delid'])){
    $data = array(
        'PostId'=>$_GET['delid']
    );
    db_delete('fp_posts',$data);
    header("Location: ".THIS_PAGE);
}
if(isset($_GET['pubid'])){
    $data = array(
        'PostStatus'=>'2'
    );
    $cond = array(
        'PostId'=>$_GET['pubid']
    );
    db_update('fp_posts',$data,$cond);
    header("Location: ".THIS_PAGE);
}
if(!isset($_POST['submit'])){
    if ($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
        $data = array(
            'PostType'=>'post',
        );
        $cond = "WHERE PostType='post'";
    }else{
        $data = array(
            'PostType'=>'post',
            'PostAuthor'=>$_SESSION['infos']['id'],
        );
        $cond = "WHERE PostType='post' AND PostAuthor='".$_SESSION['infos']['id']."'";
    }
    $result = db_get_where('fp_posts',$data,'ORDER BY PostDate DESC');
    $res_pager = Pager_db('fp_posts',$cond,'','ORDER BY PostDate DESC',50,'pages');//divide into pagers.
    $res_status = db_get_count('fp_posts','PostTitle','PostStatus',$data);
    $count_detail = count($res_status);    
    $count = count($result);
}else{
    
    if ($_SESSION['infos']['role'] == 'Admin'|| $_SESSION['infos']['role'] == 'Superuser'){
        $res_pager = db_get('fp_posts',"Where PostType='post' AND PostTitle LIKE '%".$_POST['title']."%'",'','ORDER BY PostDate DESC');
    }else{
        $res_pager = db_get('fp_posts',"Where PostType='post' AND PostAuthor= '".$_SESSION['infos']['id']."' AND PostTitle LIKE '%".$_POST['title']."%'",'','ORDER BY PostDate DESC');
    }
    $count = count($res_pager);
}



?>
<div class="small-12 columns big-menu">
<h3>POST LIST</h3>
<div class="row">
    <div class="small-12 medium-8 large-8 columns left">
        <form action="<?=THIS_PAGE?>" method="Post">
            <input class="input-group-field" type="text" name="title">
            <input type="submit" class="button" value="Submit" name="submit">
        </form>
    </div>
</div>
<ul class="menu">
  <li class="menu-text">All(<?=$count?>)</li>
  <?php
if (!isset($_POST['submit'])){
  $status = '';
    for($i=0;$i<$count_detail;$i++){
        if ($res_status[$i]['Name'] != 0){
            if($res_status[$i]['Name']==1){
                $status = 'save';
            }else if($res_status[$i]['Name']==2){
                $status = 'publish';
            }else if($res_status[$i]['Name']==3){
                $status = 'editing';
            }
    echo '<li><a href="#">| '.$status.'(
        '.$res_status[$i]['Qty'].'
    )</a></li>';
        }
    }
}else{
    echo '<li class="menu-text"><a href="'.THIS_PAGE.'">Show All</a></li>';
}
    ?>
  
  
</ul>
<div class="table-scroll">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr>
        <th class="small-5">Title</th><th>Author</th><th><i class="fa fa-comment" aria-hidden="true"></i></th><th>Post Status</th><th>Date</th>
        </tr>
        <?php
        
        for($i=0;$i<count($res_pager);$i++)
        {
        if($res_pager[$i]['PostId']!=0){

        
            if($res_pager[$i]['PostStatus'] == 1){
                $status = 'save';
                $quick_pub = '
                | <a href="?pubid='.$res_pager[$i]['PostId'].'" class="small">Publish  </a>
                ';
            }else if($res_pager[$i]['PostStatus']== 2){
                $status = 'publish';
                $quick_pub='';
            }else if($res_pager[$i]['PostStatus']== 3){
                $status = 'editing';
                $quick_pub = '
                | <a href="?pubid='.$res_pager[$i]['PostId'].'" class="small">Publish  </a>
                ';
            }
            //Compare date and get the date
            if ($res_pager[$i]['PostModify']== NULL){
                $date = format_date($res_pager[$i]['PostDate'],'d-m-Y H:i:s');
            }else{
                $date = format_date($res_pager[$i]['PostModify'],'d-m-Y H:i:s');
            }
            //Get the author last and first name from fp_users
            $data = array(
                'Id'=>$res_pager[$i]['PostAuthor']
            );
            $res_author = db_get_where('fp_users',$data);
            for($y=0;$y<count($res_author);$y++){
                $author = $res_author[$y]['LastName'].' '.$res_author[$y]['FirstName'];
                $role_author = $res_author[$y]['Role'];
            }
             
        echo'
        <tr>
        <td >'.$res_pager[$i]['PostTitle'].'<br/>';    
            //if role is admin and article written by super
            if($_SESSION['infos']['role'] == 'Admin' && $role_author == 'Superuser'){
                echo '<a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a>';
            //if role is admin and article is not written by admin
            }else if ($_SESSION['infos']['role'] == 'Admin' && $role_author != 'Admin'){
                echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            //if role is admin and article is written by admin
            }else if ($_SESSION['infos']['role'] == 'Admin' && $role_author == 'Admin'){
                //if article the same writter 
                if ($res_pager[$i]['PostAuthor'] == $_SESSION['infos']['id']){
                    echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
                }else{
                    echo '<a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a>';
                }
            //if role is superuser show all
            }else if ($_SESSION['infos']['role'] == 'Superuser'){
                echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            }else{
                if ($res_pager[$i]['PostAuthor'] == $_SESSION['infos']['id']){
                    echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
                }else{
                    echo '<a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a>';
                }
            }
            
            /*if($role_author == 'Superuser'){
                echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
                | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            //If admin can only edit what he have
            //}else if($role_author == 'Admin' && $res_pager[$i]['PostAuthor'] == $_SESSION['infos']['id'] ){
            //    echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a>
            //    | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            }else if($role_author != 'Admin'){
            //    echo '<a href="posts_add.php?id='.$res_pager[$i]['PostId'].'" class="small">Edit </a>  | <a href="posts_preview.php?id='.$res_pager[$i]['PostId'].'" target="_blank" class="small">View</a> 
            //    | <a href="#" onclick="msgbox('."'Do you want to delete this post?','?delid=".$res_pager[$i]['PostId']."','_self','yesno'".')" class="small">Delete</a>';
            }
            if($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
                echo $res_pager[$i]['PostAuthor'];
                echo '| <a href="#" class="small" onclick="update_time('."'".$res_pager[$i]['PostId']."','posts_list.php'".')">Update time </a>'.$quick_pub;
            }    
            */
        echo '
        </td><td>'.$author.' | '.$role_author.'</td><td></td>
                <td>'.$status.'</td><td>'.$date.'</td>
        </tr>';
        }
        }
        ?>
  </table>
</div>
<div class="row">
<?php
    echo '<div class="small-12 medium-12 large-12">';
    Pager_Nav_Admin($result,50,'pages','posts_list.php');
    echo '</div>';
?>
</div>
</div>
<?php
}
?>