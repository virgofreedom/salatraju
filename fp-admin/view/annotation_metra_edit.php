
<div class="small-12 columns big-menu">
<?php
if (isset($_GET['kromid'])){
    $data=array();
    if ($_GET['kromid'] != "all"){
        $kinthi_data = db_get("kinthi","Where kromId='".$_GET['kromid']."'","","","",DB_NAME2);
        for ($kin = 0;$kin<count($kinthi_data);$kin++){
            $metika_data = db_get("metika","Where kinthiId='".$kinthi_data[$kin]['kinthiId']."'","","","",DB_NAME2);
            for ($met=0;$met<count($metika_data);$met++){
                $chapter_data = db_get("chapter","Where metikaId='".$metika_data[$met]['metikaId']."'","","","",DB_NAME2);
                for($chap=0;$chap<count($chapter_data);$chap++){
                    $section_data = db_get("section","Where chapterId='".$chapter_data[$chap]['chapterId']."'","","","",DB_NAME2);
                    for($sec=0;$sec<count($section_data);$sec++){
                        $kathapheak_data = db_get("kathapheak","Where sectionId='".$section_data[$sec]['sectionId']."'","","","",DB_NAME2);
                        for($kat=0;$kat<count($kathapheak_data);$kat++){
                            $metra_data = db_get("metra","Where kathapheakId='".$kathapheak_data[$kat]['kathapheakId']."'","ORDER BY metraOrder ASC","","",DB_NAME2);
                            for($metr=0;$metr<count($metra_data);$metr++){
                                array_push(
                                    $data,array(
                                        "MetraId"=>$metra_data[$metr]['metraId'],
                                        "MetraTitle"=>$metra_data[$metr]['metraTitle'],
                                        "MetraOrder"=>$metra_data[$metr]['metraOrder'],
                                    )
                                );
                            }

                        }
                    }
                }
            }
        }
    }else{
        $metra_data = db_get("metra","","ORDER BY metraOrder ASC","","",DB_NAME2);
                            for($metr=0;$metr<count($metra_data);$metr++){
                                array_push(
                                    $data,array(
                                        "MetraId"=>$metra_data[$metr]['metraId'],
                                        "MetraTitle"=>$metra_data[$metr]['metraTitle'],
                                        "MetraOrder"=>$metra_data[$metr]['metraOrder'],
                                    )
                                );
                            }
    }
    echo count($data);
?>
<!-- Edit metra -->
<div class="row">
    <fieldset>
        <legend>តារាងមាត្រា</legend>
        <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
            <thead><tr><th>លេខសម្គាល់មាត្រា</th><th>លេខរៀងមាត្រា</th><th>ចំណងជើងមាត្រា</th><th></th></tr></thead>
            <tbody>
                <?php
                asort($data);
                for ($i=0;$i<count($data);$i++){
                ?>
                <tr><td><?=$data[$i]['MetraId']?></td><td>  <?=$data[$i]['MetraOrder']?></td><td><?=$data[$i]['MetraTitle']?></td><td>
                <a href="<?=THIS_PAGE?>?metraId=<?=$data[$i]['MetraId']?>"> Edit </a> | 
                <?php
                echo '<a onclick="msgbox('."'តើពិតជាចង់លុបមាត្រាលេខ ".$data[$i]['MetraOrder']."នេះមែន?','".THIS_PAGE."?action=deleted&id=".$data[$i]['MetraId']."&backid=".$_GET['kromid']."','_self','yesno'".')">Delete</a>';
                ?>
                </td></tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </fieldset>
</div>
<?php


}elseif (isset($_GET['metraId'])){
    $metra = db_get("metra","Where metraId='".$_GET['metraId']."'","","","",DB_NAME2);
    $desc = htmlspecialchars_decode($metra[0]['metraDescription'],ENT_QUOTES);
?>
    <form action="annotation_metra_edit.php" class="align-center" method="POST">
                    <input type="text" name="metra_id" value="<?=$metra[0]['metraId']?>" readonly hidden>
                    <div class="small-6 columns">
                        <label for="title">លេខមាត្រា (សូមបញ្ចូលតែលេខបានហើយ មិនត្រូវមានតួរអក្សរទេ!)
                            <input type="number" name="metra_order" required value="<?=$metra[0]['metraOrder']?>"/>
                        </label>
                    </div>
                    <div class="small-6 columns">
                        <label for="title">ចំណងជើង
                            <input type="text" name="metra_title" required value="<?=$metra[0]['metraTitle']?>"/>
                        </label>
                    </div>
                    
                    <div class="small-12">
                        <label for="title">និយមន័យ
                            
                            <textarea name="metra_desc" id="post_content" rows="10" cols="80" required>
                            <?=$desc?>
                            </textarea>
                                <script>
                                    // Replace the <textarea id="editor1"> with a CKEditor
                                    // instance, using default configuration.
                                    CKEDITOR.replace( 'post_content',{
                                        filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                                    } );
                            </script>
                        </label>
                    </div>
                    <br>
                    <div class="small-12 row">
                        <input type="submit" name="update" value="រក្សា" class="button"/>
                    </div>
    </form>
<?php
}elseif (isset($_POST['update'])){
    
    $cond = array("metraId"=>$_POST['metra_id']);
            $data = array(
                "metraTitle"=>$_POST['metra_title'],
                "metraDescription"=>htmlspecialchars_decode($_POST['metra_desc'],ENT_QUOTES),
                "metraOrder"=>$_POST['metra_order']
            );
    db_update("metra",$data,$cond,DB_NAME2);
    header("Location: ". THIS_PAGE);
}else{
    $data = db_get("krom","","","ORDER BY kromId ASC","",DB_NAME2);
        $thead = "<th>ចំណងជើងក្រម</th>";
        $tbody = "";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='<td><a href="'.THIS_PAGE.'?kromid='.$data[$i]['kromId'].'">'.$data[$i]['kromTitle'].'</a></td>';
            $tbody .='</tr>';
        }
        $tbody .='<tr>';
        $tbody .='<td><a href="'.THIS_PAGE.'?kromid=all">បង្ហាញមាត្រាទាំងអស់</a></td>';
        $tbody .='</tr>';
?>
<div class="small-12 columns big-menu">

<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
        <?=$thead?>
        </tr>
    </thead>
    <tbody>
    <?=$tbody?>
    </tbody>
</table>

</div>
<?php
    if(isset($_GET['action']) && $_GET['action'] == 'deleted'){
        $cond = array("metraId" => $_GET['id']);
                $get_data = db_get_where("metra",$cond,'',DB_NAME2);
                $data_log = array(
                    "UserId"=> $_SESSION['infos']['id'],
                    "Action"=> $_GET['action'],
                    "ItemName"=>"metra",
                    "ItemTitle"=>$get_data[0]['metraTitle'],
                    "Date"=>date('Y-m-d h:i:s')
                );
                db_insert('annotation_deleted_list',$data_log);
                db_delete('metra',$cond,DB_NAME2);
                header("Location: ".THIS_PAGE."?kromid=".$_GET['backid']);
    }
}
?>


</div>