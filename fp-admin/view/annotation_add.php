<?php
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
if (isset($_POST['Save'])){
  $title = htmlspecialchars($_POST["Title"],ENT_QUOTES);
  $writer= $_POST["Writer"];
  $company= $_POST["Company"];
  $year= $_POST["Year"];
  $month=$_POST["Month"];
  $day = $_POST["Day"];
  $description = htmlspecialchars($_POST['Description'],ENT_QUOTES);  
  $content = htmlspecialchars($_POST["Content"],ENT_QUOTES);  
  $domain=$_POST["Domain"];
  $category=$_POST["Category"];
  $link=$_POST["Link"];      
  $id = "";
  $data = array(
      "Title"=>$title,
      "Content"=>$content,
      "Writer"=>$writer,
      "Description"=> $description,
      "Company"=>$company,
      "Domain"=>$domain,
      "Categories"=>$category,
      "Day"=>$day,
      "Month"=>$month,
      "Year"=>$year,
      "Link"=>$link
  );
    db_insert('anotation',$data);
    $status = "The data has been saved!";
    $btn = "Cancel";
}elseif (isset($_GET['edit'])){
      $cond = array("Id"=>$_GET['edit']);
      $res = db_get_where('anotation',$cond);
    $id = $_GET['edit'];
    $title = $res[0]['Title'];
    $content = $res[0]['Content'];
    $writer=$res[0]['Writer'];
    $company=$res[0]['Company'];
    $day=$res[0]['Day'];
    $year=$res[0]['Year'];
    $month=$res[0]['Month'];
    $kh_month = khmer_month($month);
    $description=$res[0]['Description'];
    $domain=$res[0]['Domain'];
    $category=$res[0]['Categories'];
    $link=$res[0]['Link'];
    $btn = "Update";
    $status = "";
}elseif (isset($_POST['Update'])){
    
    $id = $_POST['id'];
    $title = htmlspecialchars($_POST["Title"],ENT_QUOTES);
  $writer= $_POST["Writer"];
  $company= $_POST["Company"];
  $year= $_POST["Year"];
  $month=$_POST["Month"];
  $day = $_POST["Day"];
  $description = htmlspecialchars($_POST['Description'],ENT_QUOTES);  
  $content = htmlspecialchars($_POST["Content"],ENT_QUOTES);  
  $domain=$_POST["Domain"];
  $category=$_POST["Category"];
  $link=$_POST["Link"];      
  $cond = array("Id"=>$id);
  $data = array(
      "Title"=>$title,
      "Description"=>$description,
      "Content"=>$content,
      "Writer"=>$writer,
      "Company"=>$company,
      "Domain"=>$domain,
      "Categories"=>$category,
      "Day"=>$day,
      "Month"=>$month,
      "Year"=>$year,
      "Link"=>$link
  );
  db_update('anotation',$data,$cond);
  $status = "The data has been updated!";
  $btn = "Cancel";
}else{
$btn = "Save";
$id ="";
  $title = "";
  $writer="";
  $company="";
  $day="";
  $year="";
  $month="";
  $description="";
  $content = "";  
  $status = "";
  $domain="";
  $category="";
  $link="";
  
}
?>
<div class="small-12 columns big-menu">
<script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>
<form action="<?=THIS_PAGE?>" method="POST">
<input type="hidden" name="id" value="<?=$id?>" readonly/>
    <div class="row">
        <div class="small-12">
            <label for="title">ឈ្មោះឯកសារ:*
                <input type="text" name="Title" value="<?=$title?>" placeholder="Please enter title here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">ឈ្មោះអ្នកសរសេរ/អ្នករៀបរៀង:*
                <input type="text" name="Writer" value="<?=$writer?>" placeholder="Please enter Writer\'s name here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">ស្ថាប័ន:
                <input type="text" name="Company" value="<?=$company?>" placeholder="Please enter Company\'s name here."/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">វិស័យ:
                <input type="text" name="Domain" value="<?=$domain?>" placeholder="Please enter Domain here."/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">ប្រភេទឯកសារ:
                <input type="text" name="Category" value="<?=$category?>" placeholder="Please enter Category here."/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-4 columns">
            <label for="title">ថ្ងៃ:
                <input type="text" name="Day" value="<?=$day?>" placeholder="Please enter Day in number here."/>
            </label>
        </div>
        <div class="small-4 columns">
            <label for="title">ខែ*:
                <?php 
                    if ($month ==""){
                        select_kh_month('Month','required');
                    }else{
                        echo '<input type="text" name="Month" value="'.$month.'">';
                    }
                    
                ?>    
            </label>
        </div>
        <div class="small-4 columns">
            <label for="title">ឆ្នាំ*:
                <input type="text" name="Year" value="<?=$year?>" placeholder="Please enter Day in number here." required/>
            </label>
        </div>
    </div>
    <div class="row">
    <label for="title">អត្ថបទសង្ខេប:*</label>
    </div>
    <div class="row">
        <textarea name="Description" id="Description" rows="10" cols="80" required>
        <?=$description?>
        </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'Description',{
                    filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                } );
            </script>
    </div>

    <div class="row">
    <label for="title">អត្ថបទ:*</label>
    </div>
    <div class="row">
        <textarea name="Content" id="post_content" rows="10" cols="80" required>
        <?=$content?>
        </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'post_content',{
                    filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                } );
            </script>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">Link:*
                <input type="text" name="Link" value="<?=$link?>" placeholder="Please enter Link here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <input type="submit" class="button right" name="<?=$btn?>" value="<?=$btn?>"/>
            <span><?=$status?></span>   
        </div>  
    </div>
</form>
</div>
<?php
}#end permission
?>