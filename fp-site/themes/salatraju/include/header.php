
<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="<?=DOMAIN?>fp-admin/img/logo.png">
    <?php
    $titile = "សាលាត្រាជូ";
    $content = "សមាគមសាលាត្រាជូមានសេចក្ដីរីករាយសូមនាំមកជូននូវគេហទំព័រថ្មីដែលផ្ទុកទៅដោយឯកសារជំនួយស្មារតី អត្ថបទច្បាប់ បទវិចារណកថា វីដេអូអប់រំច្បាប់ បទបង្ហាញច្បាប់ ព្រមទាំងព័ត៌មានជាច្រើនដទៃទៀតដែលមានសារៈសំខាន់ដល់សាធារណជនទូទៅ ក៏ដូចជាអ្នកសិក្សាស្វែងយល់អំពីច្បាប់ និងវិស័យដែលពាក់ព័ន្ធ។ ដោយឡែក អត្ថបទ និងឯកសារជំនួយស្មារតីត្រូវបាន និងកំពុងរៀបចំទៅតាមរចនាបទថ្មីប្រកបដោយលក្ខណៈវិទ្យាសាស្ត្រ និងស្តង់ដា។";
    $str_cont = '';

    if (VIEW_PAGE != "index"){
      //Get data from database
      if(isset($_GET['p'])){
        $id = $_GET['p'];
        $cond = array(
        'PostId'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
      }
      if(isset($_GET['page'])){
        $id = $_GET['page'];
        $cond = array(
        'PostTitle'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
      }
      if(isset($_GET['mem'])){
        $id = $_GET['mem'];
        $cond = array(
        'PostId'=>$id,
        'PostStatus'=>2
        );
        $res = db_get_where('fp_posts',$cond);
        
      }
      $url = DOMAIN.'index.php/post?p='.$id;
      $page_exception = array("lexique","member","annotation","member_add","about_lexique","gallery");
      if (in_array(VIEW_PAGE,$page_exception)){
          $id = "";
          if (VIEW_PAGE == "lexique"){
              $titile = "វាក្យស័ព្ទច្បាប់";
              $content ='«វាក្យស័ព្ទច្បាប់អេឡិចត្រូនិក» ​ឬ «Online Legal Terminology» គឺជាគម្រោងថ្មីមួយរបស់សមាគមសាលាត្រាជូ​ ដែលត្រូវបានរចនាឡើង ក្នុងគោលបំណងផ្ដល់ភាពងាយស្រួលក្នុងការស្វែងរកនិយមន័យនៃ​ពាក្យបច្ចេកទេសច្បាប់នីមួយៗ ព្រមទាំងដើម្បីលើកកម្ពស់ការសិក្សា​ស្រាវជ្រាវច្បាប់នៅក្នុងប្រទេសកម្ពុជា។';            
              if (isset($_GET['word'])){
                $word = $_GET['word'];
                $url = DOMAIN . "lexique.php?word=$word";
              }else{
                $url = DOMAIN . "lexique";
              }
          }else{
            $titile=VIEW_PAGE;
            $content='';  
          }
          
      }else{
          for($i=0;$i<count($res);$i++){
          $titile = $res[$i]['PostTitle'];
          $str_cont = htmlspecialchars_decode($res[$i]['PostContent'],ENT_QUOTES);

          if (stripos($str_cont,'img') == '' ){
            $content = substr($str_cont,stripos($str_cont,'<p>'),stripos($str_cont,'</p>'));
          }
          
        }
      }

    
    ?>
    <!--Facebook meta -->
    <meta property="og:url"                content="<?=$url?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?=$titile?>"/>
    <meta property="og:description"        content="<?=$content?>" />
    <!--meta property="og:image"              content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" / -->
    <?php
    }else{
      ?>
      <!--Facebook meta -->
      <meta property="og:url"                content="http://salatraju.com/" />
      <meta property="og:type"               content="article" />
      <meta property="og:title"              content="សាលាត្រាជូ"/>
      <meta property="og:description"        content="សមាគមសាលាត្រាជូមានសេចក្ដីរីករាយសូមនាំមកជូននូវគេហទំព័រថ្មីដែលផ្ទុកទៅដោយឯកសារជំនួយស្មារតី អត្ថបទច្បាប់ បទវិចារណកថា វីដេអូអប់រំច្បាប់ បទបង្ហាញច្បាប់ ព្រមទាំងព័ត៌មានជាច្រើនដទៃទៀតដែលមានសារៈសំខាន់ដល់សាធារណជនទូទៅ ក៏ដូចជាអ្នកសិក្សាស្វែងយល់អំពីច្បាប់ និងវិស័យដែលពាក់ព័ន្ធ។ ដោយឡែក អត្ថបទ និងឯកសារជំនួយស្មារតីត្រូវបាន និងកំពុងរៀបចំទៅតាមរចនាបទថ្មីប្រកបដោយលក្ខណៈវិទ្យាសាស្ត្រ និងស្តង់ដា។" />
    <?php
    }
    ?>
    <!--End Facebook meta -->
    <title><?=$titile?></title>
    <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/vendor/modernizr.js"></script>
    <script src="<?=VIRTUAL_PATH?>js/virgo.js"></script>
    <script src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/js/jquery.js"></script>

    <link rel="stylesheet" href="http://salatraju.com/fp-site/themes/salatraju/khmerfonts/font.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/khmerfonts/font.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/foundation6.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/foundation.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/souris.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/w3.css" /><!-- w3.css use for do the slideshow-->
    <link rel="stylesheet" href="http://salatraju.com/fp-site/themes/salatraju/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/main.css">
    <link rel="stylesheet" href="<?=VIRTUAL_PATH_SITE?>themes/salatraju/css/virgo.css" />
    <script type="text/javascript">
    $(document).ready(function () {
        //Disable full page
        /*
        $("body").on("contextmenu",function(e){
            return false;
        });
        
        $('body').bind('cut copy paste', function (e) {
        e.preventDefault();
        });
        //Disable part of page
        /*
        $("#id").on("contextmenu",function(e){
            return false;
        });
        */
    });
    </script>
    
    <?php
      if (THEME_CUSTOMIZE == 1){
        include 'customize.php';
      }
    ?>
   
  </head>
  <body>
  <?php
  //google analystic
  include PHYSICAL_PATH.'library/analystics.php';
  //facebook sdk
  include PHYSICAL_PATH.'library/facebook_sdk.php';
  ?>
  <div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
      
      <div class="off-canvas position-right" id="offCanvasRight" data-off-canvas data-position="right">
      <!-- right menu-->
            <ul class="vertical menu" data-drilldown>
              <?php include INCLUDE_PATH_SITE.'menu_drill.php'; ?>
            </ul>
      </div>
      <div class="off-canvas-content" data-off-canvas-content>
      <!-- page content middle-->
      <nav class="smal-12 columns hide-for-large-up banner">
          <div class="small-10 columns text-center">
            <a href="<?=NAV_PATH?>index.php/index"><img style="width:60%" src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/img/banner.png" alt="banner"></a>
          </div>
          <div class="small-2 columns offtoggle">
            <a  href="#" data-toggle="offCanvasRight"><i class="fa fa-bars fa-3x"></i></a>
          </div>
            
      </nav>
      <div class="small-12 columns big-banner show-for-large-up">
            <a href="<?=NAV_PATH?>index.php/index"><img style="width:60%" src="<?=VIRTUAL_PATH_SITE?>themes/salatraju/img/banner.png" alt="banner"></a>
      </div>
      <div class="small-12 columns navigation show-for-large-up">
            <div class="desktop-menu">
            <!-- ul class="desktop-menu dropdown menu" data-dropdown-menu -->
             <?php 
             $limit = "";
             include INCLUDE_PATH_SITE.'menu.php'; ?>
            <!-- /ul -->
            </div>
      </div>
      <!-- Start left Sidebar -->
      <?php
      if (key($_GET) =="" || key($_GET) == "home"){
        $class_content = "medium-6 large-6";
        sidebar_left('index-post','medium-3 large-3');  
      }else{
        $class_content = "medium-9 large-9";
      }
      
      ?>
      <!-- End left Sidebar -->
      <!-- Start Content Area -->
      <div class="small-12 <?=$class_content?> columns">
            <div class="small-12 columns content-area">
              

              
            