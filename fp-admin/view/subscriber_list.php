<?php
include PHYSICAL_PATH.'library/admin.php';
$permission = array("Admin","Superuser");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
?>
<?php 
if(isset($_POST["update"])){
    
    $val = array("LastName"=>$_POST['LastName'],
                "FirstName"=>$_POST['FirstName'],
                "LastNameKh"=>$_POST['LastNameKh'],
                "FirstNameKh"=>$_POST['FirstNameKh'],
                "Email"=>$_POST['Email'],
                "Sex"=>$_POST['Sex']
                );
    $cond = array("UserId"=>$_GET['userid']);
    db_update("subscribers",$val,$cond);
}
if (isset($_POST['Email'])){#search by email
    $res = db_get("subscribers","Where Email='".$_POST['Email']."'");
}else if(isset($_POST['LastName'])){#search by last name
    $res = db_get("subscribers","Where LastName='".$_POST['LastName']."'");
}else if(isset($_POST['FirstName'])){#search by first name
    $res = db_get("subscribers","Where FirstName='".$_POST['FirstName']."'");
}else{
    #get all the data here form database
    $res = db_get("subscribers");
}
?>
<div class="small-12 columns big-menu">
<h3>Search </h3>
<div class="small-4 columns">
    <form action="<?=THIS_PAGE?>" method="post"><label for="email">Email</label>
        <input type="email" name="Email" id="email" required>
        <input type="submit" class="button" value="search">
    </form>
</div>
<div class="small-4 columns">
    <form action="<?=THIS_PAGE?>" method="post"><label for="LastName">Last Name</label>
        <input type="text" name="LastName" id="LastName" required>
        <input type="submit" class="button" value="search">
    </form>
</div>
<div class="small-4 columns">
    <form action="<?=THIS_PAGE?>" method="post"><label for="FirstName">First Name</label>
        <input type="text" name="FirstName" id="FirstName" required>
        <input type="submit" class="button" value="search">
    </form>
</div>


</div>
<!-- List from here -->
<div class="small-12 columns big-menu">
<h3>subscribers' List</h3>
<div class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
  <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
        <tr >
        <th class="small-1 center">User Id</th><th class="center">ឈ្មោះ</th><th class="center">Name</th><th class="center">Sex</th>
        <th class="center">Email</th>
        <th class=" center">Date join</th><th class=" center">Date registration</th>
        <th class=" center">Date exprire</th>
        <th class=" center">Status</th>
        <th class=" center">Other</th><th></th>
        
        
        </tr>
        <?php
        if (count($res)==0){
            echo '
            <tr><td colspan="10" class="center">There are no record found!</td></tr>
            ';
        }
        if (isset($_GET['activate'])){
            
                $cond = array("Email"=>$_GET['email']);
                $val = array("Status"=>$_GET['activate']);
                db_update('subscribers',$val,$cond,);
                header("Location: ". THIS_PAGE);
            
        }
        for($i=0;$i<count($res);$i++)
        {
            if ($res[$i]['Status'] == 0){
                $status = "Pending";
                $btn_act = "<a href='".THIS_PAGE."?activate=1&email=".$res[$i]['Email']."'>Activate</a> | <a href='".THIS_PAGE."?activate=3'>Terminate</a>";
            }else if ($res[$i]['Status'] == 1){
                $status = "Activated";
                $btn_act = "<a href='".THIS_PAGE."?activate=3&email=".$res[$i]['Email']."'>Terminate</a>";
            }else if ($res[$i]['Status'] == 2){
                $status = "Expired";
                $btn_act = "<a href='".THIS_PAGE."?activate=1&email=".$res[$i]['Email']."'>Renew</a>";
            }else if ($res[$i]['Status'] == 3){
                $status = "Terminated";
                $btn_act = "<a href='".THIS_PAGE."?activate=1&email=".$res[$i]['Email']."'>Activate</a>";
            }

            $income = db_get('Income','WHERE UserId="'.$res[$i]['UserId'].'"');

        echo'
        <tr class="small">
        <td class="center">'.$res[$i]['UserId'].'</td>
        <td class="center">'.$res[$i]['LastNameKh'].' '.$res[$i]['FirstNameKh'].'</a></td>
        <td class="center">'.$res[$i]['LastName'].' '.$res[$i]['FirstName'].'</a></td>
        <td class="center">'.$res[$i]['Sex'].'</td><td>'.$res[$i]['Email'].'</td>
     
        <td class="center">'.format_date($res[$i]['DateJoin'],"d-m-Y").'</td>
        <td class="center">'.format_date($income[0]['DateRegistration'],"d-m-Y").'</td>
        <td class="center">'.format_date($income[0]['DateExpired'],"d-m-Y").'</td>
        <td class="center">'.$status.'</td>
        <td class="center">'.$btn_act.'</td>
        <td class="center"><a href="'.THIS_PAGE.'?edit='.$res[$i]['UserId'].'">Edit</a></td>
        
        
        
        </tr>
        ';
        }
        ?>
  </table>
</div>
</div>
<?php
    if (isset($_GET['edit'])){
?>
<div class="small-12 columns big-menu">
<h3>Edit Information</h3>
        <?php
        $res = db_get("subscribers","Where UserId='".$_GET['edit']."'");
        if (count($res)>0){
        ?>
        
        <form action="<?=THIS_PAGE?>?userid=<?=$_GET['edit']?>" method="post"><label for="email">Email</label>
            <div class="row">
                <div class="small-6 columns">
                Last Name :
                <input class="w3-input" type="text" name="LastName" value="<?=$res[0]['LastName']?>">
                </div>
                <div class="small-6 columns">
                First Name :
                <input class="w3-input" type="text" name="FirstName" value="<?=$res[0]['FirstName']?>">
                </div>
                <div  class="small-6 columns">
                Last Name Kh:
                <input class="w3-input" class="w3-input" type="text" name="LastNameKh" value="<?=$res[0]['LastNameKh']?>">
                </div>
                <div class="small-6 columns">
                First Name Kh:
                <input class="w3-input" type="text" name="FirstNameKh" value="<?=$res[0]['FirstNameKh']?>">
                </div>
                <div class="small-6 columns">
                Sex :
                <select class="w3-select" name="Sex" id="">
                    <option value="<?=$res[0]['Sex']?>"><?=$res[0]['Sex']?></option>
                    <option value="M">M</option>
                    <option value="F">F</option>
                </select>
                </div>
                <div class="small-6 columns">
                Email :
                <input class="w3-input" type="text" name="Email" value="<?=$res[0]['Email']?>">
                </div>
                <div class="small-12 columns">
                
                <input class="w3-button w3-green w3-block" type="submit" name="update" value="Update">
                </div>
            </div>
    </form>

        
</div>
<?php
        }
    }
    
?>

<?php
}#end permission
?>