<div class="small-12 columns big-menu">
<?php

if (isset($_POST['kinthi'])){
    //Unset all Session
    unset($_SESSION["KinthiId"]);
    unset($_SESSION["MetikaId"]);
    unset($_SESSION["ChapterId"]);
    unset($_SESSION["SectionId"]);
    unset($_SESSION["Kathapheak"]);
    // echo '<pre>';
    // var_dump($_SESSION);
    // echo '</pre>';    
    //Store krom Id to session to be use later
    $_SESSION['KromId'] = $_POST['krom_id'];
// Add krom reference into database
    $krom_id = $_SESSION['KromId'];
    if(isset($_POST['krom_description'])){
        $krom_desc = $_POST['krom_description'];
    }
    if(isset($_POST['krom_description'])){
        $krom_dateuse = $_POST['krom_dateuse'];
    }
    if(isset($_POST['krom_description'])){
        $krom_title = $_POST['krom_title'];
    }
    if(isset($_POST['krom_description'])){
        $val_krom = array(
                    "kromTitle"=>$_POST['krom_title'],
                    "kromDescrtiption"=>$krom_desc,
                    "kromDateuse"=>$krom_dateuse
                            );
        $cond_krom = array("kromId"=>$krom_id);
        #update krom description
        if ($krom_desc !=""){
            db_update('krom',$val_krom,$cond_krom,DB_NAME2);
        }
        #add reference
        for ($i=0;$i<count($_POST['ref_idkh']);$i++){
            if ($_POST['ref_title'][$i] != ''){
                if ($_POST["ref_date"][$i] == ''){
                    $ref_date = "0000/00/00";
                }else{
                    $ref_date = $_POST["ref_date"][$i];
                }
            $val_ref = array(
                "referenceIdkh"=>$_POST["ref_idkh"][$i],
                "referenceTitle"=>$_POST["ref_title"][$i],
                "referenceDate"=>$ref_date,
                "referenceDescription"=>$_POST["ref_desc"][$i],
                "KromId"=>$krom_id
            );
            
            db_insert('reference',$val_ref,DB_NAME2);
            }    
        }
    }else{
        $krom_data = db_get("krom","Where kromId='$krom_id' AND Status=1","","","",DB_NAME2);
        $krom_desc = $krom_data[0]['kromDescrtiption'];
        $krom_dateuse = $krom_data[0]['kromDateuse'];
        $krom_title = $krom_data[0]['kromTitle'];
    }
    
    
    $kinthi_data = db_get("kinthi","Where kromId='$krom_id' AND Status=1","","","",DB_NAME2);
    

    ?>


    <form action="<?=THIS_PAGE?>" method="POST">
    <!-- ផ្នែកគន្ថី -->
    <div class="row">
            <div class="small-12">
                <label for="title">
                <?php
                echo $krom_title;
                ?>
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden/>
                </label>
            </div>
    </div>
            <div class="row">
                <div class="small-12">
                    ឈ្មោះគន្ថី:*
                    <label for="title" id="kinthi_div">
                        <!-- <input type="text" id="kinthi_title" name="kinthi_title" required/> -->
                        <select id="kinthi_title" name="kinthi_title" onchange="toinput('kinthi_div','kinthi_title','new');">
                            <option value=""></option>
                            <option value="0">គ្មានមាតិកា</option>
                            <option value="new">Add New</option>
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" class="button" name="metika" value="Add New"/>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
<div class="row">
    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead><tr><th colspan="2">Id</th><th>ចំណងជើងគន្ថី</th><th></th></tr></thead>
    <tbody>
    <?php
    $count_data = 0;
    for($i=0;$i<count($kinthi_data);$i++){
        if ($kinthi_data[$i]['Status'] == -1 && $kinthi_data[$i]['Status'] != NULL){
            $status = "| រង់ចាំការពិនិត្យ";
        }else{
            $status = "";
        }
        if ($kinthi_data[$i]['Status'] != -2){
            $count_data++;
        echo '
        <tr onclick="box_check('."'kinthi".$i."'".');">
        <td><input type="radio" id="kinthi'.$i.'" name="kinthi_title" value="'.$kinthi_data[$i]['kinthiId'].'" required></td>
        <td>'.$kinthi_data[$i]['kinthiId'].'</td>
        <td>'.$kinthi_data[$i]['kinthiTitle'].'</td>
        <td><a href="annotation_edit.php?step=kinthi&id='.$kinthi_data[$i]['kinthiId'].'&back=kinthi.php&backid='.$krom_id.'&bname=krom_id">Edit</a> ';
        $permission = array("Admin","Superuser");
        if (in_array($_SESSION['infos']['role'],$permission)){
        echo '
        | <a href="annotation_request.php?step=kinthi&id='.$kinthi_data[$i]['kinthiId'].'&back=kinthi.php&backid='.$krom_id.'&bname=krom_id">Delete</a>';
        }
        echo'
        <i>'.$status.'</i>
        </td>
        </tr>
        ';
        }
    }
    ?>
    </tbody>
    </table>
                <?php
                
                if($count_data != 0){
                ?>
                <div class="small-12 large-1 columns right">
                    <input type="submit" class="button" name="metika" value="Next >"/>
                </div>  
                <?php
                }
                ?>
</form>
                <form action="annotation.php" method="POST">
                    <input type="text" name="krom_title" value="<?=$krom_title?>" readonly hidden>
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="back" value="< Back"/>
                    </div>  
                </form>
</div>
</div>
<?php
}

// meteka
if (isset($_POST['metika'])){
    
    $krom_id = $_SESSION['KromId'];
    if ($_POST['metika'] == "Add New"){
        $val = array(
            'kinthiTitle'=>$_POST['kinthi_title'],
            'kromId'=>$_POST['krom_id'],
            'Status'=>1
    
        );
        db_insert('kinthi',$val,DB_NAME2);
        $kinthi_data = db_get("kinthi","Where kinthiTitle='".$_POST['kinthi_title']."' AND Status=1","","","",DB_NAME2);
        $_SESSION["KinthiId"] = $kinthi_data[0]['kinthiId'];
        echo '
        <form action="'.THIS_PAGE.'" method="POST" id="return_back">
        <input type="text" class="button" name="metika" value="Next >" hidden/>
        </form>
        <script>form_submit("return_back");</script>';
    }else{
        if (isset($_SESSION["KinthiId"])){
            $kinthiId = $_SESSION["KinthiId"];
        }else{
            $kinthiId = $_POST['kinthi_title'];
            $_SESSION["KinthiId"] = $kinthiId;
        }
        $kinthi_data = db_get("kinthi","Where kinthiId='".$kinthiId."' AND kromId='".$krom_id."' AND Status=1","","","",DB_NAME2);   
    }   
        $methika_data = db_get("metika","Where kinthiId='".$kinthiId."' AND Status=1","","","",DB_NAME2);
?>
<form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
            <div class="small-12">
                <label for="title">
                    <h4>
                <?php
                
                if ($kinthi_data[0]['kinthiTitle'] == "0"){
                    echo 'មិនមានមាតិកា';
                }else{
                    echo $kinthi_data[0]['kinthiTitle'];
                }
                
                ?>
                    </h4>
                </label>
            </div>
    </div>
            <div class="row">
                <div class="small-12">
                    ឈ្មោះមាតិកា:*
                    <label for="title" id="kinthi_div">
                        <select id="metika_title" name="metika_title" onchange="toinput('kinthi_div','metika_title','new');">
                            <option value=""></option>
                            <option value="0">គ្មានមាតិកា</option>
                            <option value="new">Add New</option>
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" class="button" name="chapter" value="Add New"/>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
<div class="row">
    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead><tr><th>Id</th><th>ចំណងជើងមាតិកា</th><th></th></tr></thead>
    <tbody>
    <?php
    $count_data = 0;
    for($i=0;$i<count($methika_data);$i++){
        if ($methika_data[$i]['Status'] == -1 && $methika_data[$i]['Status'] != NULL){
            $status = "| រង់ចាំការពិនិត្យ";
        }else{
            $status = "";
        }
        if ($methika_data[$i]['Status'] != -2){
            $count_data++;
            
            if ($methika_data[$i]['metikaTitle'] == "0"){
                $metika_title = "មិនមានមាតិកា";
            }else{
                $metika_title = $methika_data[$i]['metikaTitle'];
            }
        echo '
        <tr onclick="box_check('."'metika".$i."'".');">
        <td><input type="radio" id="metika'.$i.'" name="metika_title" value="'.$methika_data[$i]['metikaId'].'" required></td>
        
        <td>'.$metika_title.'</td>
        <td><a href="annotation_edit.php?step=metika&id='.$methika_data[$i]['metikaId'].'&back=kinthi.php&backid='.$kinthiId.'&bname=kinthi_title&previous=krom_id&previousval='.$krom_id.'">Edit</a> ';
        $permission = array("Admin","Superuser");
        if (in_array($_SESSION['infos']['role'],$permission)){
        echo'
        | <a href="annotation_request.php?step=metika&id='.$methika_data[$i]['metikaId'].'&back=kinthi.php&backid='.$kinthiId.'&bname=kinthi_title&previous=krom_id&previousval='.$krom_id.'">Delete</a>';
        }
        echo'
        <i>'.$status.'</i>
        </td>
        </tr>
        ';
        }
    }
    ?>
    </tbody>
    </table>
                <?php
                if($count_data != 0){
                ?>
                <div class="small-12 large-1 columns right">
                    <input type="submit" class="button" name="chapter" value="Next >"/>
                    
                </div>  
                <?php
                }
                ?>
</form>
                <form action="<?=THIS_PAGE?>" method="POST">
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden> <!-- For go to before back page-->
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="kinthi" value="< Back"/>
                    </div>  
                </form>
</div>
</div>
<?php    
}
// Chapter
if (isset($_POST['chapter'])){
    $kinthiId = $_SESSION["KinthiId"];
    $krom_id = $_SESSION["KromId"];
    if ($_POST['chapter'] == "Add New"){
        $val = array(
            'metikaTitle'=>$_POST['metika_title'],
            'kinthiId'=>$_SESSION['KinthiId'],
            'Status'=>1
        );
        db_insert('metika',$val,DB_NAME2);
        $metika_data = db_get("metika","Where metikaTitle='".$_POST['metika_title']."' AND kinthiId='".$kinthiId."' AND Status=1","","","",DB_NAME2);
        $_SESSION["MetikaId"] = $metika_data[0]['metikaId'];
        echo '
        <form action="'.THIS_PAGE.'" method="POST" id="return_back">
        <input type="text" class="button" name="chapter" value="Next >" hidden/>
        </form>
        <script>form_submit("return_back");</script>';
    }else{
        
        $metika_data = db_get("metika","Where metikaId='".$_POST['metika_title']."' AND Status=1","","","",DB_NAME2);
    }
        $chpater_data = db_get("chapter","Where metikaId='".$_POST['metika_title']."' AND Status='1'","","","",DB_NAME2);
        $_SESSION["MetikaId"] = $metika_data[0]['metikaId'];
        $metikaId = $metika_data[0]['metikaId'];
?>
    <form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
            <div class="small-12">
                <label for="title">
                <?php
                
                if ($metika_data[0]['metikaTitle'] == "0"){
                    echo 'មិនមានមាតិកា';
                }else{
                    echo $metika_data[0]['metikaTitle'];
                }
                
                ?>
                    
                </label>
            </div>
    </div>
            <div class="row">
                <div class="small-12">
                    ឈ្មោះជំពូក:*
                    <label for="title" id="kinthi_div">
                        <select id="chapter_title" name="chapter_title" onchange="toinput('kinthi_div','chapter_title','new');">
                            <option value=""></option>
                            <option value="0">គ្មានជំពូក</option>
                            <option value="new">Add New</option>
                            
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" class="button" name="section" value="Add New"/>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
<div class="row">
    <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead><tr><th colspan="2">Id</th><th>ចំណងជើងជំពូក</th><th></th></tr></thead>
    <tbody>
    <?php
    $count_data = 0;
    for($i=0;$i<count($chpater_data);$i++){
        
        if ($chpater_data[$i]['Status'] == -1 && $chpater_data[$i]['Status'] != NULL){
            $status = "| រង់ចាំការពិនិត្យ";
        }else{
            $status = "";
        }
        if ($chpater_data[$i]['Status'] != -2){
            $count_data++;
            if ($chpater_data[$i]['chapterTitle'] == "0"){
                $chapter_title = "មិនមានមាតិកា";
            }else{
                $chapter_title = $chpater_data[$i]['chapterTitle'];
            }
        echo '
        <tr onclick="box_check('."'chapter".$i."'".');">
        <td><input type="radio" id="chapter'.$i.'" name="chapter_title" value="'.$chpater_data[$i]['chapterId'].'" required></td>
        <td>'.$chpater_data[$i]['chapterId'].'</td>
        <td>'.$chapter_title.'</td>
        <td><a href="annotation_edit.php?step=chapter&id='.$chpater_data[$i]['chapterId'].'&back=kinthi.php&backid='.$metikaId.'&bname=metika_title&previous=kinthi_id&previousval='.$kinthiId.'">Edit</a> ';
        $permission = array("Admin","Superuser");
        if (in_array($_SESSION['infos']['role'],$permission)){
        echo'
        | <a href="annotation_request.php?step=chapter&id='.$chpater_data[$i]['chapterId'].'&back=kinthi.php&backid='.$metikaId.'&bname=metika_title&previous=kinthi_id&previousval='.$kinthiId.'">Delete</a>';
        }
        echo '
        <i>'.$status.'</i>
        </td>
        </tr>
        ';
        }
    }
    ?>
    </tbody>
    </table>
                <?php
                if ($count_data != 0){
                ?>
                <div class="small-12 large-1 columns right">
                    <input type="submit" class="button" name="section" value="Next >"/>
                </div>  
                <?php
                }
                ?>
</form>
                <form action="<?=THIS_PAGE?>" method="POST">
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden>
                    <input type="text" name="kinthi_title" value="<?=$kinthiId?>" readonly hidden>
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="metika" value="< Back"/>
                    </div>  
                </form>
</div>
</div>
<?php
}
// Section
if (isset($_POST['section'])){
    $krom_id = $_SESSION["KromId"];
    $kinthiId = $_SESSION["KinthiId"];
    $metikaId = $_SESSION["MetikaId"];
    if ($_POST['section'] == "Add New"){
        $val = array(
            'chapterTitle'=>$_POST['chapter_title'],
            'metikaId'=>$_SESSION['MetikaId'],
            'Status'=>1
        );
        db_insert('chapter',$val,DB_NAME2);
        $chapter_data = db_get("chapter","Where chapterTitle='".$_POST['chapter_title']."' AND metikaId='$metikaId' AND Status=1","","","",DB_NAME2);
        $_SESSION["ChapterId"] = $chapter_data[0]['chapterId'];
        echo '
        <form action="'.THIS_PAGE.'" method="POST" id="return_back">
        <input type="text" class="button" name="section" value="Next >" hidden/>
        </form>
        <script>form_submit("return_back");</script>';
    }else{
        
        $chapter_data = db_get("chapter","Where chapterId='".$_POST['chapter_title']."' AND Status=1","","","",DB_NAME2);
    }
        $section_data = db_get("section","Where chapterId='".$_POST['chapter_title']."' AND Status=1","","","",DB_NAME2);
        $_SESSION["ChapterId"] = $chapter_data[0]['chapterId'];
        $chapterId = $chapter_data[0]['chapterId'];
?>
<form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
            <div class="small-12">
                <label for="title">
                    <h4>
                <?php
                if ($chapter_data[0]['chapterTitle'] == "0"){
                    echo "មិនមានជំពូក";
                }else{
                    echo $chapter_data[0]['chapterTitle'];
                }
                
                ?>
                </h4>
                    <input type="text" name="chapter_id" value="<?=$chapterId?>" readonly hidden/>
                </label>
            </div>
    </div>
            <div class="row">
                <div class="small-12">
                    ឈ្មោះផ្នែក:*
                    <label for="title" id="chapter_div">
                        <select id="section_title" name="section_title" onchange="toinput('chapter_div','section_title','new');">
                            <option value=""></option>
                            <option value="0">គ្មានផ្នែក</option>
                            <option value="new">Add New</option>
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" class="button" name="kathapheak" value="Add New"/>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
<div class="row">
    <table  class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead><tr><th colspan="2">Id</th><th>ចំណងជើងផ្នែក</th><th></th></tr></thead>
    <tbody>
    <?php
    $count_data=0;
    for($i=0;$i<count($section_data);$i++){
        if ($section_data[$i]['sectionTitle'] == "0"){
            $section_title = "គ្មានចំណងជើង";
        }else{
            $section_title = $section_data[$i]['sectionTitle'];
        }
        if ($section_data[$i]['Status'] == -1 && $section_data[$i]['Status'] != NULL){
            $status = "| រង់ចាំការពិនិត្យ";
        }else{
            $status = "";
        }
        if ($section_data[$i]['Status'] != -2){
            $count_data++;
        echo '
        <tr onclick="box_check('."'section".$i."'".');">
        <td><input type="radio" id="section'.$i.'" name="section_title" value="'.$section_data[$i]['sectionId'].'" required></td>
        <td>'.$section_data[$i]['sectionId'].'</td>
        <td>'.$section_title.'</td>
        <td><a href="annotation_edit.php?step=section&id='.$section_data[$i]['sectionId'].'&back=kinthi.php&backid='.$chapterId.'&bname=chapter_title&previous=metika_title&previousval='.$metikaId.'">Edit</a> ';
        $permission = array("Admin","Superuser");
        if (in_array($_SESSION['infos']['role'],$permission)){
        echo'
        | <a href="annotation_request.php?step=section&id='.$section_data[$i]['sectionId'].'&back=kinthi.php&backid='.$chapterId.'&bname=chapter_title&previous=metika_title&previousval='.$metikaId.'">Delete</a>';
        }
        echo '<i>'.$status.'</i>
        </td>
        </tr>
        ';
        }
    }
    ?>
    </tbody>
    </table>
                <?php
                if ($count_data != 0){
                ?>
                <div class="small-12 large-1 columns right">
                    <input type="submit" class="button" name="kathapheak" value="Next >"/>
                    
                </div>  
                <?php
                }
                ?>
</form>
                <form action="<?=THIS_PAGE?>" method="POST">
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden>
                    <input type="text" name="kinthi_title" value="<?=$kinthiId?>" readonly hidden>
                    <input type="text" name="metika_title" value="<?=$metikaId?>" readonly hidden>
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="chapter" value="< Back"/>
                    </div>  
                </form>
</div>
</div>
<?php
}
// Kathapheak
if (isset($_POST['kathapheak'])){
    $krom_id = $_SESSION["KromId"];
    $kinthiId = $_SESSION["KinthiId"];
    $metikaId = $_SESSION["MetikaId"];
    $chapterId = $_SESSION["ChapterId"];

    if ($_POST['kathapheak'] == "Add New"){
        $val = array(
            'sectionTitle'=>$_POST['section_title'],
            'chapterId'=>$chapterId,
            'Status'=>1
        );
        db_insert('section',$val,DB_NAME2);
        $section_data = db_get("section","Where sectionTitle='".$_POST['section_title']."' AND chapterId='".$chapterId."' AND Status=1","","","",DB_NAME2);
        $_SESSION['SectionId'] = $section_data[0]['sectionId'];
        echo '
        <form action="'.THIS_PAGE.'" method="POST" id="return_back">
        <input type="text" class="button" name="kathapheak" value="Next >" hidden/>
        </form>
        <script>form_submit("return_back");</script>';
    }else{
        
        $section_data = db_get("section","Where sectionId='".$_POST['section_title']."' AND chapterId='$chapterId' AND Status=1","","","",DB_NAME2);
    }
        $_SESSION['SectionId'] = $section_data[0]['sectionId'];    
        $sectionId = $section_data[0]['sectionId'];  
        $kathapheak_data = db_get("kathapheak","Where sectionId='".$sectionId."' AND Status=1","","","",DB_NAME2);
        

?>
<form action="<?=THIS_PAGE?>" method="POST">
    <div class="row">
            <div class="small-12">
                <label for="title">
                <?php
                if ($section_data[0]['sectionTitle'] == "0"){
                    echo "មិនមានផ្នែកទេ!";
                }else{
                    echo $section_data[0]['sectionTitle'];
                }
                ?>
                    <input type="text" name="section_id" value="<?=$sectionId?>" readonly hidden/>
                </label>
            </div>
    </div>
            <div class="row">
                <div class="small-12">
                    ឈ្មោះកថាភាគ:*
                    <label for="title" id="katapheak_div">
                        <select id="katapheak_title" name="katapheak_title" onchange="toinput('katapheak_div','katapheak_title','new');">
                            <option value=""></option>
                            <option value="0">គ្មានកថាភាគ</option>
                            <option value="new">Add New</option>
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" class="button" name="metra" value="Add New"/>
    </form>
    <form action="<?=THIS_PAGE?>" method="POST">
<div class="row">
   <table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead><tr><th colspan="2">Id</th><th>ចំណងជើងកថាភាគ</th><th></th></tr></thead>
    <tbody>
    <?php
    $count_data=0;
    for($i=0;$i<count($kathapheak_data);$i++){
        if ($kathapheak_data[$i]['kathapheakTitle'] == "0"){
            $kathapheak_title = "គ្មានចំណងជើង";
        }else{
            $kathapheak_title = $kathapheak_data[$i]['kathapheakTitle'];
        }
        if ($kathapheak_data[$i]['Status'] == -1 && $kathapheak_data[$i]['Status'] != NULL){
            $status = "| រង់ចាំការពិនិត្យ";
        }else{
            $status = "";
        }
        if ($kathapheak_data[$i]['Status'] != -2){
            $count_data++;
        echo '
        <tr onclick="box_check('."'katapheak".$i."'".');">
        <td><input type="radio" id="katapheak'.$i.'" name="katapheak_title" value="'.$kathapheak_data[$i]['kathapheakId'].'" required></td>
        <td>'.$kathapheak_data[$i]['kathapheakId'].'</td>
        
        <td>'.$kathapheak_title.'</td>
        <td><a href="annotation_edit.php?step=kathapheak&id='.$kathapheak_data[$i]['kathapheakId'].'&back=kinthi.php&backid='.$sectionId.'&bname=section_title&previous=chapter_title&previousval='.$chapterId.'">Edit</a> ';
        $permission = array("Admin","Superuser");
        if (in_array($_SESSION['infos']['role'],$permission)){
        echo'
        | <a href="annotation_request.php?step=kathapheak&id='.$kathapheak_data[$i]['kathapheakId'].'&back=kinthi.php&backid='.$sectionId.'&bname=section_title&previous=chapter_title&previousval='.$chapterId.'">Delete</a>';
        }
        echo'
        <i>'.$status.'</i>
        </td>
        </tr>
        ';
        }
    }
    ?>
    </tbody>
    </table>
                <?php
                if ($count_data != 0){
                ?>
                <div class="small-12 large-1 columns right">
                    <input type="submit" class="button" name="metra" value="Next >"/>
                    
                </div>  
                <?php
                }
                ?>
</form>
                <form action="<?=THIS_PAGE?>" method="POST">
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden>
                    <input type="text" name="kinthi_title" value="<?=$kinthiId?>" readonly hidden>
                    <input type="text" name="metika_title" value="<?=$metikaId?>" readonly hidden>
                    <input type="text" name="chapter_title" value="<?=$chapterId?>" readonly hidden>
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="section" value="< Back"/>
                    </div>  
                </form>
</div>
</div>
<?php
}
if (isset($_POST['metra'])){
    $krom_id = $_SESSION["KromId"];
    $kinthiId = $_SESSION["KinthiId"];
    $metikaId = $_SESSION["MetikaId"];
    $chapterId = $_SESSION["ChapterId"];
    $sectionId = $_SESSION["SectionId"];
    if ($_POST['metra'] == "Add New"){
        $val = array(
            'kathapheakTitle'=>$_POST['katapheak_title'],
            'sectionId'=>$sectionId,
            'Status'=>1
        );
        db_insert('kathapheak',$val,DB_NAME2);
        $kathapheak_data = db_get("kathapheak","Where kathapheakTitle='".$_POST['katapheak_title']."' AND sectionId='".$sectionId."' AND Status=1","","","",DB_NAME2);
        $_SESSION['Kathapheak'] = $kathapheak_data[0]['kathapheakId'];
        echo '
        <form action="'.THIS_PAGE.'" method="POST" id="return_back">
        <input type="text" class="button" name="metra" value="Next >" hidden/>
        </form>
        <script>form_submit("return_back");</script>';
    }else{
        
        $kathapheak_data = db_get("kathapheak","Where kathapheakId='".$_POST['katapheak_title']."' AND sectionId='$sectionId' AND Status=1","","","",DB_NAME2);
        $_SESSION['Kathapheak'] = $kathapheak_data[0]['kathapheakId'];
    }
?>
<fieldset>
<legend>ព័តមានសង្ខេប</legend>

<?php
$section_data = db_get("section","Where sectionId='".$kathapheak_data[0]['sectionId']."' AND Status=1","","","",DB_NAME2);
$chpater_data = db_get("chapter","Where chapterId='".$section_data[0]['chapterId']."' AND Status=1","","","",DB_NAME2);
$metika_data = db_get("metika","Where metikaId='".$chpater_data[0]['metikaId']."' AND Status=1","","","",DB_NAME2);
$kinthi_data = db_get("kinthi","Where kinthiId='".$metika_data[0]['kinthiId']."' AND Status=1","","","",DB_NAME2);
$krom_data = db_get("krom","Where kromId='".$kinthi_data[0]['kromId']."' AND Status=1","","","",DB_NAME2);
?>
<h5>
ក្រម ៖
<?php
if ($krom_data[0]['kromTitle'] == "0"){
    echo "មិនមានក្រម";
}else{
    echo $krom_data[0]['kromTitle'];
}
?>
</h5>

<h5>
គន្ថី ៖
<?php
if ($kinthi_data[0]['kinthiTitle'] == "0"){
    echo "មិនមានគន្ថី";
}else{
    echo $kinthi_data[0]['kinthiTitle'];
}
?>
</h5>

<h5>
មាតិកា ៖
<?php
if ($metika_data[0]['metikaTitle'] == "0"){
    echo "មិនមានមាតិកា";
}else{
    echo $metika_data[0]['metikaTitle'];
}
?>
</h5>

<h5>
ជំពូក ៖
<?php
if ($chpater_data[0]['chapterTitle'] == "0"){
    echo "មិនមានជំពូក";
}else{
    echo $chpater_data[0]['chapterTitle'];
}
?>
</h5>
<h5>
ផ្នែក ៖
<?php

if ($section_data[0]['sectionTitle'] == "0"){
    echo "មិនមានផ្នែក";
}else{
    echo $section_data[0]['sectionTitle'];
}
?>
</h5>
<h5>
កថាភាគ ៖ <?php 
if ($kathapheak_data[0]['kathapheakTitle'] == "0"){
    echo "មិនមានកថាភាគ";
}else{
    echo $kathapheak_data[0]['kathapheakTitle'];
}
$kathapheakId = $kathapheak_data[0]['kathapheakId'];
?>
</h5>
                <form action="<?=THIS_PAGE?>" method="POST">
                    <input type="text" name="krom_id" value="<?=$krom_id?>" readonly hidden>
                    <input type="text" name="kinthi_title" value="<?=$kinthiId?>" readonly hidden>
                    <input type="text" name="metika_title" value="<?=$metikaId?>" readonly hidden>
                    <input type="text" name="chapter_title" value="<?=$chapterId?>" readonly hidden>
                    <input type="text" name="section_title" value="<?=$sectionId?>" readonly hidden>
                    <div class="small-12 large-1 columns">
                        <input type="submit" class="button" name="kathapheak" value="< Back"/>
                    </div>  
                </form>
</fieldset>
<form action="annotation_metra.php" method="POST">
<div class="row">
    <div class="small-12 columns">
        <label for="title">
            <input type="text" name="kathapheakId" value="<?=$kathapheakId?>"  readonly hidden/>
        </label>
    </div>
</div>

<div class="row">
            <fieldset>
                <legend>មាត្រា</legend>
                <button class="button" type="button"
                onclick= "addMetra('metra');" >បន្ថែម</button>
                <button class="button" type="submit">រក្សាទុក</button>
                <div id="metra">
                    <div class="small-6 columns">
                        <label for="title">លេខមាត្រា (សូមបញ្ចូលតែលេខបានហើយ មិនត្រូវមានតួរអក្សរទេ!)
                            <input type="number" name="metra_order[0]" required/>
                        </label>
                    </div>
                    <div class="small-6 columns">
                        <label for="title">ចំណងជើង
                            <input type="text" name="metra_title[0]" class="metra" required/>
                        </label>
                    </div>
                    
                    <div class="small-12">
                        <label for="title">និយមន័យ
                            
                                <textarea name="metra_desc[0]" id="post_content" rows="10" cols="80" required>
                                </textarea>
                                    <script>
                                        // Replace the <textarea id="editor1"> with a CKEditor
                                        // instance, using default configuration.
                                        CKEDITOR.replace( 'post_content',{
                                            filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                                        } );
                                    </script>
                            
                        </label>
                    </div>
                </div>
            </fieldset>
        </div>
</form>
<?php
}
?>
</div>
