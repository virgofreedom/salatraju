<?php 

$permission = array("Admin","Superuser","LegalUpdate","AdminLegalUpdate");
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    $db_name = "salatraju_legal_update";
if(!isset($_POST['saved']) && !isset($_POST['published'])){
    
    if (isset($_POST['legal_number'])){
        $data_number = db_get('legal_update_number','Where Id = "'.$_POST['legal_number'].'"');
        if (count($data_number) == 0){
            // Add new
            $val = array("Title"=>$_POST['legal_number']);
            db_insert("legal_update_number",$val);
            $legalnumer = db_get("legal_update_number","WHERE Title='".$_POST['legal_number']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
        }else{
            // use old
            $legalid = $data_number[0]['Id'];
            $legalTitle = $data_number[0]['Title'];
        }
        $new_mode = 0;
            $id = "";
            $title = "";
            $content = "";
            $link = "";
            $status = "";
            if (isset($_SESSION["infos"]["username"])){
                $writer = $_SESSION["infos"]["last_name"]." ".$_SESSION["infos"]["first_name"];
            }else{
                $writer = "";
            }
    }else{

    
        if (isset($_GET['id'])){
            // Edit mode
            $new_mode = 0;
            $data = db_get('legal_update','Where Id="'.$_GET['id'].'" AND UserId="'.$_SESSION['infos']['id'].'"');
            $id = $_GET['id'];
            $legalnumer = db_get("legal_update_number","WHERE Id='".$data[0]['LegalNumber']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
            $title = $data[0]['Title'];
            $writer = $data[0]['Writer'];
            $content = $data[0]['Content'];
            $link = $data[0]['Link'];
            $status = $data[0]['Status'];
        }else{
            // Add New mode
            $new_mode = 1;
            $id = "";
            $title = "";
            $content = "";
            $link = "";
            $status = "";
            if (isset($_SESSION["infos"]["username"])){
                $writer = $_SESSION["infos"]["last_name"]." ".$_SESSION["infos"]["first_name"];
            }else{
                $writer = "";
            }
        }
    }
    
}elseif (isset($_POST['saved'])){
    // Save entry here
    $new_mode = 0;
    $title = htmlspecialchars($_POST['title'],ENT_QUOTES);
    $writer = htmlspecialchars($_POST['writer'],ENT_QUOTES);
    $content = htmlspecialchars($_POST['content'],ENT_QUOTES);
    $link = $_POST['link'];
    $data = array(
        "LegalNumber"=>$_POST['LegalId'],
        'UserId'=> $_SESSION['infos']['id'],
        "Title"=>$title,
        "Writer"=>$writer,
        "Content"=>$content,
        "Link"=>$link,
        "DateModify"=>date('Y-m-d H:i:s'),
        "Status"=>"Saved"
    );
    // Check if there is the same title or not
    $data_title = db_get('legal_update','WHERE Title="'.$title.'"');
    // Check if there is already Id
    $data_id = db_get('legal_update','WHERE Id="'.$_POST['Id'].'"');
    $cond = array("Id"=>$_POST['Id']);
    if (count($data_id)>0){
        db_update('legal_update',$data,$cond);
        $data_id_updated = db_get('legal_update','WHERE Id="'.$_POST['Id'].'"');
        $id = $data_id_updated[0]['Id'];
        $status = $data_id_updated[0]['Status'];
        $legalnumer = db_get("legal_update_number","WHERE Id='".$data_id_updated[0]['LegalNumber']."'");
        $legalid = $legalnumer[0]['Id'];
        $legalTitle = $legalnumer[0]['Title'];
    }else{
        
        if (count($data_title)>0){
            // Show message
            msgbox("This title is already in use with other article, Please change your title.");
            $id = '';
            $legalnumer = db_get("legal_update_number","WHERE Id='".$_POST['LegalId']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
            $status = "Error: Doublicate Title";
        }else{
            db_insert('legal_update',$data);
            $data_saved = db_get('legal_update','WHERE Title="'.$title.'"');
            $id = $data_saved[0]['Id'];
            
            $status = $data_saved[0]['Status'];
            $legalnumer = db_get("legal_update_number","WHERE Id='".$data_saved[0]['LegalNumber']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
        }
    }
    
    
}elseif (isset($_POST['published'])){
    $new_mode = 0;
    // Save entry here
    $title = htmlspecialchars($_POST['title'],ENT_QUOTES);
    $writer = htmlspecialchars($_POST['writer'],ENT_QUOTES);
    $content = htmlspecialchars($_POST['content'],ENT_QUOTES);
    $link = $_POST['link'];
    $data = array(
        "LegalNumber"=>$_POST['LegalId'],
        'UserId'=> $_SESSION['infos']['id'],
        "Title"=>$title,
        "Writer"=>$writer,
        "Content"=>$content,
        "Link"=>$link,
        "DateModify"=>date('Y-m-d H:i:s'),
        "Status"=>"Published"
    );
    // Check if there is the same title or not
    $data_title = db_get('legal_update','WHERE Title="'.$title.'"');
    // Check if there is already Id
    $data_id = db_get('legal_update','WHERE Id="'.$_POST['Id'].'"');
    $cond = array("Id"=>$_POST['Id']);
    if (count($data_id)>0){
        db_update('legal_update',$data,$cond);
        $data_id_updated = db_get('legal_update','WHERE Id="'.$_POST['Id'].'"');
        $id = $data_id_updated[0]['Id'];
        $status = $data_id_updated[0]['Status'];
        $legalnumer = db_get("legal_update_number","WHERE Id='".$data_id_updated[0]['LegalNumber']."'");
        $legalid = $legalnumer[0]['Id'];
        $legalTitle = $legalnumer[0]['Title'];
    }else{
        if (count($data_title)>0){
            // Show message
            msgbox("This title is already in use with other article, Please change your title.");
            $id = '';
            $legalnumer = db_get("legal_update_number","WHERE Id='".$_POST['LegalId']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
            $status = "Error: Doublicate Title";
        }else{
            db_insert('legal_update',$data);
            $data_saved = db_get('legal_update','WHERE Title="'.$title.'"');
            $id = $data_saved[0]['Id'];
            $status = $data_saved[0]['Status'];
            $legalnumer = db_get("legal_update_number","WHERE Id='".$data_saved[0]['LegalNumber']."'");
            $legalid = $legalnumer[0]['Id'];
            $legalTitle = $legalnumer[0]['Title'];
        }
    }
}
    if ($new_mode == 1){
        $legal_data = db_get("legal_update_number","","","","",$db_name);
?>
<div class="small-12 columns big-menu w3-padding-large">
    <form action="lg_update_meteka" method="GET">
            <div class="row">
                <div class="small-12">
                លេខបច្ចុប្បន្នភាគគតិយុត្តកម្ពុជាប្រចាំសប្តាហ៍:*
                    <label for="lgupdate_number" id="legal_div">
                    
                        <select id="lgupdate_number" name="lgupdate_number" class="w3-input" onchange="toinput('legal_div','lgupdate_number','new');" required>
                            <option value=""></option>
                            <option value="new">Add New</option>
                            <?php
                            for($i=0;$i<count($legal_data);$i++){
                                echo '<option value="'.$legal_data[$i]['Id'].'">'.$legal_data[$i]['Title'].'</option>';
                            }
                            ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <input type="submit" class="w3-button w3-right w3-green" value="Next"/>
                    
                </div>  
            </div>
    </form>
</div>
<?php
    }else{
?>

<div class="small-12 columns big-menu w3-padding-large">
    <script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>
    <form action="<?=THIS_PAGE?>" method="POST">
    <input class="w3-input" type="text" name="Id" value="<?=$id?>" readonly hidden/>
    <input class="w3-input" type="text" name="LegalId" value="<?=$legalid?>" readonly hidden/>
    <h5><?=$legalTitle?></h5>
    <div class="row">
        <div class="small-12">
            <label for="title">ចំណងជើង ៖
                <input class="w3-input" type="text" name="title" value="<?=$title?>" placeholder="Please enter title here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">ឈ្មោះអ្នករៀបរៀង ៖
                <input class="w3-input" type="text" name="writer" value="<?=$writer?>" placeholder="Please enter writer here." required/>
            </label>
        </div>
    </div>
    <div class="row">
        <div class="small-12">
            <label for="title">Link to download ៖
                <input class="w3-input" type="text" name="link" value="<?=$link?>" placeholder="Please enter link here." required/>
            </label>
        </div>
    </div>
    <div class="row">
    <label for="title">អត្ថបទ៖*</label>
    </div>
    <div class="row">
        <textarea name="content" id="post_content" rows="10" cols="80">
        <?=$content?>
        </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'post_content',{
                    filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                } );
            </script>
    </div>
    <div class="row">
    Status : <?=$status?>
    </div>
    <div class="row w3-padding-large">
        <input class="w3-button w3-green" type="submit" value="Save" name="saved">
        <input class="w3-button w3-green" type="submit" value="Publish" name="published">
    </div>
    </form>
</div>
<?php
    }
}
?>