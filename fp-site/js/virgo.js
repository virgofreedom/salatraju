//img_view is a function to view the picture when we just select from file. 
function img_view(input,receiver,width,height) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    document.getElementById(receiver).src = e.target.result;
    document.getElementById(receiver).style.width = width;
    document.getElementById(receiver).style.height = height;
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
//function to print the container of an iframe.
function print_iframe(el){
  window.frames[el].contentWindow.print();
}
//function to print the container of an div.
function printContent(el,type_print,title)
{
	var restorepage = document.body.innerHTML;
  var printcontent;
  if (type_print == "receipt")
  {
    printcontent = document.getElementById(el).innerHTML + document.getElementById(el).innerHTML;  
  }else if (type_print == "table"){
    var Table_list = document.getElementById("table_employee");
	
      for (var i=0; i< Table_list.rows.length;i++)
      {
        if (i == 0)
        {
        Table_list.rows[i].deleteCell(8);  
        }else{
        Table_list.rows[i].deleteCell(10);  
        }
        
      }
    printcontent = '<h3 style="text-align:center;">'+title+'</h3>'+ document.getElementById(el).innerHTML;
  } else{
    printcontent = document.getElementById(el).innerHTML;
  }
	
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
  
}
//msgbox is alert function that combine the window.opn with confrim dialogue
function msgbox(message,url="",target="",type="",cond="",name=""){
  // name define only when the type of msgbox is promt
  // name is a name of the value when the message is submitted.
  // 
  // cond define only when the type of msgbox is promt
  // cond is a condition to do the action after the message is submitted.
  // cond sample : cond="id=1"
  if(type == "yesno") //it will show the confirm message before action.
  {
    if (confirm(message)){
      //save it here
      window.open(url,target);
    }else{
      //do nothing
    }
  }else if(type == "prompt"){ // it wil show the pop up input.
    var res = prompt(message);
    if (res != "" ){
        //save it here
        // Example : url = abc.com, cond = "id=1", name="date", res="1987/09/20"
        // the action is to open a link : abc.com?id=1&date=1987/09/20
      window.open(url +"?"+cond +"&"+ name +"="+ res,target);
    }
  }else{
    alert(message);
    if(url!=""){
        window.open(url,target);
    }
    
  }
}
//nav_color is to change the navigation background color
function bg_color(receiver,color){
    document.getElementById(receiver).style.backgroundColor = '#'+color;
}
function text_color(receiver,color){
    var elem_a = document.getElementById(receiver).getElementsByTagName('a');
    var elem_p = document.getElementById(receiver).getElementsByTagName('p');
    for (var i=0;i<elem_a.length;i++ ){
      elem_a[i].style.color = '#' + color;
    }
    for (var i=0;i<elem_p.length;i++ ){
      elem_p[i].style.color = '#' + color;
    }
}
function bd_img(input,receiver){
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
    $(receiver)
    .css('background-image', "url("+e.target.result+")");
    };
    
    reader.readAsDataURL(input.files[0]);
    }
}
function data_exist(table,field,val,receiver,what){
  var checking = "select * from "+table+" where `"+field+"`='"+val+"'";
    if (val == ""){
      document.getElementById(receiver).innerHTML = '';
    }
    var xhttp;
        xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              
                document.getElementById(receiver).innerHTML =  xhttp.responseText;
            }
          };
      xhttp.open("GET", "../library/data_exist.php?checking="+checking+"&what="+what, true);
      xhttp.send();
}
function username_validate(str,receiver,btn){
  
  if (str.indexOf("'") != -1){
    document.getElementById(receiver).innerHTML = '<i class="label red">The username can\'t containt any of sigle quote \' and double quotes \". </i>';  
    document.getElementById(btn).disabled = true;  
}else if (str.indexOf('"') != -1){
    document.getElementById(receiver).innerHTML = '<i class="label red">The username can\'t containt any of sigle quote \' and double quotes \". </i>';
    document.getElementById(btn).disabled = true;
  }else{
    document.getElementById(receiver).innerHTML ='';
    document.getElementById(btn).disabled = false;
  }

}

function class_checkbox(Cname,id,receiver){
  var el = document.getElementsByClassName(Cname);
  var val_receiver = document.getElementById(receiver).value;
if (id.checked == true){
  
    if (val_receiver == ""){
      document.getElementById(receiver).value = id.value;
  
    }else{
  
      document.getElementById(receiver).value += ";" + id.value;
    }
    for(var i=0;i<el.length;i++){
      el[i].checked = true;
    }
  }else{
    for(var i=0;i<el.length;i++){
      el[i].checked = false;
    }
     ////convert to array
  var arr_receiver = val_receiver.split(';');
  ////get indexOf value in array    
  var index_receiver = arr_receiver.indexOf(id.value);
    if (index_receiver!=-1){
      arr_receiver.splice(index_receiver,1);
    }else{
      arr_receiver.splice(0,1);
    }
    ///Clear receiver 
    document.getElementById(receiver).value  = "";
    ///split array into string to use with comma (;)
    for (i=0;i < arr_receiver.length;i++){
        if (document.getElementById(receiver).value == ""){
          document.getElementById(receiver).value = arr_receiver[i];
        }else{
          document.getElementById(receiver).value += ";"+ arr_receiver[i];
        }
    }  
  }
}
function check_value(id,receiver){
  var val_receiver = document.getElementById(receiver).value;
  if (id.checked == true){
  
    if (val_receiver == ""){
      document.getElementById(receiver).value = id.value;
  
    }else{
  
      document.getElementById(receiver).value += ";" + id.value;
    }
   
  }else{
   
    ////convert to array
  var arr_receiver = val_receiver.split(';');
  ////get indexOf value in array    
  var index_receiver = arr_receiver.indexOf(id.value);
    if (index_receiver!=-1){
      arr_receiver.splice(index_receiver,1);
    }else{
      arr_receiver.splice(0,1);
    }
    ///Clear receiver 
    document.getElementById(receiver).value  = "";
    ///split array into string to use with comma (;)
    for (i=0;i < arr_receiver.length;i++){
        if (document.getElementById(receiver).value == ""){
          document.getElementById(receiver).value = arr_receiver[i];
        }else{
          document.getElementById(receiver).value += ";"+ arr_receiver[i];
        }
    }
  }

}
function check_innerHTML(id,span,receiver){
  
  var val_receiver = document.getElementById(receiver).value;
  var title = document.getElementById(span);
  if (id.checked == true){
    if (val_receiver == ""){
      document.getElementById(receiver).value = title.innerHTML;
    }else{
  
      document.getElementById(receiver).value += ";" + title.innerHTML;
    }
  
  }else{
    
  ////convert to array
  var arr_receiver = val_receiver.split(';');
  ////get indexOf value in array    
  var index_receiver = arr_receiver.indexOf(title.innerHTML);
    if (index_receiver!=-1){
      arr_receiver.splice(index_receiver,1);
    }else{
      arr_receiver.splice(0,1);
    }
    ///Clear receiver 
    document.getElementById(receiver).value  = "";
    ///split array into string to use with comma (;)
    for (i=0;i < arr_receiver.length;i++){
        if (document.getElementById(receiver).value == ""){
          document.getElementById(receiver).value = arr_receiver[i];
        }else{
          document.getElementById(receiver).value += ";"+ arr_receiver[i];
        }
    }
   /////////////////////////////////////////////
    
  }

}
function remove_item(item,val,receiver){
  item.parentNode.removeChild(item);
  
  var val_receiver = document.getElementById(receiver).value;
  ////convert to array
  var arr_receiver = val_receiver.split(';');
  ////get indexOf value in array    
  
  var index_receiver = arr_receiver.indexOf(val);
  
    if (index_receiver!=-1){
      arr_receiver.splice(index_receiver,1);
    }else{
      arr_receiver.splice(0,1);
    }
  
    ///Clear receiver 
    document.getElementById(receiver).value  = "";
    ///split array into string to use with comma (;)
    for (i=0;i < arr_receiver.length;i++){
        if (document.getElementById(receiver).value == ""){
          document.getElementById(receiver).value = arr_receiver[i];
        }else{
          document.getElementById(receiver).value += ";"+ arr_receiver[i];
        }
    }
    
}

function menu_add(id,receiver,url,drop,htmlId,val_receiver,html_receiver){
var drop_active = document.getElementById(drop);
var val = document.getElementById(id).value;
if (drop_active.checked == true){
  ////Add item to the dropdown menu pre created
  var val_title = document.getElementById(htmlId).value;
  if (document.getElementById(val_receiver).value == ""){
      document.getElementById(val_receiver).value = val;
  }else{
    document.getElementById(val_receiver).value += ";"+val;
  }
  
  var arr_title = val_title.split(";");
  var arr_val = val.split(";");
  for(i=0;i<arr_title.length;i++){
    document.getElementById(html_receiver).innerHTML += '<li onclick="remove_item(this,'+"'"+arr_val[i]+"','"+val_receiver+"'"+')"><i  class="fa fa-times" aria-hidden="true"></i>'+arr_title[i]+'</li>';
  }
}else{
    
    var xhttp;
        xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              
                document.getElementById(receiver).innerHTML =  xhttp.responseText;
            }
          };
      xhttp.open("GET", url+"?m="+val, true);
      xhttp.send();
}
    
}
//box_check is for use in the table row, when click on the row check the input inside the row
function box_check(id){
  var el = document.getElementById(id);
  
  if(el.checked == true){
    el.checked = false;
  }else{
    el.checked = true;
  }
}
function allowDrop(ev){
  ev.preventDefault();
  
  var el = ev.target;
  if (el.id != 'sidebar_item' && el.id != 'sidebar_left' && el.id != 'sidebar_right'){
    el.getElementsByTagName('i')[0].className = 'fa fa-caret-up right fa-lg';
    el.getElementsByTagName('ul')[0].style.display = "none";
  }
  if(el.id == 'sidebar_left'){
    remove_side(sessionStorage.getItem('sidebarid'),'left_side');
  }
  if(el.id == 'sidebar_right'){
    remove_side(sessionStorage.getItem('sidebarid'),'right_side');
  }
}

function drag(ev,OrgInnerHtml,HtmlId,sidebarid){
  
  ev.dataTransfer.effectAllowed = 'move';
  ev.dataTransfer.setData("text",ev.target.id);
  sessionStorage.setItem("sidebarid",sidebarid);
  sessionStorage.setItem(HtmlId,OrgInnerHtml);
  
}

function drop(ev){
  ev.preventDefault();
  
  var data = ev.dataTransfer.getData("text");
  var innerSrc = sessionStorage.getItem(data);
  var newinner = '<ul style="display:none;"><label>Number of Items :</label><input name="" type="text" /></ul>';
  var el = ev.target;
  if (el.id != 'sidebar_item' && el.id != 'sidebar_left' && el.id != 'sidebar_right'){
    el = el.parentNode;
    
  }
  var title = ''+ innerSrc+'<i class="fa fa-caret-up right fa-lg" aria-hidden="true"></i>';
  
  if (el.id == 'sidebar_item'){
    document.getElementById(data).innerHTML = innerSrc;
  }else{
    document.getElementById(data).innerHTML =  title + newinner;
  }
  el.appendChild(document.getElementById(data));
  document.getElementById('left_side').value ='';
  document.getElementById('right_side').value = '';
    var left_items = document.getElementById('sidebar_left').getElementsByTagName('li');
    for(var y = 0; y < left_items.length;y++){
      if (document.getElementById('left_side').value == ""){
          document.getElementById('left_side').value = left_items[y].getAttribute('sidebar');
      }else{
          document.getElementById('left_side').value += ";" + left_items[y].getAttribute('sidebar');
      }
    }
    var right_items = document.getElementById('sidebar_right').getElementsByTagName('li');
    for(var z = 0; z < right_items.length;z++){
      if (document.getElementById('right_side').value == ""){
          document.getElementById('right_side').value = right_items[z].getAttribute('sidebar');
      }else{
          document.getElementById('right_side').value += ";" + right_items[z].getAttribute('sidebar');
      }
    }
  
  
  
}

function show_hide(el){
  if (el.getElementsByTagName('ul')[0].style.display == 'block'){
      el.getElementsByTagName('i')[0].className = 'fa fa-caret-up right fa-lg';
      el.getElementsByTagName('ul')[0].style.display = 'none';
  }else{
      el.getElementsByTagName('i')[0].className = 'fa fa-sort-desc right fa-lg';
      el.getElementsByTagName('ul')[0].style.display = 'block';
  }
}
////Pager/////
function pager(receiver,val){
  var xhttp;
        xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
              
                document.getElementById(receiver).innerHTML =  xhttp.responseText;
            }
          };
      xhttp.open("GET", "../library/pager_ajax.php?p="+val, true);
      xhttp.send();
}
//setInterval(show_time, 1000);

function show_time(){
var xhttp;
xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
if (xhttp.readyState == 4 && xhttp.status == 200) {  
    document.getElementById("ct").innerHTML =  xhttp.responseText;
  //alert (xhttp.responseText);
}
};
xhttp.open("GET", "../clock.php");
xhttp.send();
}

function update_time(Id,url){
    var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {    
      msgbox('the time been updated!',url,"_self");
    }  

};
xhttp.open("GET", "post_time.php?Id="+Id);
xhttp.send();
}
///
function show_div(el){
  document.getElementById(el).style.display = "block";
}
function hide_div(el){
  document.getElementById(el).style.display = "none";
}
function sub_review(id,val){
  document.getElementById(id).name = val;
}

function form_submit(formid){
  document.getElementById(formid).submit();
  // $(formid).submit(function( event ) {
  // alert( "Handler for .submit() called." );
  // event.preventDefault();
  // });
}
function youtube_preview(id,yid){
  var n  = yid.lastIndexOf("/");
  var youid = yid.substr(n+1);
  var y_emb_url = "https://www.youtube.com/embed/"+youid;
  document.getElementById(id).src = y_emb_url;
}

function ajaxs(receiver,phpfile,method,datas){
    var xhttp;
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {    
        document.getElementById(receiver).innerHTML = xhttp.responseText;
      }  

    };
    for (var i = 0;i < datas.length;i++){
      var data;
      if (i == 0){
        data = datas[i]+"="+document.getElementById(datas[i]).value;
      }else{
        data += "&&" + datas[i]+"="+document.getElementById(datas[i]).value;
      }
    }
    if (method == "get" || method == "Get" || method == "GET"){
      xhttp.open("GET", phpfile+"?"+data);
      xhttp.send();
    }else{
      
      xhttp.open("POST", phpfile, true);
      xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhttp.send(data);
    }
}
function ajaxdb(receiver,phpfile,method,datas){
  var xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {    
      document.getElementById(receiver).innerHTML = xhttp.responseText;
    }  

  };
  
  if (method == "get" || method == "Get" || method == "GET"){
    xhttp.open("GET", phpfile+"?"+datas);
    xhttp.send();
  }else{
    
    xhttp.open("POST", phpfile, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(datas);
  }
}
//Convert input between text between select
function toinput(divName,element,cond)
{
  if (document.getElementById(element).value == cond){
    document.getElementById(divName).innerHTML= "<input id='"+element+"' name='"+element+"' type='text' class='w3-input w3-border'/>";
    document.getElementById(divName).getElementsByTagName('input')[0].focus();
  }
  
}
// function to Verify Confirm password
// PassId and ConfirmPassId must to be input 
// ResId must to be label or text
function confirmpassword(PassId,ConfirmPassId,ResId,SubmitButton){
    
    if (document.getElementById(PassId).value != document.getElementById(ConfirmPassId).value){
      document.getElementById(ResId).innerHTML = "លេខសម្ងាត់មិនដូចគ្នា សូមបញ្ចូលម្តងទៀត / Wrong Password, Please enter the same value again.";
      document.getElementById(SubmitButton).disabled = true;
    }else{
      document.getElementById(ResId).innerHTML ="";
      document.getElementById(SubmitButton).disabled = false;
    }
}

