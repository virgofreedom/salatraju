
<ul class="vertical dropdown menu" data-dropdown-menu>
  <?php
  // Delevoper only
  if($_SESSION['infos']['role'] == 'Superuser'){
    echo '<li><a href="menu_admin.php">Create admin menu</a></li>';
  }
  // Admin and Superuser Session
  if ($_SESSION['infos']['role'] == 'Admin' || $_SESSION['infos']['role'] == 'Superuser'){
  ?>
  <li>
    <a href="#"><i class="fa fa-users" aria-hidden="true"></i>Users</a>
    <ul class="menu">
      <li><a href="users_add.php">Add New</a></li>
      <li><a href="users_list.php">User's List</a></li>
      
    </ul>
  </li>
  <li>
    <a href="#"><i class="fa fa-line-chart" aria-hidden="true"></i>Reports</a>
    <ul class="menu">
      <li><a href="report_article.php">Article</a></li>
      
    </ul>
  </li>
  <li>
    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i>Appearance</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/themes.php">Themes</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/menu_site.php">Menu</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/sidebar.php">Sidebar</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/index_layout.php">index's layout</a></li>
      <?php
      if ($_SESSION['infos']['role'] == 'Superuser'){
        echo '
          <li><a href="'.VIRTUAL_PATH.'index.php/sidebar_new.php">Add New Item to Sidebar</a></li>
        ';
      }
      ?>
    </ul>
  </li>



  <li>
    <a href="#"><i class="fa fa-users" aria-hidden="true"></i>Memebers</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/member_list.php">List</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="fa fa-users" aria-hidden="true"></i>Subscribers</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/subscriber_log.php">Activity Log</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/subscriber_list.php">List</a></li>
    </ul>
  </li>
  <?php
  }
  ?>
<?php
// Admin and Superuser and Editor Session
  if ($_SESSION['infos']['role'] == 'Superuser' || $_SESSION['infos']['role']== 'Admin' || $_SESSION['infos']['role']== 'Editor'){
?>

  <li>
    <a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i>Pages</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/pages_add.php">Add New</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/pages_list.php">Pages' List</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="fa fa-video-camera" aria-hidden="true"></i>Videos</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/videos.php">Add New</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/videos_list.php">Video's List</a></li>
    </ul>
  </li>
  <li>
    <a href="#"><i class="fa fa-clipboard" aria-hidden="true"></i>Posts</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/posts_add.php">Add New</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/posts_list.php">Posts' List</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/catergories_list.php">Categories</a></li>
      
    </ul>
  </li>
  
  
  <li>
    <a href="<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php"><i class="fa fa-file-image-o" aria-hidden="true"></i>Images Manager</a>
  </li>
  <?php
  }
  ?>
  <?php
  // Admin and Superuser  and Dictonary Entry data Session
  if ($_SESSION['infos']['role'] == 'Superuser' || $_SESSION['infos']['role']== 'Admin' || $_SESSION['infos']['role']== 'Dictionary'){
  ?>
  <li>
    <a href="#"><i class="fa fa-book" aria-hidden="true"></i>Dictionary</a>
    <ul class="menu">
      <li><a href="<?=VIRTUAL_PATH?>index.php/dictionary_add.php">Add New</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/dictionary_list.php">List</a></li>
      <li><a href="<?=VIRTUAL_PATH?>index.php/dictionary_misspelling.php">Correct Spelling</a></li>
    </ul>
  </li>
  <?php
  }
  // Admin and Superuser  and Annotation Entry data Session
  if ($_SESSION['infos']['role'] == 'Superuser' || $_SESSION['infos']['role']== 'Admin' || $_SESSION['infos']['role']== 'Annotation'){
  ?>
    <li>
    <a href="#"><i class="fa fa-book" aria-hidden="true"></i>Annotation</a>
    <ul class="menu">
    <li><a href="<?=VIRTUAL_PATH?>index.php/annotation.php">New annotation</a></li>
    <li><a href="<?=VIRTUAL_PATH?>index.php/annotation_list.php">List</a></li>
  <?php
  if($_SESSION['infos']['role'] == 'Superuser' || $_SESSION['infos']['role']== 'Admin'){
?>
    <li><a href="<?=VIRTUAL_PATH?>index.php/annotation_req_list.php">Request to delete's list</a></li>
    <li><a href="<?=VIRTUAL_PATH?>index.php/annotation_del_list.php">Delete's list</a></li>
<?php
  }
?>
    </ul>
  </li>
  <?php
  }
  ?>
</ul>