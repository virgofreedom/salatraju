<div class="small-12 medium-12 large-12 columns post-block index-post">  
<?php
if (!isset($_SESSION['userinfos'])){
?>
   <div class="w3-center"><h4 class="kh-moullight">ចុះឈ្មោះចូលដើម្បីអានទំព័រនេះ</h4></div>
   <div class="small-10 small-offset-1">
   <form action="<?=DOMAIN?>subscribe_loging" method="post">
        <input type="hidden" name="page" value="<?=key($_GET)?>" readonly>
       <div class="row">
           <div class="small-3 columns">
                <label for="email" class="text-right middle" >អ៊ីម៉ែល/Email</label>
           </div>
           <div class="small-9 columns">
                <input type="Email" name="Email" id="email" placeholder="អ៊ីម៉ែល/Email" required>
            </div>
       </div>
       <div class="row">
           <div class="small-3 columns">
                <label for="password" class="text-right middle" >លេខសម្ងាត់/Password</label>
           </div>
           <div class="small-9 columns">
                <input type="password" name="Password" id="password" placeholder="លេខសម្ងាត់/Password" required>
            </div>
       </div>
                <?php
                if(isset($_SESSION['usererror'])){
                    echo '<div class="row text-right">';
                    echo '<span class="label red">';
                    echo $_SESSION['usererror'];
                    echo '</span>';
                    echo '</div>';
                }
                ?>
       <div class="row text-right">
                <input type="submit" name="submit" value="ចុះឈ្មោះចូល/Login" class="button success small">
        </div>
        <div class="row text-right">
            មិនទាន់មានគណនី <a href="<?=DOMAIN?>subscribe_form">សូមចុចនៅទីនេះ</a> <br>
            Don't have account?<a href="<?=DOMAIN?>subscribe_form">click here</a>
        </div>
        <div class="row text-right">
        <a href="<?=DOMAIN?>subscribe_forgot">ភ្លេចលេខសម្ងាត់? | forgot your password?</a> <br>
        </div>
   </form>
   </div>
<?php
}
?>
</div>
