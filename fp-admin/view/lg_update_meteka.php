
<div class="small-12 columns big-menu w3-padding-large">
<?php
$permission = array("Admin","Superuser","LegalUpdate","AdminLegalUpdate");
$db_name = "salatraju_legal_update";
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{
    $metekaid = "";$order = "";$title = "";$subtitle = "";$shortdesc = "";$detail="";$status="";
    if (isset($_POST['save'])){
        
        $order = $_POST['order'];
        $title = $_POST['title'];
        $subtitle = $_POST['subtitle'];
        $shortdesc = $_POST['shortdesc'];
        $detail = $_POST["desc"];
        $status = "Saved";
            $data = array(
                "MetekaId" => $_POST['order'],
                "MetekaTitle" => $_POST['title'],
                "MetekaSubTitle" => $_POST['subtitle'],
                "ShortDescription" => htmlspecialchars($_POST['shortdesc'],ENT_QUOTES),
                "Detail" => htmlspecialchars($_POST["desc"],ENT_QUOTES),
                "PublicationId"=> $_POST['pubid'],
                "Author" => $_SESSION['infos']['id'],
                "Status"=> 1
            );
            if ($_POST['id'] == ""){
                db_insert("Meteka",$data,$db_name);
                $data_re = db_get('Meteka','WHERE MetekaTitle="'.$_POST['title'].'"',"","","",$db_name);
                $metekaid = $data_re[0]['Id'];
                $pub_id = $data_re[0]['PublicationId'];
                activity_log("Legal Update","Created New and Saved");
            }else{
                $cond = array("Id"=>$_POST['id']);
                db_update("Meteka",$data,$cond,$db_name);
                $data_re = db_get('Meteka','WHERE MetekaTitle="'.$_POST['title'].'"',"","","",$db_name);
                $pub_id = $data_re[0]['PublicationId'];
                $metekaid = $data_re[0]['Id'];
                activity_log("Legal Update","Updated New and Saved");
            }   
            
    }
    elseif (isset($_POST['publish'])){
        
        $order = $_POST['order'];
        $title = $_POST['title'];
        $subtitle = $_POST['subtitle'];
        $shortdesc = $_POST['shortdesc'];
        $detail = $_POST["desc"];
        $status = "Published";
            $data = array(
                "MetekaId" => $_POST['order'],
                "MetekaTitle" => $_POST['title'],
                "MetekaSubTitle" => $_POST['subtitle'],
                "ShortDescription" => htmlspecialchars($_POST['shortdesc'],ENT_QUOTES),
                "Detail" => htmlspecialchars($_POST["desc"],ENT_QUOTES),
                "PublicationId"=> $_POST['pubid'],
                "Author" => $_SESSION['infos']['id'],
                "Status"=> 2
            );
            if ($_POST['id'] == ""){
                db_insert("Meteka",$data,$db_name);
                $data_re = db_get('Meteka','WHERE MetekaTitle="'.$_POST['title'].'"',"","","",$db_name);
                $metekaid = $data_re[0]['Id'];
                $pub_id = $data_re[0]['PublicationId'];
                activity_log("Legal Update","Created New and Published");
            }else{
                $cond = array("Id"=>$_POST['id']);
                db_update("Meteka",$data,$cond,$db_name);
                $data_re = db_get('Meteka','WHERE MetekaTitle="'.$_POST['title'].'"',"","","",$db_name);
                $pub_id = $data_re[0]['PublicationId'];
                $metekaid = $data_re[0]['Id'];
                activity_log("Legal Update","Updated New and Published");
            }   
    }
    if(isset($_GET['new'])){
        $pub_id = $_GET['pudid'];
    }

    if(isset($_GET['lgupdate_number'])){
        $metekaid = "";$order = "";$title = "";$subtitle = "";$shortdesc = "";$detail="";$status="";
        
        $data_pub = db_get('publications','WHERE Id="'.$_GET['lgupdate_number'].'"',"","","",$db_name);
            if(count($data_pub) == 0){//No publication for this number so add new
                $data = array("Title"=> $_GET['lgupdate_number'],"MonthYear"=>date('Y-m-d'));
                db_insert('publications',$data,$db_name);
                echo 'data inserted!';
                $data = db_get("publications","","","ORDER BY Id DESC","",$db_name);
                activity_log("Legal Update","Created New Publication");
                header("Location: lg_update_meteka?lgupdate_number=".$data[0]['Id']);
            }else{    
                $pub_id = $data_pub[0]['Id'];
                // Show Form to create meteka
            }
    }
    if(isset($_GET['metekaid'])){
        
        $data = db_get('Meteka','WHERE Id="'.$_GET['metekaid'].'"',"","","",$db_name);
        $metekaid = $data[0]['Id'];$order = $data[0]['MetekaId'];
        $title = $data[0]['MetekaTitle'];$subtitle = $data[0]['MetekaSubTitle'];
        $shortdesc = $data[0]['ShortDescription'];$detail=$data[0]['Detail'];
        $status="";$pub_id= $data[0]['PublicationId'];
    }

?>
    <form action="lg_update_meteka" method="POST">
        <center>មាតិកា</center>
        <div class="row">
            <div class="small-12">
                <label for="title">លេខរៀង ៖
                    <input class="w3-input" type="text" name="order" value="<?=$order?>" placeholder="" required/>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12">
                <label for="title">ចំណងជើង ៖
                    <input class="w3-input" type="text" name="title" value="<?=$title?>" placeholder="" required/>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12">
                <label for="title">ចំណងជើងរង ៖
                    <input class="w3-input" type="text" name="subtitle" value="<?=$subtitle?>" placeholder=""/>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12">
                <label for="title">អត្ថបទសង្ខេប ៖
                <textarea name="shortdesc" id="shortdesc" rows="3" cols="80">
                <?=$shortdesc?>
                </textarea>
                <script>
                                    // Replace the <textarea id="editor1"> with a CKEditor
                                    // instance, using default configuration.
                                    CKEDITOR.replace( 'shortdesc',{
                                        filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                                    } );
                            </script>
                </label>
            </div>
        </div>
        <div class="row">
            <div class="small-12">
                <label for="title">អត្ថបទសម្រាយ ៖
                <textarea name="desc" id="desc" rows="5" cols="80">
                <?=$detail?>
                </textarea>
                <script>
                                    // Replace the <textarea id="editor1"> with a CKEditor
                                    // instance, using default configuration.
                                    CKEDITOR.replace( 'desc',{
                                        filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                                    } );
                            </script>
                
                </label>
            </div>
        </div>
        <center><?=$status?></center>
        <input type="hidden" name="pubid" value="<?=$pub_id?>">
        <input type="hidden" name="id" value="<?=$metekaid?>">
        <input type="submit" name="save" class="w3-button  w3-green w3-margin-right" style="font-size:16px" value="Save"/>
<?php
        $pub_button = array("Admin","Superuser","AdminLegalUpdate");
        if (in_array($_SESSION['infos']['role'],$pub_button)){
?>
        <input type="submit" name="publish" class="w3-button  w3-green w3-margin-right" value="Publish" style="font-size:16px"/>
<?php
        }
?>
        <a href="<?=VIRTUAL_PATH?>index.php/lg_update_meteka?pudid=<?=$pub_id?>&new" class="w3-button w3-round-large  w3-green w3-margin-right" style="font-size:16px">Create New</a>
        <a href="<?=VIRTUAL_PATH?>index.php/lg_update_list" class="w3-button w3-round-large  w3-red w3-margin" style="font-size:16px">Cancel</a>
    </form>
<?php                
    
    
?>


<?php
}
?>
</div>