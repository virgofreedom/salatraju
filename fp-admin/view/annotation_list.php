<?php
$permission = array("Admin","Superuser","Annotation");
$tbody="";
$links="";
if (!in_array($_SESSION['infos']['role'],$permission)){
    echo "You don't have the permission to use this page.";
}else{

    if (isset($_GET['kromid'])){#get kinthi from krom Id
        $_SESSION['links'] = array(
            "kromid" => $_GET['kromid'],
            "kromtitle" => $_GET['kromtitle']
        );
        $data = db_get("kinthi","Where kromId='".$_GET['kromid']."' AND Status=1 ","","ORDER BY kinthiId ASC","",DB_NAME2);
        $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a>";
        $thead = "<th>ចំណងជើងគន្ថី</th>";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='
            <td>
                <a href="'.THIS_PAGE.'?kinthiId='.$data[$i]['kinthiId'].'&kinthititle='.$data[$i]['kinthiTitle'].'">'.$data[$i]['kinthiTitle'].'</a>
            </td>';
            
            $tbody .='</tr>';
        }   
    }elseif (isset($_GET['kinthiId'])){#metika from kinthi id
        $data = db_get("metika","Where kinthiId='".$_GET['kinthiId']."' AND Status=1 ","","ORDER BY metikaId ASC","",DB_NAME2);
        $_SESSION['links'] = array(
            "kromid" => $_SESSION["links"]['kromid'],
            "kromtitle" => $_SESSION["links"]['kromtitle'],
            "kinthiId"=>$_GET['kinthiId'],
            "kinthititle"=>$_GET['kinthititle']
        );

        $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a> > 
        <a href='".THIS_PAGE."?kinthiId=".$_SESSION["links"]['kinthiId']."&kinthititle=".$_SESSION["links"]['kinthititle']."'>".$_SESSION["links"]['kinthititle']."</a>
        ";
        $thead = "<th>ចំណងជើងមាតិកា</th>";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='
            <td>
                <a href="'.THIS_PAGE.'?metikaId='.$data[$i]['metikaId'].'&metikatitle='.$data[$i]['metikaTitle'].'">'.$data[$i]['metikaTitle'].'</a>
            </td>';
            
            $tbody .='</tr>';
        }
    }elseif (isset($_GET['metikaId'])){#get chapter from metikaId
        $data = db_get("chapter","Where metikaId='".$_GET['metikaId']."' AND Status=1 ","","ORDER BY chapterId ASC","",DB_NAME2);
        $_SESSION['links']=array(
            "kromid" => $_SESSION["links"]['kromid'],
            "kromtitle" => $_SESSION["links"]['kromtitle'],
            "kinthiId"=>$_SESSION["links"]['kinthiId'],
            "kinthititle"=>$_SESSION["links"]['kinthititle'],
            "metikaId"=>$_GET['metikaId'],
            "metikatitle"=>$_GET['metikatitle']
        );

        $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a> > 
        <a href='".THIS_PAGE."?kinthiId=".$_SESSION["links"]['kinthiId']."&kinthititle=".$_SESSION["links"]['kinthititle']."'>".$_SESSION["links"]['kinthititle']."</a> >
        <a href='".THIS_PAGE."?metikaId=".$_SESSION["links"]['metikaId']."&metikatitle=".$_SESSION["links"]['metikatitle']."'>".$_SESSION["links"]['metikatitle']."</a>
        ";
        $thead = "<th>ចំណងជើងជំពូក</th>";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='
            <td>
                <a href="'.THIS_PAGE.'?chapterId='.$data[$i]['chapterId'].'&chaptertitle='.$data[$i]['chapterTitle'].'">'.$data[$i]['chapterTitle'].'</a>
            </td>';
            
            $tbody .='</tr>';
        }
    }elseif (isset($_GET['chapterId'])){#get section from chapter
        $data = db_get("section","Where chapterId='".$_GET['chapterId']."' AND Status=1 ","","ORDER BY sectionId ASC","",DB_NAME2);
        $_SESSION['links']=array(
            "kromid" => $_SESSION["links"]['kromid'],
            "kromtitle" => $_SESSION["links"]['kromtitle'],
            "kinthiId"=>$_SESSION["links"]['kinthiId'],
            "kinthititle"=>$_SESSION["links"]['kinthititle'],
            "metikaId"=>$_SESSION["links"]['metikaId'],
            "metikatitle"=>$_SESSION["links"]['metikatitle'],
            "chapterId"=>$_GET['chapterId'],
            "chaptertitle"=>$_GET['chaptertitle']
        );
        
        if ($data[0]["sectionTitle"]=="0"){  
            echo '<script>window.open("'.THIS_PAGE.'?sectionId='.$data[0]['sectionId'].'&sectionTitle='.$data[0]['sectionTitle'].'","_self")</script>';
        }else{
            $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a> > 
            <a href='".THIS_PAGE."?kinthiId=".$_SESSION["links"]['kinthiId']."&kinthititle=".$_SESSION["links"]['kinthititle']."'>".$_SESSION["links"]['kinthititle']."</a> >
            <a href='".THIS_PAGE."?metikaId=".$_SESSION["links"]['metikaId']."&metikatitle=".$_SESSION["links"]['metikatitle']."'>".$_SESSION["links"]['metikatitle']."</a> >
            <a href='".THIS_PAGE."?chapterId=".$_SESSION["links"]['chapterId']."&chaptertitle=".$_SESSION["links"]['chaptertitle']."'>".$_SESSION["links"]['chaptertitle']."</a>
            ";
            $thead = "<th>ចំណងជើងផ្នែក</th>";
            for ($i=0;$i<count($data);$i++){
                $tbody .='<tr>';
                $tbody .='
                <td>
                    <a href="'.THIS_PAGE.'?sectionId='.$data[$i]['sectionId'].'&sectionTitle='.$data[$i]['sectionTitle'].'">'.$data[$i]['sectionTitle'].'</a>
                </td>';
                
                $tbody .='</tr>';
            }   
        }
    }elseif (isset($_GET['sectionId'])){#get kathapheak from section
        $data = db_get("kathapheak","Where sectionId='".$_GET['sectionId']."' AND Status=1 ","","ORDER BY kathapheakId ASC","",DB_NAME2);
        $_SESSION['links']=array(
            "kromid" => $_SESSION["links"]['kromid'],
            "kromtitle" => $_SESSION["links"]['kromtitle'],
            "kinthiId"=>$_SESSION["links"]['kinthiId'],
            "kinthititle"=>$_SESSION["links"]['kinthititle'],
            "metikaId"=>$_SESSION["links"]['metikaId'],
            "metikatitle"=>$_SESSION["links"]['metikatitle'],
            "chapterId"=>$_SESSION["links"]['chapterId'],
            "chaptertitle"=>$_SESSION["links"]['chaptertitle'],
            "sectionId"=>$_GET['sectionId'],
            "sectionTitle"=>$_GET['sectionTitle']
        );
        if($data[0]["kathapheakTitle"]=="0"){
            echo '<script>window.open("'.THIS_PAGE.'?kathapheakId='.$data[0]['kathapheakId'].'&kathapheakTitle='.$data[0]['kathapheakTitle'].'","_self")</script>';
        }else{
            $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a> > 
            <a href='".THIS_PAGE."?kinthiId=".$_SESSION["links"]['kinthiId']."&kinthititle=".$_SESSION["links"]['kinthititle']."'>".$_SESSION["links"]['kinthititle']."</a> >
            <a href='".THIS_PAGE."?metikaId=".$_SESSION["links"]['metikaId']."&metikatitle=".$_SESSION["links"]['metikatitle']."'>".$_SESSION["links"]['metikatitle']."</a> >
            <a href='".THIS_PAGE."?chapterId=".$_SESSION["links"]['chapterId']."&chaptertitle=".$_SESSION["links"]['chaptertitle']."'>".$_SESSION["links"]['chaptertitle']."</a>
            <a href='".THIS_PAGE."?sectionId=".$_SESSION["links"]['sectionId']."&sectionTitle=".$_SESSION["links"]['sectionTitle']."'>".$_SESSION["links"]['sectionTitle']."</a>
            ";
            $thead = "<th>ចំណងជើងកថាភាគ</th>";
            for ($i=0;$i<count($data);$i++){
                $tbody .='<tr>';
                $tbody .='
                <td>
                    <a href="'.THIS_PAGE.'?kathapheakId='.$data[$i]['kathapheakId'].'&kathapheakTitle='.$data[$i]['kathapheakTitle'].'">'.$data[$i]['chapterTitle'].'</a>
                </td>';
                
                $tbody .='</tr>';
            }
        }
        
    }elseif (isset($_GET['kathapheakId'])){#get metra from kathapeak
        
        $data = db_get("metra","Where kathapheakId='".$_GET['kathapheakId']."'","","ORDER BY metraOrder ASC","",DB_NAME2);
        $_SESSION['links']=array(
            "kromid" => $_SESSION["links"]['kromid'],
            "kromtitle" => $_SESSION["links"]['kromtitle'],
            "kinthiId"=>$_SESSION["links"]['kinthiId'],
            "kinthititle"=>$_SESSION["links"]['kinthititle'],
            "metikaId"=>$_SESSION["links"]['metikaId'],
            "metikatitle"=>$_SESSION["links"]['metikatitle'],
            "chapterId"=>$_SESSION["links"]['chapterId'],
            "chaptertitle"=>$_SESSION["links"]['chaptertitle'],
            "sectionId"=>$_SESSION["links"]['sectionId'],
            "sectionTitle"=>$_SESSION["links"]['sectionTitle'],
            "kathapheakId"=>$_GET['kathapheakId'],
            "kathapheakTitle"=>$_GET['kathapheakTitle']
        );

        $links = "<a href='".THIS_PAGE."?kromid=".$_SESSION["links"]['kromid']."&kromtitle=".$_SESSION["links"]['kromtitle']."'>".$_SESSION["links"]['kromtitle']."</a> > 
        <a href='".THIS_PAGE."?kinthiId=".$_SESSION["links"]['kinthiId']."&kinthititle=".$_SESSION["links"]['kinthititle']."'>".$_SESSION["links"]['kinthititle']."</a> >
        <a href='".THIS_PAGE."?metikaId=".$_SESSION["links"]['metikaId']."&metikatitle=".$_SESSION["links"]['metikatitle']."'>".$_SESSION["links"]['metikatitle']."</a> >
        <a href='".THIS_PAGE."?chapterId=".$_SESSION["links"]['chapterId']."&chaptertitle=".$_SESSION["links"]['chaptertitle']."'>".$_SESSION["links"]['chaptertitle']."</a> >
        ";
        if ($_SESSION["links"]["sectionTitle"]!="0"){
            $links .= "<a href='".THIS_PAGE."?sectionId=".$_SESSION["links"]['sectionId']."&sectionTitle=".$_SESSION["links"]['sectionTitle']."'>".$_SESSION["links"]['sectionTitle']."</a> >";
        }
        if ($_SESSION["links"]["kathapheakTitle"]!="0"){
            $links .= "<a href='".THIS_PAGE."?kathapheakId=".$_SESSION["links"]['kathapheakId']."&kathapheakTitle=".$_SESSION["links"]['kathapheakTitle']."'>".$_SESSION["links"]['kathapheakTitle']."</a>";
        }
        $thead = "<th>ចំណងជើងមាត្រា</th><th>មាត្រាពិពណ៌នា</th>";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='<td>'.$data[$i]['metraTitle'].'</td>';
            $tbody .='<td>'.$data[$i]['metraDescription'].'</td>';
            $tbody .='</tr>';
        }
    }else{#get Krom
        unset($_SESSION['links']);
        $data = db_get("krom","","","ORDER BY kromId ASC","",DB_NAME2);
        $thead = "<th>ចំណងជើងក្រម</th><th>ការពិពណ៌នា</th>";
        $tbody = "";
        for ($i=0;$i<count($data);$i++){
            $tbody .='<tr>';
            $tbody .='
            <td>
                <a href="'.THIS_PAGE.'?kromid='.$data[$i]['kromId'].'&kromtitle='.$data[$i]['kromTitle'].'">'.$data[$i]['kromTitle'].'</a>
            </td>';
            $tbody .='<td>'.$data[$i]['kromDescrtiption'].'</td>';
            $tbody .='</tr>';
        }
        
    }
    
    
?>
<div class="small-12 columns big-menu">
<?=$links?>
<table class="w3-table w3-bordered w3-striped w3-border w3-hoverable ">
    <thead>
        <tr>
        <?=$thead?>
        </tr>
    </thead>
    <tbody>
    <?=$tbody?>
    </tbody>
</table>

</div>

<?php
}#end permission
?>
 