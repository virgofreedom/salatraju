<?php
// Headers
header('Access-Control-Allow-Origin:*');
header('Content-Type: application/json');

include '../fp-admin/config/config.php';
$myObj = new \stdClass();
$lex = array();

if (isset($_GET["id"])){#show content by id
    $res = db_get("dictionnary","Where Id='".$_GET["id"]."'");
    $res_des = 0;
}elseif(isset($_GET["word"])){#show content by word
    $word = $_GET["word"];
    $res = db_get('dictionnary',"WHERE Word LIKE '%$word%'",'',"Order BY Id");
    $res_des = db_get('dictionnary',"WHERE Description LIKE '%$word%'",'',"Order BY Id");
}else{#show all
    $res = db_get("dictionnary");
    $res_des = 0;
}
    for ($i=0;$i<count($res);$i++){
        #post_content
        $content = str_replace("&nbsp;"," ",html_entity_decode($res[$i]["Description"],ENT_QUOTES));
        $content = str_replace("\n",chr(32),$content);
        $content = str_replace("\r",chr(32),$content);
        array_push(
            $lex,array(
                'Id'=>$res[$i]["Id"],
                'Word'=>$res[$i]["Word"],
                'Description'=>$content,
                'view'=>$res[$i]["View"]
            )
        );
    }
    if ($res_des!=0){
        for ($i=0;$i<count($res_des);$i++){
            #post_content
            $content = str_replace("&nbsp;"," ",html_entity_decode($res_des[$i]["Description"],ENT_QUOTES));
            $content = str_replace("\n",chr(32),$content);
            $content = str_replace("\r",chr(32),$content);
            array_push(
                $lex,array(
                    'Id'=>$res_des[$i]["Id"],
                    'Word'=>$res_des[$i]["Word"],
                    'Description'=>$content,
                    'view'=>$res_des[$i]["View"]
                )
            );
        }
    }

    $myJSON = json_encode($lex, JSON_PRETTY_PRINT);
echo $myJSON;