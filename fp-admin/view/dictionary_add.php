<?php
if (isset($_POST['add'])){
    $cond = array("Word"=>$_POST['Word']);
    $res_word = db_get_where('dictionnary',$cond);
    if (count($res_word) == 0){
        //if $res_word == 0 add new word
        $data = array(
        "Word"=>$_POST['Word'],
        "Description"=>$_POST['Definition'],
        "Status"=>0
        );
        db_insert('dictionnary',$data);
        $status = "$word has been added!";
    }else{
        $message = "ពាក្យនេះធ្លាប់បានបញ្ចូលម្តងហើយ! សូមមេត្តាចូលទៅក្នុងបញ្ជីពាក្យដើម្បីធ្វើការកែប្រែ! សូមអគុណ!";
        //if $res_word > 0, ask user to update or not
        msgbox($message,VIRTUAL_PATH . "index.php/dictionary_list.php?Word=".$_POST['Word']);
    }
}else if(isset($_POST['save'])){
    // update word
    $data = array(
        "Word"=>$_POST['Word'],
        "Description"=>$_POST['Definition']
    );
    $cond = array("Id"=>$_POST['id']);
    db_update('dictionnary',$data,$cond);
    $status = $_POST['Word']." has beed updated!";
    msgbox($status,VIRTUAL_PATH . "index.php/dictionary_list.php?Word=".$_POST['Word']);
}
if (isset($_GET['Id'])){
    $id = $_GET['Id'];
    $cond = array("Id"=>$_GET['Id']);
    $res = db_get_where('dictionnary',$cond);
    $word = $res[0]['Word'];
    $content = $res[0]['Description'];
    
    $name_btn = "save";
    $caption_btn = "រក្សាទុក";
    
}else{
    $name_btn = "add";
    $caption_btn = "បន្ថែមពាក្យ";
    $word = "";
    $content = "";  
    $status = "";
}
?>
<div class="small-12 columns big-menu">
<script src="<?=VIRTUAL_PATH?>ckeditor/ckeditor.js"></script>
<form action="<?=THIS_PAGE?>" method="POST">
<input type="hidden" name="id" value="<?=$id?>" readonly/>
    <div class="row">
        <div class="small-12">
            <label for="title">Word:*
                <input type="text" name="Word" value="<?=$word?>" placeholder="Please enter title here." required/>
            </label>
        </div>
    </div>
    <div class="row">
    <label for="title">Definition:*</label>
    </div>
    <div class="row">
        <textarea name="Definition" id="post_content" rows="10" cols="80">
        <?=$content?>
        </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'post_content',{
                    filebrowserBrowseUrl: '<?=VIRTUAL_PATH?>ckeditor/plugins/imageuploader/imgbrowser.php'
                } );
            </script>
    </div>
    <div class="row">
        <div class="small-12">
            <input type="submit" class="button right" name="<?=$name_btn?>" value="<?=$caption_btn?>"/>
            <span id="status"><?=$status?></span>   
        </div>  
    </div>
</form>
</div>
